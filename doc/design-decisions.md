# Design decisions regarding the Michelson-Polyrun prototype

This document lists some of the design questions which were raised
during the prototyping of Polyrun. For each question, we present the
options we considered, the one we implemented, and why.

## Supported language

We don't (yet) support 
- macros
- gas model 
- annotations

The subset of types and instructions supported will grow progressively.

The initial subset represents a wide variety of feature
- overloading
- polymorphic and monomorphic instructions
- control structures

## Input

See issue https://gitlab.com/nomadic-labs/michelson-polyrun/-/issues/1, merge request https://gitlab.com/nomadic-labs/michelson-polyrun/-/merge_requests/5 and milestone https://gitlab.com/tezos/michelson-reference/-/milestones/4.

Many options are available for the concrete syntax of the description
of the Michelson language which is used as input by Polyrun. The
options we considered were:

- OCaml:
  - Pros: nothing to parse
  - Cons: too verbose, even when abusing optional arguments
- Yaml + Ott:
  - Pros: description already partially available in the Michelson reference repository
  - Cons:
    - users need to jungle with two input files which is confusing
    - dependency on the Ott tool, which is not much maintained
    - Yaml is indentation sensitive and the simplest way to check compliance with a schema seems to be to translate it to JSON
- JSON from Michelson Reference:
  - Pros:
    - Used as intermediate format in the Michelson Reference (so as complete as the Yaml + Ott option)
    - Easy to parse in OCaml
    - Easy to check compliance with a schema (done in the Michelson Reference repo)
    - No new dependency: JSON parsers are already in Octez dep cone
  - Cons:
    - to produce the JSON input, we have a dependency on Michelson reference
      Python scripts and the patched Ott tool
- Custom syntax:
  - Pros: as detailed and concise as we want
  - Cons:
    - requires to write and maintain a good documentation of the syntax
    - parser to develop and maintain

We picked the JSON option in MR
https://gitlab.com/nomadic-labs/michelson-polyrun/-/merge_requests/5.

The typing judgements in the input format are defined in raw strings.  We could
either patch the ott tool to produce structured json description of rules'
judgements, or reparse them.  
Reparsing was simpler for this prototype.


## Internal architecture

For a description of the architecture of Polyrun, see ./architecture.md.

The main question regarding the architecture is how much sharing we
want between various backends. To allow some sharing without making
the input syntax too verbose, we chose to distinguish the common input
of all the backends (the data model) from the actual input of Polyrun
(the description) and to perform an automated translation from the
latter to the former (the enriching phase). Ideally, backends should
be relatively straightforward iterations over the data model, and the
data model should be extended when new features are needed; the
interesting transformations should be defined in the enriching phase so that they can be used by all backends, including future ones.

## Output

### Controlling order of iteration

Our targets are not always consistent in the order in which they iterate over
types and instructions. They may even change the order on purpose to ease
readability (for instance by putting together similar cases or putting the most
interesting case first).  

We can either change the targets (protocol files, etc) to use a consistent
order, or we let the backends use a description of the order in which it should
iterate over the Michelson types or instructions.  
More details in issue
https://gitlab.com/nomadic-labs/michelson-polyrun/-/issues/8.

We chose the later option.

### "Smart" pretty printer of pattern matching

Pushing syntaxical rewritting to the pretty printers allows for simpler backend
implementation, for example:
- replacing unused variables by underscores
- factorising patterns with common associated code

### Templating and string interpolation

See issue https://gitlab.com/nomadic-labs/michelson-polyrun/-/issues/6
and merge request
https://gitlab.com/nomadic-labs/michelson-polyrun/-/merge_requests/6.

A goal of Polyrun is to replace existing tools depending on the
details of Michelson semantics. To integrate Polyrun-generated code in
the development workflow of these existing tools, we can either try to
match the manually-written source files of these tools or simply use
the existing implementation as inspiration for a next version using
code generation. Reproducing the existing files has the benefit of
easing the transition but requires to dump a lot of verbatim code in
the Polyrun backends (everything that is not meant to be generated in
the files containing generated code). This verbatim code may vary
while Polyrun is being developed so it should be considered as a
moving target but fortunately we don't do anything about this code but
dump it so maintaining it is not very costly. Handling large verbatim
code in backends poses however readability challenges; if we simply
rely on Format.fprintf to produce the file we get someting like

```
Format.fprintf fmt "<large prefix>%a<large midfix>%a<large other midfix>%a<large suffix>"
  some_printing_function some_input
  some_other_printing_function some_other_input
  yet_another_printing_function yet_another_input
```

The main problem with this is that it is hard to relate the generated
parts and the non-generated ones. To understand if the things defined
in `<large other midfix>` are or not available when generating code
with `some_other_printing_function` we need to locate the `%a` hidden
in the middle of the format string, count them and count the
parameters of Format.fprintf.

The dependencies between generated and non-generated parts can be made
slightly more explicit by naming non-generated parts too:

```
let prefix = "<large prefix>" in
let midfix_1 = "<large midfix>" in
let midfix_2 = "<large other midfix>" in
let suffix = "<large suffix>" in

Format.fprintf fmt "%s%a%s%a%s%a%s"
  prefix
  some_printing_function some_input
  midfix_1
  some_other_printing_function some_other_input
  midfix_2
  yet_another_printing_function yet_another_input
  suffix
```

It makes clearer that `some_other_printing_function` has `midfix_1`
but not `midfix_2` in its scope but naming the various non-generated
parts in a meaningful way is hard because the source files have not
been structured to make this splitting meaningful.

Another option, which is available in some languages (such as Python
and Javascript) is to refer to the name of the generated part instead
of Format's `%a` notation. This is called string interpolation and
would look like this in Python:

```
print (f"<large prefix>{generated_string}<large midfix>{other_genereted_string}<large other midfix>{yet_another_generated_stuff}<large suffix>")
```

OCaml does not feature string interpolation natively but string
interpolation is featured by several ppxs. We chose ppx_string in
https://gitlab.com/nomadic-labs/michelson-polyrun/-/merge_requests/6
because it is both simple and easy to install (it is available on
Opam).

String interpolation can be seen as a light form of templating;
several template engines exist for OCaml but they typically require
learning a custom programming language which we don't want to impose
on developers of Polyrun backends.

### Drawing the line between generated and non-generated parts

When generating a type definition or a function defined by
pattern-matching, we usually have to write one or two lines of header
followed by many individual cases. For example, the definition of the
GADT for Michelson instructions in the protocol looks like

```
type ('before_top, 'before, 'result_top, 'result) kinstr =
  | IDrop :
      Script.location * ('b, 's, 'r, 'f) kinstr
      -> ('a, 'b * 's, 'r, 'f) kinstr
  | IDup :
      Script.location * ('a, 'a * ('b * 's), 'r, 'f) kinstr
      -> ('a, 'b * 's, 'r, 'f) kinstr
  [...]
```

It's not _a priori_ clear if the first line, `type ('before_top,
'before, 'result_top, 'result) kinstr =`, should be considered
generated or not. We have chosen to move it to the non-generated part
of the output so the interpolated string for this GADT looks like

```
type ('before_top, 'before, 'result_top, 'result) kinstr =
%{String.concat "\n" kinstr_gadt_cases}
```

We made this choice for two reasons:
- It makes the relationship between the type being defined and the
  code generating the cases more explicit.
- We don't need to handle toplevel statements in the fragment of the
  Ocaml syntax formalized in the Ocaml_syntax module. This makes this
  module much lighter.


