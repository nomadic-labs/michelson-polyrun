
# Data-model spec

The data model aims to be a central knowledge data-base about Michelson language
that the backends require to generate Michelson runtimes.

Currently we forsee the following need:
- Michelson type description
  - concrete syntax
  - type arity
  - type attributes (comparable, packable, etc)
  - constraints on type's parameter
  - Internal representation in host languages
  - structure of the type
    - names and arities of algebraic-types constructors
  - doc strings

- Instruction
  - instruction name
  - concrete syntax
  - constructor_name
  - overloading
  - typing rules
  - semantics rules
  - associated host functions
  - doc strings

# Input spec
The input should be sufficient to recompute the data-model.  

For now we use the
language description from the
[michelson-reference](https://gitlab.com/tezos/michelson-reference) project
extended with some `ocaml_repr` fields.
