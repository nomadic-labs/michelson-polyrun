# Architecture of the Michelson-Polyrun prototype

This is a schema of the goal architecture of the prototype (not yet fully implemented).


```
input: michelson.json
  |
  | Parsing
  V
----------------------------------------
| Michelson_descr.primitive list +     |
| Michelson_descr.data_constr list +   |  Lightweight validation
| Michelson_descr.type list +          |-------------------------> OK / Error
| Michelson_descr.instruction list +   |  of the consistency of
| Michelson_descr.macros list +        |       the input
| Michelson_descr.continuation list    |
----------------------------------------
  |
  | Enriching the data model
  V
-------------------------------------------------------------------------------------------------
| Michelson_data_model.(primitive, data_constr, ...) list                                       |
-------------------------------------------------------------------------------------------------
  |                              |                        |                                   |
  | Protocol backend             | Annotation backend     | Tezos-on-tezos backend            | Text backend
  V                              V                        V                                   |
-------------------------------------------  ----------------------------------------------   |
| Ocaml_syntax.(GADTs + rec functions)    |  | Rust_syntax.(type defs + rec functions)    |   |
-------------------------------------------  ----------------------------------------------   |
  |                              |                        |                                   |
  | Ocaml pretty printing        |                        | Rust pretty printing              |
  V                              V                        V                                   V
script_ir_*.ml                 michelson_v1_annots.ml   michelson_interpreter.rs            michelson.txt
```

The input is a collection of files describing several aspects of the
language: grammar, typing, and semantics in Ott; details about the
type system, backend-specific information, documentation, gas model,
examples, tests, and history in Yaml.

The parsing phase converts these files into OCaml values whose types
are defined in the Michelson_desc module. Many fields in these
descriptions are optional so there is a lot of information to
reconstruct from the given fields.

The lightweight validation phase checks the consistency of the
description; for example it checks that types appearing in the typing
rules of instructions have been declared and are used with the
expected arity.

The next phase is about enriching the data model for the knowledge of
the Michelson language. It is backend-agnostic.

Then several backends -- ad-hoc code-generators differing greatly in role and in
target language -- take the data model as input to produce code in one
of the available ASTs representing the syntax of the fragment of the
language that we need (modules XXX_syntax). Typically they will
provide ways to define (generalized) algebraic data types and
(mutually) recursive functions over them.

Finally, the pretty-printing phase converts the backend ASTs to textual
representations.
