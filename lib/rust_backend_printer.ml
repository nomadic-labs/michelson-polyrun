let print_rust_output ~print_file tys instrs =
  List.iter
    (fun (filename, fn) -> print_file ~filename (fn tys instrs))
    [
      ("types.rs", Types_rs_template.print);
      ("interpreter.rs", Interpreter_template.print);
      ("instructions/algebraic.rs", Algebraic_rs_template.print);
    ]
