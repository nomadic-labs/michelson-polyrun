(* Fragment of the OCaml syntax that we target. *)

type typeexpr =
  | Typevar of string
  | Typewildcard
  | Typeconstr of typeexpr list * string
  | Arrow of typeexpr * typeexpr
  | Prod of typeexpr list
  | Polytype of string list * typeexpr
  | Extra of string

let type_constant s = Typeconstr ([], s)

type constant = String_literal of string | Constant of string

type pattern =
  | Wildcard
  | PVariable of string
  | PApp of string * pattern list
  | PRecord of string list

type expr =
  | Variable of string
  | Constant of constant
  | Tuple of expr list
  | App of {func : expr; args : expr list; tailcall : bool}
  | Infix of expr * string * expr
  | Match of expr * pattern_matching list
  | Fun of string list * expr
  | Function of pattern_matching list

and pattern_matching = {
  pattern : pattern;
  when_clause : expr option;
  body : expr;
}

let app ?(tailcall = false) func args = App {func; args; tailcall}

type let_binding = {
  value_name : string;
  parameters : string list;
  type_ : typeexpr option;
  body : expr;
}

type function_definition = {rec_flag : bool; bindings : let_binding list}

type record_or_product_type =
  | Record of (string * typeexpr) list
  | Product of typeexpr list

type typedef_body =
  | Alias of typeexpr
  | Record of (string * typeexpr) list
  | Variant of (string * record_or_product_type) list
  | GADT of (string * record_or_product_type * typeexpr) list

type toplevel_definition = Function_definition of function_definition

(* Collect the variables which are bounded by a pattern. *)
let rec bounded_vars_in_pattern = function
  | Wildcard -> []
  | PVariable v -> [v]
  | PApp (_constr, l) -> List.flatten (List.map bounded_vars_in_pattern l)
  | PRecord l -> l

let sorted_bounded_vars_in_pattern patt =
  List.sort String.compare @@ bounded_vars_in_pattern patt

(* Collect the variables appearing free in an expression. *)
let rec free_vars = function
  | Variable x -> [x]
  | Constant _ -> []
  | Tuple l -> List.flatten (List.map free_vars l)
  | App {func; args; tailcall = _} ->
      free_vars func @ List.flatten (List.map free_vars args)
  | Infix (e1, _, e2) -> free_vars e1 @ free_vars e2
  | Match (e, matchings) ->
      free_vars e @ List.flatten (List.map free_vars_in_matching matchings)
  | Fun (vars, e) ->
      let vs = free_vars e in
      List.filter (fun v -> not @@ List.mem v vars) vs
  | Function matchings ->
      List.flatten (List.map free_vars_in_matching matchings)

(* Collect the variables appearing free in a pattern matching. *)
and free_vars_in_matching {pattern; when_clause; body} =
  let vs =
    (match when_clause with
    | Some when_clause -> free_vars when_clause
    | None -> [])
    @ free_vars body
  in
  let bounded = bounded_vars_in_pattern pattern in
  List.filter (fun v -> not @@ List.mem v bounded) vs

(* [remove_unused_vars_in_pattern vars patt] is the
   same pattern as [patt] but the variables which appear in
   [patt] and not in [vars] are replaced by wildcards.
   If after this replacement we get a pattern of the form
   [Constr (_, _, _)], we replace it by [Constr _]. *)
let rec remove_unused_vars_in_pattern vars = function
  | Wildcard -> Wildcard
  | PVariable v -> if List.mem v vars then PVariable v else Wildcard
  | PApp (constr, []) -> PApp (constr, [])
  | PApp (constr, l) ->
      let l = List.map (remove_unused_vars_in_pattern vars) l in
      let l = if List.for_all (( = ) Wildcard) l then [Wildcard] else l in
      PApp (constr, l)
  | PRecord l -> PRecord (List.filter (fun v -> List.mem v vars) l)

let remove_unused_vars_in_matching {pattern; when_clause; body} =
  let vars =
    (match when_clause with
    | Some when_clause -> free_vars when_clause
    | None -> [])
    @ free_vars body
  in
  {pattern = remove_unused_vars_in_pattern vars pattern; when_clause; body}

(* This function is used to avoid repetitions of bodies in pattern
   matching lists; it replaces `| patt1 -> body | patt2 -> body`
   by `| patt1 | patt2 -> body` when patt1 and patt2 bind
   the same set of variables.

   This function preserves the order of the patterns so it does nothing
   on patterns associated to the same body when they are not next to
   each other: `| patt1 -> body | patt2 -> body' | patt3 -> body` is
   unchanged.
*)
let rec condense_pattern_matching_list (l : pattern_matching list) :
    ((pattern * expr option) list * string list * expr) list =
  match l with
  | [] -> []
  | [a] ->
      let {pattern; when_clause; body} = remove_unused_vars_in_matching a in
      [([(pattern, when_clause)], sorted_bounded_vars_in_pattern pattern, body)]
  | a :: l -> (
      let a = remove_unused_vars_in_matching a in
      match condense_pattern_matching_list l with
      | [] -> assert false
      | (patts, bounded_vars, body) :: l ->
          let a_patt_vars = sorted_bounded_vars_in_pattern a.pattern in
          if a.body = body && List.equal String.equal a_patt_vars bounded_vars
          then ((a.pattern, a.when_clause) :: patts, bounded_vars, body) :: l
          else
            ([(a.pattern, a.when_clause)], a_patt_vars, a.body)
            :: (patts, bounded_vars, body)
            :: l)

let rec print_typeexpr fmt = function
  | Typevar alpha -> Format.fprintf fmt "'%s" alpha
  | Typewildcard -> Format.fprintf fmt "_"
  | Typeconstr ([], constr) -> Format.fprintf fmt "%s" constr
  | Typeconstr ([arg], constr) ->
      Format.fprintf fmt "@[%a %s@]" print_typeexpr arg constr
  | Typeconstr (args, constr) ->
      Format.fprintf
        fmt
        "@[(@[%a@]) %s@]"
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
           print_typeexpr)
        args
        constr
  | Arrow (a, b) ->
      Format.fprintf
        fmt
        "@[%a ->@ %a@]"
        print_typeexpr_paren
        a
        print_typeexpr_paren
        b
  | Prod l ->
      Format.fprintf
        fmt
        "@[%a@]"
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt " *@ ")
           print_typeexpr_paren)
        l
  | Polytype (l, t) ->
      Format.fprintf
        fmt
        "@[type %a. %a@]"
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt " ")
           (fun fmt s -> Format.fprintf fmt "%s" s))
        l
        print_typeexpr
        t
  | Extra s -> Format.fprintf fmt "%s" s

and print_typeexpr_paren fmt te =
  match te with
  | Typevar _ | Typeconstr _ | Typewildcard -> print_typeexpr fmt te
  | Arrow _ | Prod _ | Extra _ | Polytype _ ->
      Format.fprintf fmt "@[(%a)@]" print_typeexpr te

let print_constant fmt = function
  | String_literal s -> Format.fprintf fmt "\"%s\"" (String.escaped s)
  | Constant s -> Format.fprintf fmt "%s" s

let rec print_pattern fmt = function
  | Wildcard -> Format.fprintf fmt "_"
  | PVariable v -> Format.fprintf fmt "%s" v
  | PApp (constr, []) -> Format.fprintf fmt "%s" constr
  | PApp (constr, [arg]) ->
      Format.fprintf fmt "@[%s %a@]" constr print_pattern arg
  | PApp (constr, args) ->
      Format.fprintf
        fmt
        "@[%s (@[%a@])@]"
        constr
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
           print_pattern)
        args
  | PRecord l ->
      Format.fprintf
        fmt
        "{@[%a;@ _}@]"
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt ";@ ")
           (fun fmt label -> Format.fprintf fmt "%s" label))
        l

let rec print_expr fmt = function
  | Constant c -> print_constant fmt c
  | Variable v -> Format.fprintf fmt "%s" v
  | Tuple args ->
      Format.fprintf
        fmt
        "@[(@[%a@])@]"
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
           print_expr)
        args
  | App {func; args; tailcall} -> (
      let print_func fmt func =
        if tailcall then
          Format.fprintf fmt "(%a [@ocaml.tailcall])" print_expr func
        else print_expr_paren fmt func
      in
      match args with
      | [] -> print_func fmt func
      | [arg] ->
          Format.fprintf fmt "@[%a %a@]" print_func func print_expr_paren arg
      | args ->
          Format.fprintf
            fmt
            "@[%a @[%a@]@]"
            print_func
            func
            (Format.pp_print_list
               ~pp_sep:(fun fmt () -> Format.fprintf fmt "@ ")
               print_expr_paren)
            args)
  | Infix (a, op, b) ->
      Format.fprintf
        fmt
        "@[%a %s@ %a@]"
        print_expr_paren
        a
        op
        print_expr_paren
        b
  | Match (a, matchings) ->
      Format.fprintf
        fmt
        "@[match@ %a@ with@;@[<v 0>%a@]@]"
        print_expr
        a
        print_pattern_matching_list
        matchings
  | Fun (vars, e) ->
      Format.fprintf
        fmt
        "@[fun %a ->@ %a@]"
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt " ")
           (fun fmt s -> Format.fprintf fmt "%s" s))
        vars
        print_expr
        e
  | Function matchings ->
      Format.fprintf
        fmt
        "@[function@;@[<v 0>%a@]@]"
        print_pattern_matching_list
        matchings

and print_expr_paren fmt e =
  match e with
  | Constant _ | Variable _ | Tuple _ -> print_expr fmt e
  | App _ | Infix _ | Match _ | Fun _ | Function _ ->
      Format.fprintf fmt "(%a)" print_expr e

and print_when_clause fmt = function
  | None -> ()
  | Some cond -> Format.fprintf fmt "@ @[when %a@]" print_expr cond

and print_pattern_matching fmt a =
  let {pattern; when_clause; body} = remove_unused_vars_in_matching a in
  Format.fprintf
    fmt
    "@[| %a%a ->@ %a@]@;"
    print_pattern
    pattern
    print_when_clause
    when_clause
    print_expr
    body

and print_pattern_matching_list fmt l =
  Format.pp_print_list
    ~pp_sep:(fun _fmt () -> ())
    (fun fmt (patts, _bounded_vars, body) ->
      Format.fprintf
        fmt
        "@[%a ->@ %a@]@;"
        (fun fmt patts ->
          Format.pp_print_list
            ~pp_sep:(fun _fmt () -> ())
            (fun fmt (patt, when_clause) ->
              Format.fprintf
                fmt
                "| %a%a"
                print_pattern
                patt
                print_when_clause
                when_clause)
            fmt
            patts)
        patts
        print_expr
        body)
    fmt
    (condense_pattern_matching_list l)

let print_product_type fmt args =
  match args with
  | [] -> ()
  | args ->
      (Format.pp_print_list
         ~pp_sep:(fun fmt () -> Format.fprintf fmt " *@ ")
         print_typeexpr)
        fmt
        args

let print_record_type fmt args =
  Format.fprintf
    fmt
    "@[<v 2>{@;%a@;@]}"
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt "@;")
       (fun fmt (label, ty_) ->
         Format.fprintf fmt "@[%s :@ %a;@]" label print_typeexpr ty_))
    args

let print_record_or_product_type fmt (rorp : record_or_product_type) =
  match rorp with
  | Record args -> print_record_type fmt args
  | Product args -> print_product_type fmt args

let print_typedef_body_gadt_case fmt (constructor, args, ty) =
  Format.fprintf
    fmt
    "@[| @[<v 2>%s :@ %a%a%a@]@]@;"
    constructor
    print_record_or_product_type
    args
    (fun fmt () ->
      match args with Product [] -> () | _ -> Format.fprintf fmt " ->@ ")
    ()
    print_typeexpr
    ty
