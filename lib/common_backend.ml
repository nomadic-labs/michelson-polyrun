module Set = Set.Make (String)

let diff ~filter_table table selected_names =
  let table_names =
    Hashtbl.to_seq table
    |> Seq.filter_map (fun (a, b) -> if filter_table a b then Some a else None)
    |> Set.of_seq
  in
  let selected_names = Set.of_list selected_names in
  (Set.diff table_names selected_names, Set.diff selected_names table_names)

let pp_cases ppf = function
  | [] -> ()
  | cases ->
      Format.fprintf
        ppf
        "@[%s@[%a@]%s@]@."
        Colors.FG.gray
        Format.(pp_print_list ~pp_sep:Format.pp_print_space pp_print_string)
        cases
        Colors.reset

let print_warn action ~__LOC__ context = function
  | [] -> ()
  | cases ->
      Format.eprintf
        "@[<v 8>%s%s/!\\ The following items where %s in %s cases:%s@[%a@]@."
        Colors.FG.yellow
        __LOC__
        action
        context
        Colors.reset
        pp_cases
        cases

let print_ignored = print_warn "ignored"

let print_unknown = print_warn "selected but are unkown in the model"

let print_ignored_added =
  print_warn "added at the end as they where not explicitly ordered"

(** [check_iteration_order context ty_table ty_names] returns true iff
   [ty_names] is a permutation of the keys of the [ty_table]
   hashtable. Defining such permutations is useful to repproduce the
   order in which the cases are treated in the protocol code (which is
   not consistent). The [context] string is used to provide context
   information in error messages. *)
let check_iteration_order ~__LOC__ context ~filter_table table selected_names =
  let ignored, unknown = diff ~filter_table table selected_names in
  if Set.is_empty ignored && Set.is_empty unknown then true
  else (
    print_ignored ~__LOC__ context (Set.elements ignored) ;
    print_unknown ~__LOC__ context (Set.elements unknown) ;
    false)

(** [assert_completeness context table names] provoke an assertion
    failure when [names] is not a permutation of the keys of the [table]
    hashtable. The [context] string is used to provide context information in
    error messages. *)
let assert_completeness ~__LOC__ context ~filter_table table selected_names =
  assert (
    check_iteration_order ~__LOC__ context ~filter_table table selected_names) ;
  selected_names

(** [check_completeness context table names] output warnings when
    [names] is not a permutation of the keys of the [table] hashtable. The
    [context] string is used to provide context information in error
    messages. *)
let check_completeness ~__LOC__ context ~filter_table table selected_names =
  ignore
    (check_iteration_order ~__LOC__ context ~filter_table table selected_names) ;
  selected_names

(** [check_completeness context table names] output warnings when
    [names] is not a permutation of the keys of the [table] hashtable.
    The keys from [table] are added at the end of the returned [names].
    If a name is missing in [table] the function fails.
    The [context] string is used to provide context information in error
    messages. *)
let complete_selected_names ~__LOC__ context ~filter_table table selected_names
    =
  let ignored, unknown = diff ~filter_table table selected_names in
  let ignored_elements = Set.elements ignored in
  print_ignored_added ~__LOC__ context ignored_elements ;
  if Set.is_empty unknown then selected_names @ ignored_elements
  else (
    print_unknown ~__LOC__ context (Set.elements unknown) ;
    failwith
      (__LOC__ ^ " " ^ context ^ ": Some elements are unkown in the data-model"))

(** [narrow_to_available warn context table names] returns a list narrowed down
    to the elements available in the table.
     If warn is set to true, warnings are emitted when [names] is not a
     permutation of the keys of the [table] hashtable.
     The [context] string is used to provide context information in error
     messages. *)
let narrow_to_available ~__LOC__ context ~warn ~filter_table table
    selected_names =
  let ignored, unknown = diff ~filter_table table selected_names in
  let ignored_elements = Set.elements ignored in
  if warn then print_ignored_added ~__LOC__ context ignored_elements ;
  if Set.is_empty unknown then selected_names
  else (
    if warn then print_unknown ~__LOC__ context (Set.elements unknown) ;
    List.filter (fun name -> not @@ Set.mem name unknown) selected_names)

let wrap_selected_names ?(action = `Available_only true) ~__LOC__ context
    ?(filter_table = fun _ _ -> true) table selected_names =
  (match action with
  | `Available_only warn -> narrow_to_available ~warn ~filter_table
  | `Warn -> check_completeness ~filter_table
  | `Fail -> assert_completeness ~filter_table
  | `Complete -> complete_selected_names ~filter_table)
    ~__LOC__
    context
    table
    selected_names
