(* Definition of the syntax of types. *)

(* When defining a type, we must be explicit about comparability because it is enforced by the OCaml GADT.
   There are two aspects to this:
   - we must declare which type arguments are required to be comparable (for example map keys)
   - we must define in which cases the type we are defining is comparable.
*)

(* An argument to a Michelson type constructor is either a type, a comparable type, or a memo size. *)
type type_argument =
  | Any_type of string
  | Comparable_type of string
  | Memo_size
[@@deriving show]

type comparable_flag =
  (* {Yes} means that this type is always comparable *)
  | Yes
  (* {No} means that this type is never comparable *)
  | No
  (* {List [v1; ...; vn]} means that this type is comparable iff its parameters named {v1} ... {vn} are all comparable. *)
  | List of string list
[@@deriving show]

(* Grammar *)

type type_def = {
  ty_name : string; (* The name of the type in Michelson *)
  ty_arity : type_argument list;
      (* The names and comparability of the arguments of this type constructor *)
  ty_ocaml_repr : string; (* The ocaml type implementing this Michelson type. *)
  ty_rust_repr : string;
  ty_comparable : comparable_flag; (* When is this type comparable *)
}
[@@deriving show]

(* A helper to construct values of type {type_def} using some default arguments. *)
let define_ty ~name ~arity ?ocaml_repr ?rust_repr ~comparable () =
  let ty_ocaml_repr = Option.value ~default:name ocaml_repr in
  let ty_rust_repr =
    Option.value ~default:(String.capitalize_ascii name) rust_repr
  in
  {
    ty_name = name;
    ty_arity = arity;
    ty_ocaml_repr;
    ty_rust_repr;
    ty_comparable = comparable;
  }

type ty = Ty_var of string | Ty_memo of string | Ty_app of string * ty list
[@@deriving show]

type stack_ty = ty list * string [@@deriving show]

type instr_param =
  | IA_type of string
    (* Type parameter for an instruction (for example "'b" in "LEFT 'b") *)
  | IA_comparable_type of string
    (* Same but the type must be comparable (for example "'a" in "EMPTY_SET 'a") *)
  | IA_depth of string
    (* depth parameter for an instruction (for example "n" in "DIP n") *)
  | IA_data of string
    (* Data parameter for an instruction (for example "'x" in "PUSH 'ty 'x") + its type *)
  | IA_code of string
    (* Named code parameter for an instruction (for example "'i" in "DIP {'i}") *)
  | IA_extra of string
(* Some extra data available from the OCaml GADT but not visible in the concrete syntax *)
[@@deriving show]

type instr = IVar of string | IApp of string * instr_param list
[@@deriving show]

let instr_name = function IVar v | IApp (v, _) -> v

type typing_judgment = {
  typing_judgment_instr : instr;
  typing_judgment_input : stack_ty;
  typing_judgment_output : stack_ty;
}
[@@deriving show]

type typing_premisse = TP_data of string * ty | TP_instr of typing_judgment
[@@deriving show]

type typing_rule = {
  name : string;
  typing_rule_premisses : typing_premisse list;
  typing_rule_conclusion : typing_judgment;
}
[@@deriving show]

type instruction_def = {
  instr_concrete_name : string;
  instr_constructor_name : string;
      (* The name of the corresponding constructor in the OCaml GADT *)
  instr_params : instr_param list;
      (* The list of params of the instruction in the OCaml GADT. *)
  instr_typing_rules : typing_rule list;
}
[@@deriving show]
