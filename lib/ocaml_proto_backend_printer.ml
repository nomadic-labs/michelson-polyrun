let print_ocaml_output ~print_file tys instrs =
  List.iter
    (fun (filename, printing_fun) ->
      print_file ~filename (printing_fun tys instrs))
    [
      ("script_typed_ir.ml", Script_typed_ir_template.print);
      ("script_ir_translator.ml", Script_ir_translator_template.print);
    ]
