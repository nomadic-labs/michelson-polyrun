{
open Input_rule_parser
}

let nonneg = ['0'-'9']+
let id = ['a'-'z''A'-'Z''0'-'9''_']*
let stackty = ['A'-'B']
let whitespace = [' ' '\t' '\n']

rule lex = parse
  | ('n' nonneg* as n) { DEPTH n  }
  | "ctx" { CTX }
  | ":-" { VDASH }
  | "::" { DOUBLE_COLON }
  | ":" { COLON }
  | "=>" { FAT_ARROW }
  | "(" { LPAREN }
  | ")" { RPAREN }
  | "{" { LBRACE }
  | "}" { RBRACE }
  | whitespace { lex lexbuf }
  | ("instr" nonneg* as v) { CODE_VAR v }
  | ('x' id as v) { DATA_VAR v }
  | (stackty nonneg* as v) { STACK_VAR v }
  | (("vty"|"ty") nonneg* as v) { TY_VAR v }
  | ("ms" as n) { MEMO n }
  | (("cty"|"kty") nonneg* as v) { CTY_VAR v }
  | id as c { CONST c }
  | _ as c { invalid_arg (Printf.sprintf "Lexer: %c" c) }
  | eof { EOF }
