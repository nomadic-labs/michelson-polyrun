open Log

let input : Ezjsonm.value =
  let channel = open_in "bin/michelson_input.json" in
  let res = Ezjsonm.from_channel channel in
  close_in channel ;
  res

let tys : Michelson_descr.type_def list =
  let dict = Ezjsonm.get_dict input in
  let types = List.assoc "types" dict in
  Ezjsonm.get_list
    (fun ty ->
      let dict = Ezjsonm.get_dict ty in
      let name = Ezjsonm.get_string (List.assoc "ty" dict) in
      let name_with_args = Ezjsonm.get_string (List.assoc "ty_args" dict) in
      let name_and_args = String.split_on_char ' ' name_with_args in
      assert (List.hd name_and_args = name) ;
      let args = List.tl name_and_args in
      let arity =
        List.map
          (fun arg_name ->
            if arg_name = "n" then Michelson_descr.Memo_size
            else if arg_name = "kty" || arg_name = "cty" then
              Michelson_descr.Comparable_type arg_name
            else Michelson_descr.Any_type arg_name)
          args
      in
      let comparable =
        match List.assoc "comparable" dict with
        | `Bool true -> Michelson_descr.Yes
        | `Bool false -> Michelson_descr.No
        | a -> Michelson_descr.List (Ezjsonm.get_strings a)
      in
      let ocaml_repr =
        try Some (Ezjsonm.get_string (List.assoc "ocaml_repr" dict)) with
        | Not_found -> None
        | Ezjsonm.Parse_error _ ->
            Printf.eprintf
              "Warning: Ignored field \"ocaml_repr\" in type %S due to bad \
               format, string expected.\n"
              name ;
            None
      in
      let rust_repr =
        try Some (Ezjsonm.get_string (List.assoc "rust_repr" dict)) with
        | Not_found -> None
        | Ezjsonm.Parse_error _ ->
            Printf.eprintf
              "Warning: Ignored field \"rust_repr\" in type %S due to bad \
               format, string expected.\n"
              name ;
            None
      in
      Michelson_descr.define_ty
        ~name
        ~arity
        ~comparable
        ?ocaml_repr
        ?rust_repr
        ())
    types

let wrap_parsing context parse s =
  let lexbuf = Lexing.from_string s in
  try parse Input_rule_lexer.lex lexbuf
  with _ ->
    let start = Lexing.lexeme_start lexbuf in
    let last = Lexing.lexeme_end lexbuf in
    let underline =
      String.init (last + 1) (fun i ->
          if start <= i && i <= last then '^' else ' ')
    in
    Format.eprintf
      "@.%s error for %s, @.at position %i-%i on token %s@.%s@.%s@."
      context
      s
      start
      last
      (String.sub s start (last - start))
      s
      underline ;
    failwith "input_parser"

let read_typing_judgment s =
  wrap_parsing "Parsing judgment" Input_rule_parser.typing_judgment s

let read_typing_premisse s =
  wrap_parsing "Parsing premisse" Input_rule_parser.typing_premisse s

let parse_typing_rule ~(premises : string list) ~(conclusion : string) =
  (List.map read_typing_premisse premises, read_typing_judgment conclusion)

let parse_rule rule =
  let dict = Ezjsonm.get_dict rule in
  let name = List.assoc "name" dict |> Ezjsonm.get_string in
  let typing_rule_premisses, typing_rule_conclusion =
    parse_typing_rule
      ~premises:
        (List.assoc "premises" dict |> Ezjsonm.get_list Ezjsonm.get_string)
      ~conclusion:(List.assoc "conclusion" dict |> Ezjsonm.get_string)
  in
  Michelson_descr.{name; typing_rule_premisses; typing_rule_conclusion}

let parse_op_args op_args : string * Michelson_descr.instr_param list =
  match
    wrap_parsing "Parsing op_args line" Input_rule_parser.op_args op_args
  with
  | IVar constr -> (constr, [])
  | IApp (constr, args) -> (constr, args)

let match_in_list name list =
  List.exists (fun pat -> Str.string_match (Str.regexp (pat ^ "$")) name 0) list

let instrs ?(whitelist = [".*"]) ?(blacklist = []) () :
    Michelson_descr.instruction_def list =
  let dict = Ezjsonm.get_dict input in
  let instrs = List.assoc "instructions" dict in
  Ezjsonm.get_list
    (fun instr ->
      let dict = Ezjsonm.get_dict instr in
      let instr_constructor_name = Ezjsonm.get_string (List.assoc "op" dict) in
      log "Input_parser: Constructing description of %s" instr_constructor_name ;
      if
        (not @@ match_in_list instr_constructor_name whitelist)
        || match_in_list instr_constructor_name blacklist
      then None
      else
        Some
          (let instr_concrete_name, instr_params =
             parse_op_args (Ezjsonm.get_string (List.assoc "op_args" dict))
           in
           let instr_typing_rules =
             Ezjsonm.get_list parse_rule (List.assoc "ty" dict)
           in
           Michelson_descr.
             {
               instr_concrete_name;
               instr_constructor_name;
               instr_params;
               instr_typing_rules;
             }))
    instrs
  |> List.filter_map Fun.id
