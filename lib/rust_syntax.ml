(** The AST described here is a rough and partial translation of Rustc AST that
    you can explore at
    https://doc.rust-lang.org/beta/nightly-rustc/rustc_ast/ast/index.html *)
type borrow_kind = Ref | Raw

type mutability = Const | Mutable

type binop = Add | Sub

type unop = Deref | Not | Neg

type path_segment = string

type path = path_segment list

type pat =
  | Wild
  | Ident of mutability * string
  | Tuple of pat list
  | TupleStruct of path * pat list

type ty =
  | T_ImplicitSelf
  | T_Ref of ty * mutability
  | T_Path of path
  | T_Enum of string * enum_case list

and enum_case = string * ty list

type param = {param_pat : pat; param_ty : ty}

type arm = pat * expr

and expr =
  | E_Block of block
  | E_Call of expr * expr list
  | E_Field of expr * string
  | E_BinOp of binop * expr * expr
  | E_UnOp of unop * expr
  | E_AddrOf of borrow_kind * mutability * expr
  | E_ForLoop of pat * expr * block
  | E_Path of path
  | E_MacCall of mac_call
  | E_Match of expr * arm list
  | E_If of expr * expr * expr option
  | E_Ret of expr option
  | E_MethodCall of expr * path_segment * expr list
  | E_Tup of expr list
  | E_Raw of string
  | E_Try of expr

and mac_call = {
  (* TODO: args probably shouldn't be of type expr *)
  path : path;
  args : expr list;
}

and local_kind = Decl | Init of expr

and local = {local_pat : pat; local_ty : ty option; local_kind : local_kind}

and stmt = Local of local | Expr of expr | Semi of expr

and block = stmt list

type fnsig = {inputs : param list; output : ty option}

type item =
  | Impl of {name : string; of_trait : string option; items : item list}
  | TyAlias of {name : string; typedef : ty}
  | Fn of {name : string; signature : fnsig; body : block option}
  | MacCall of {name : string; args : expr list}

let print_binop o op =
  let op = match op with Add -> '+' | Sub -> '-' in
  Format.pp_print_char o op

let print_unop o op =
  let op = match op with Deref -> '*' | Not -> '!' | Neg -> '-' in
  Format.pp_print_char o op

let print_path output segments =
  Format.pp_print_list
    ~pp_sep:(fun o () -> Format.pp_print_string o "::")
    Format.pp_print_string
    output
    segments

let rec print_pat output pat =
  match pat with
  | Wild -> Format.pp_print_char output '_'
  | Ident (Mutable, name) -> Format.fprintf output "mut %s" name
  | Ident (Const, name) -> Format.pp_print_string output name
  | Tuple pats ->
      Format.fprintf
        output
        "(%a)"
        (Format.pp_print_list
           ~pp_sep:(fun o () -> Format.pp_print_string o ", ")
           print_pat)
        pats
  | TupleStruct (path, pats) ->
      Format.fprintf output "%a%a" print_path path print_pat (Tuple pats)

let rec print_ty output ty =
  match ty with
  | T_ImplicitSelf -> Format.pp_print_string output "self"
  | T_Ref (ty, Mutable) -> Format.fprintf output "&mut %a" print_ty ty
  | T_Ref (ty, Const) -> Format.fprintf output "&%a" print_ty ty
  | T_Path path -> print_path output path
  | T_Enum (name, cases) ->
      Format.fprintf
        output
        "enum %s { @[%a@] }"
        name
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt ",")
           print_enum_case)
        cases

and print_enum_case output (name, args) =
  Format.fprintf
    output
    "@[%s(%a)@]"
    name
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt ",")
       print_ty)
    args

let rec print_stmt o = function
  | Expr expr -> print_expr o expr
  | Local local -> print_local o local
  | Semi expr -> Format.fprintf o "%a;" print_expr expr

and print_local o {local_pat; local_ty; local_kind} =
  Format.fprintf o "let %a" print_pat local_pat ;

  match local_ty with
  | Some local_ty -> Format.fprintf o ": %a" print_ty local_ty
  | None -> (
      match local_kind with
      | Init expr -> Format.fprintf o " = %a;" print_expr expr
      | Decl ->
          () ;

          Format.pp_print_string o ";@,")

and print_arm o (pat, expr) =
  Format.fprintf o "@[%a => %a@]" print_pat pat print_expr expr

and print_arms_list o arms_list =
  (Format.pp_print_list
     ~pp_sep:(fun o () -> Format.pp_print_string o ",@,")
     print_arm)
    o
    arms_list

and print_expr o expr =
  match expr with
  | E_MethodCall (expr, ident, args) ->
      Format.fprintf
        o
        "%a.%s(%a)"
        print_expr
        expr
        ident
        (Format.pp_print_list
           ~pp_sep:(fun o () -> Format.pp_print_string o ", ")
           print_expr)
        args
  | E_Path path -> print_path o path
  | E_Call (callee, args) ->
      Format.fprintf
        o
        "%a(%a)"
        print_expr
        callee
        (Format.pp_print_list
           ~pp_sep:(fun o () -> Format.pp_print_string o ", ")
           print_expr)
        args
  | E_Block block ->
      Format.fprintf o "{ %a }" (Format.pp_print_list print_stmt) block
  | E_Field (expr, field) -> Format.fprintf o "%a.%s" print_expr expr field
  | E_BinOp (op, lhs, rhs) ->
      Format.fprintf o "(%a %a %a)" print_expr lhs print_binop op print_expr rhs
  | E_UnOp (op, operand) ->
      Format.fprintf o "(%a%a)" print_unop op print_expr operand
  | E_ForLoop (pat, expr, block) ->
      Format.fprintf
        o
        "for %a in %a %a"
        print_pat
        pat
        print_expr
        expr
        print_block
        block
  | E_Match (expr, arms) ->
      Format.fprintf o "match %a { %a }" print_expr expr print_arms_list arms
  | E_If (cond, if_branch, else_branch) ->
      Format.fprintf o "if %a { %a }" print_expr cond print_expr if_branch ;
      Option.iter (Format.fprintf o " else { %a }" print_expr) else_branch
  | E_Ret expr ->
      Format.fprintf o "return " ;
      Option.iter (print_expr o) expr
  | E_AddrOf (borrow_kind, mutability, expr) ->
      (match borrow_kind with
      | Ref -> Format.pp_print_string o "&"
      | Raw -> Format.pp_print_string o "&raw ") ;

      (match mutability with
      | Const -> ()
      | Mutable -> Format.pp_print_string o "mut ") ;

      print_expr o expr
  | E_MacCall {path; args} ->
      Format.fprintf
        o
        "%a!(%a)"
        print_path
        path
        (Format.pp_print_list
           ~pp_sep:(fun o () -> Format.pp_print_string o ", ")
           print_expr)
        args
  | E_Tup elements ->
      Format.fprintf
        o
        "(%a)"
        (Format.pp_print_list
           ~pp_sep:(fun o () -> Format.pp_print_string o ", ")
           print_expr)
        elements
  | E_Raw code -> Format.pp_print_string o code
  | E_Try expr -> Format.fprintf o "%a?" print_expr expr

and print_block output stmts =
  Format.fprintf output "{ %a }" (Format.pp_print_list print_stmt) stmts

let rec print_item output item =
  let print_impl_trait o = Option.iter (Format.fprintf o "%s for") in
  let print_fn_param o {param_pat; param_ty} =
    match param_ty with
    | T_ImplicitSelf | T_Ref (T_ImplicitSelf, _) ->
        Format.fprintf o "%a" print_ty param_ty
    | _ -> Format.fprintf o "%a: %a" print_pat param_pat print_ty param_ty
  in
  let print_fn_signature o {inputs; output} =
    Format.fprintf
      o
      "(%a)"
      (Format.pp_print_list
         ~pp_sep:(fun o () -> Format.pp_print_string o ", ")
         print_fn_param)
      inputs ;
    Option.iter (Format.fprintf o " -> %a" print_ty) output
  in
  match item with
  | Impl {name; of_trait; items} ->
      Format.fprintf
        output
        "impl %a %s { %a }"
        print_impl_trait
        of_trait
        name
        (Format.pp_print_list print_item)
        items
  | Fn {name; signature; body} -> (
      Format.fprintf output "fn %s%a" name print_fn_signature signature ;
      match body with
      | Some body -> print_block output body
      | None -> Format.fprintf output ";")
  | TyAlias {name; typedef} ->
      Format.fprintf output "type %s = %a;" name print_ty typedef
  | MacCall {name; args} ->
      Format.fprintf
        output
        "@[%s!(%a);"
        name
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.pp_print_string fmt ", ")
           print_expr)
        args

module Helper = struct
  let seq_bind_ret expr_list =
    let rev_lst = List.rev expr_list in
    let rev_stmts =
      match rev_lst with
      | [] -> []
      | last :: prefix ->
          Expr last :: List.map (fun expr -> Semi (E_Try expr)) prefix
    in
    List.rev rev_stmts
end
