let camel_of_snake str =
  str |> String.split_on_char '_'
  |> List.map String.capitalize_ascii
  |> String.concat ""

let macro_call name args = Rust_syntax.MacCall {name; args}

let path segments = Rust_syntax.E_Path segments

open Michelson_data_model

module type Env = Env

module Types_rs (Env : Env) = struct
  open Env

  let all_declared_types =
    Common_backend.wrap_selected_names
      ~__LOC__
      "Rust - Enum stack items"
      tys
      [
        "unit";
        "bytes";
        "string";
        "int";
        "nat";
        "bool";
        "timestamp";
        "mutez";
        "address";
        "key";
        "key_hash";
        "signature";
        "chain_id";
        "contract";
        "operation";
        "option";
        "or";
        "pair";
        "list";
        "set";
        "map";
        "big_map";
        "lambda";
      ]

  let builtin_types =
    [
      "unit";
      "bool";
      "bytes";
      "string";
      "int";
      "nat";
      "mutez";
      "timestamp";
      "address";
      "key";
      "key_hash";
      "signature";
      "chain_id";
      "list";
      "set";
      "map";
      "lambda";
      "contract";
    ]

  let ord_types =
    ["unit"; "int"; "bool"; "bytes"; "string"; "nat"; "mutez"; "timestamp"]

  let not_comparable_types =
    Common_backend.wrap_selected_names
      ~__LOC__
      "Rust - not_comparable types"
      ~filter_table:(fun _ (ty : type_def) ->
        match ty.ty_comparable with No -> true | Yes | Var _ | And _ -> false)
      tys
      ["list"; "set"; "map"; "lambda"; "contract"; "big_map"; "operation"]

  let gen_type_declaration ty =
    let Michelson_data_model.{ty_name; ty_rust_repr; _} = ty in
    let macro_name =
      if List.mem ty_name ord_types then "define_item_ord" else "define_item"
    in
    macro_call
      macro_name
      [path [camel_of_snake ty_name ^ "Item"]; path [ty_rust_repr]]

  let gen_not_comparable fmt ty =
    let Michelson_data_model.{ty_name; ty_comparable; _} =
      Hashtbl.find Env.tys ty
    in
    assert (ty_comparable = Michelson_data_model.No) ;
    let item =
      macro_call "not_comparable" [path [camel_of_snake ty_name ^ "Item"]]
    in
    Rust_syntax.print_item fmt item

  let gen_polymorphic_type ty =
    let Michelson_data_model.{ty_name; ty_rust_repr; ty_arity; _} = ty in
    match ty_arity with
    | [] -> None
    | [_single] ->
        Some
          (macro_call
             "define_item_rec"
             [
               path [camel_of_snake ty_name ^ "Item"];
               path [camel_of_snake ty_rust_repr];
               path ["Type"];
             ])
    | more ->
        let inner_type =
          Rust_syntax.E_Tup
            (List.map (fun _ -> Rust_syntax.E_Path ["Type"]) more)
        in
        Some
          (macro_call
             "define_item_rec"
             [
               path [camel_of_snake ty_name ^ "Item"];
               path [camel_of_snake ty_rust_repr];
               inner_type;
             ])

  let gen_type fmt ty =
    let ty = Hashtbl.find Env.tys ty in
    let item =
      if not (List.mem ty.Michelson_data_model.ty_name builtin_types) then None
      else if List.length ty.Michelson_data_model.ty_arity > 0 then
        gen_polymorphic_type ty
      else Some (gen_type_declaration ty)
    in
    Option.iter (Rust_syntax.print_item fmt) item

  let gen_enum_stack_item fmt types =
    let open Rust_syntax in
    let stack_item =
      T_Enum
        ( "StackItem",
          List.map
            (fun ty ->
              (camel_of_snake ty, [T_Path [camel_of_snake ty ^ "Item"]]))
            types )
    in
    print_ty fmt stack_item

  (* List of types definition *)
  let types = List.map (Format.asprintf "%a" gen_type) builtin_types

  (* List of non-comparable types definition *)
  let not_comparable_types =
    List.map (Format.asprintf "%a" gen_not_comparable) not_comparable_types

  (* Enumeration of possible stack items *)
  let enum_stack_item =
    Format.asprintf "%a" gen_enum_stack_item all_declared_types
end

let find_instruction instrs name =
  try Hashtbl.find instrs name
  with Not_found ->
    Format.eprintf
      "@[<v 2>Rust_backend.find_instruction: Instruction %s not found in@  \
       @[%a@]@]@."
      name
      Format.(pp_print_list ~pp_sep:pp_print_space pp_print_string)
      (Hashtbl.to_seq_keys instrs |> List.of_seq) ;
    failwith "Rust_backend.find_instruction: instruction not found"

module Algebraic_rs (Env : Env) = struct
  open Env

  let pop_cast_of_ty ty =
    let open Michelson_data_model in
    match ty with
    | Ty_app (name, _) ->
        Rust_syntax.E_MacCall
          {
            path = ["pop_cast"];
            args = [E_Path ["stack"]; E_Path [camel_of_snake name]];
          }
    | Ty_var _ -> Rust_syntax.E_Try (E_MethodCall (E_Path ["stack"], "pop", []))
    | _ -> assert false

  let assign_pop_cast varname ty =
    let pop_cast = pop_cast_of_ty ty in
    Rust_syntax.(
      Local
        {
          local_pat = Ident (Const, varname);
          local_ty = None;
          local_kind = Init pop_cast;
        })

  let pure_interpreter instr body =
    let open Rust_syntax in
    Impl
      {
        name =
          camel_of_snake
            (String.lowercase_ascii
               instr.Michelson_data_model.instr_concrete_name);
        of_trait = Some "PureInterpreter";
        items =
          [
            Fn
              {
                name = "execute";
                signature =
                  {
                    inputs =
                      [
                        {
                          param_pat = Ident (Const, "self");
                          param_ty = T_Ref (T_ImplicitSelf, Const);
                        };
                        {
                          param_pat = Ident (Const, "stack");
                          param_ty = T_Ref (T_Path ["Stack"], Mutable);
                        };
                      ];
                    output = Some (T_Path ["Result<()>"]);
                  };
                body;
              };
          ];
      }

  open Michelson_data_model

  let collect_stack_prefix ((ty_prefix, _) : stack_ty)
      (stack_prefix : Semantics.stack_prefix) : Rust_syntax.stmt list =
    match stack_prefix with
    | Semantics.Runtime_depth ->
        failwith "unsupported semantic rule format: dynamic prefix length"
    | Semantics.Varlist stack_prefix ->
        (* if this assert ever breaks with ty_prefix being too large, replacing
           ty_prefix by its prefix of the expected length should be enough. *)
        assert (List.length ty_prefix = List.length stack_prefix) ;
        List.map2
          (fun ty varname -> assign_pop_cast varname ty)
          ty_prefix
          stack_prefix

  (* TODO: generate pop_cast and assign them to the right variable name*)

  let stmt_of_binding (binding : Semantics.var_bind) : Rust_syntax.stmt =
    match binding with
    | Semantics.Assign (_var, _expr) ->
        failwith "unsupported expression assignement"
    (* TODO : transform expr into rust expr and do assignment  *)
    | Semantics.Destruct ({constructor = "Pair"; vars = [left; right]}, var) ->
        Rust_syntax.(
          Local
            {
              local_pat = Tuple [Ident (Const, left); Ident (Const, right)];
              local_ty = None;
              local_kind = Init (E_MethodCall (E_Path [var], "unpair", []));
            })
        (* let (first, _) = val.unpair();) *)
    | Semantics.Destruct (_patterns, _var) ->
        failwith "unsupported algebraic destruction"
  (* TODO : depending on constructor, do actual pattern matching or call the right data accessor *)

  let gen_pushs output_prefix =
    let push_of var =
      Rust_syntax.E_Raw (Format.asprintf "stack.push(%s)" var)
    in
    let pushes = List.map push_of output_prefix in
    (* making all but last expr a statment in the rust error monad *)
    Rust_syntax.Helper.seq_bind_ret pushes

  let gen_interp (instr : instruction_def) =
    match instr.instr_semantics with
    (* Invariant: we rely on the semantic rule wellformedness: any used
       variable either comes from the stack prefix collection or from the bindings *)
    | {stack_prefix; bindings; output_prefix} ->
        let stack_collection_code =
          collect_stack_prefix instr.instr_type.input_type stack_prefix
        in
        let bindings_code =
          List.map (fun binding -> stmt_of_binding binding) bindings
        in
        let push_result_code = gen_pushs output_prefix in
        stack_collection_code @ bindings_code @ push_result_code

  (* TODO: This is temporary, while we're not able to generate it
     from semantics. *)
  let hard_coded_bodys =
    let open Rust_syntax in
    [
      ( "CDR",
        [
          Expr
            (E_Raw
               {|let (_, second) = val.unpair();
        stack.push(second)
     |});
        ] );
      ( "PAIR",
        [
          Expr
            (E_Raw
               {|let n = parse_arity(&self.n)?;
        let mut items: Vec<StackItem> = Vec::with_capacity(n);
        for _ in 0..n {
            items.push(stack.pop()?);
        }

        let pair = PairItem::from_items(items)?;
        stack.push(pair.into())
      |});
        ] );
      ( "UNPAIR",
        [
          Expr
            (E_Raw
               {|let n = parse_arity(&self.n)?;
        let items = val.into_items(n)?;
        for item in items.into_iter().rev() {
            stack.push(item)?;
        }
        Ok(())
      |});
        ] );
      ( "NONE",
        [
          Expr
            (E_Raw
               {|let item = OptionItem::none(&self.r#type);
        stack.push(item.into())
      |});
        ] );
      ( "SOME",
        [
          Expr
            (E_Raw
               {|let item = OptionItem::some(val);
        stack.push(item.into())
      |});
        ] );
      ( "LEFT",
        [
          Expr
            (E_Raw
               {|let res = OrItem::left(val, self.r#type.clone());
        stack.push(res.into())
      |});
        ] );
      ( "RIGHT",
        [
          Expr
            (E_Raw
               {|let res = OrItem::right(val, self.r#type.clone());
        stack.push(res.into())
      |});
        ] );
    ]

  let algebraic_instructions =
    List.map
      (fun instr ->
        ( instr,
          let instr = find_instruction instrs instr in
          gen_interp instr ))
      (Common_backend.wrap_selected_names
         ~__LOC__
         ~action:(`Available_only false)
         "Algebraic instructions with generated body"
         instrs
         ["CAR"])
    (* get Hard-coded bodies for other instructions *)
    @ List.map
        (fun instr_name ->
          ( instr_name,
            let body = List.assoc instr_name hard_coded_bodys in
            match
              (find_instruction instrs instr_name)
                .Michelson_data_model.instr_type
                .input_type
            with
            | [input], _ ->
                (* For the remaining instructions, we only support popcast for those
                   that take exactly one parameter from the stack. *)
                assign_pop_cast "val" input :: body
            | _ -> body ))
        (Common_backend.wrap_selected_names
           ~__LOC__
           ~action:(`Available_only false)
           "Algebraic instructions with  hard-coded body"
           instrs
           ["CDR"; "PAIR"; "UNPAIR"; "NONE"; "SOME"; "LEFT"; "RIGHT"])

  let print_instruction fmt instr =
    let body =
      List.assoc
        instr.Michelson_data_model.instr_concrete_name
        algebraic_instructions
    in
    Rust_syntax.print_item fmt (pure_interpreter instr (Some body))

  let supported_instructions =
    Common_backend.wrap_selected_names
      ~__LOC__
      "Algebraic supported instructions"
      instrs
      ["CAR"; "CDR"; "PAIR"; "UNPAIR"; "NONE"; "SOME"; "LEFT"; "RIGHT"]

  let supported_instructions =
    Format.asprintf
      "%a"
      (Format.pp_print_list print_instruction)
      (List.map (find_instruction instrs) supported_instructions)
end

module Interpreter (Env : Env) = struct
  let supported_instrs = ["CAR"]

  let gen_execute (instr : Michelson_data_model.instruction_def) =
    let instr_name =
      String.(capitalize_ascii @@ lowercase_ascii instr.instr_concrete_name)
    in
    let instr_pattern = if instr_name = "Seq" then "seq" else "instr" in
    let params = [Rust_syntax.E_Path ["stack"]] in
    (* TODO: depending on context/scope usage
       of the instr, execute will require more params *)
    Rust_syntax.
      ( TupleStruct (["Instruction"; instr_name], [Ident (Const, instr_pattern)]),
        E_MethodCall (E_Path [instr_pattern], "execute", params) )

  let instr_exec =
    Rust_syntax.(
      Format.asprintf
        "%a,"
        print_arms_list
        (List.map
           (fun instr -> gen_execute (find_instruction Env.instrs instr))
           supported_instrs))
end
