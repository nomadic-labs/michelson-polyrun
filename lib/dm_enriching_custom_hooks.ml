(** This file is meant to contains all hooks used to build the data model which
    should at some point disappear by pushing the relevant information in the
    michelson description.  *)

open Michelson_data_model

let enrich_type_argument ~arg ~ty_name =
  match ty_name with
  | "contract" -> Type (arg, Passable)
  | "big_map" -> Type (arg, Big_map_value)
  | _ -> Type (arg, Any)

let overloading_dispatch concrete_syntax op_name input_type_list =
  let fail_stack_type op_name input_type_list =
    Format.eprintf
      "@[<v 2 > No known overloading on %s for the given input stack type \
       @[%a@]@]@."
      op_name
      (Format.pp_print_list
         ~pp_sep:(fun ppf () -> Format.fprintf ppf ":")
         Michelson_descr.pp_ty)
      input_type_list ;
    failwith "DM_enriching.overloading_dispatch"
  in
  let open Michelson_descr in
  match (concrete_syntax, input_type_list) with
  | "ADD", [Ty_app ("int", []); Ty_app ("timestamp", [])] ->
      "ADD_seconds_to_timestamp"
  | "ADD", [Ty_app ("timestamp", []); Ty_app ("int", [])] ->
      "ADD_timestamp_to_seconds"
  | "SUB", [Ty_app ("timestamp", []); Ty_app ("int", [])] ->
      "SUB_timestamp_seconds"
  | "SUB", [Ty_app ("timestamp", []); Ty_app ("timestamp", [])] ->
      "Diff_timestamps"
  | ("ADD" | "MUL" | "EDIV" | "SUB"), [Ty_app (a, []); Ty_app (b, [])] ->
      let suffix =
        match (a, b) with
        | "nat", "nat" -> "nat"
        | ("int" | "nat"), ("int" | "nat") -> "int"
        | "mutez", "mutez" -> "tez"
        | a, b when a = b -> a
        | a, b -> Printf.sprintf "%s_%s" a b
      in
      Printf.sprintf "%s_%s" concrete_syntax suffix
  | "MAP", [Ty_app ((("list" | "map" | "option") as top_stack_ty), _)]
  | "ITER", [Ty_app ((("list" | "map" | "set") as top_stack_ty), _)]
  | ( "SIZE",
      [
        Ty_app
          ((("list" | "map" | "set" | "string" | "bytes") as top_stack_ty), _);
      ] )
  | ( "MEM",
      [_; Ty_app ((("list" | "map" | "big_map" | "set") as top_stack_ty), _)] )
  | ( "GET",
      [_; Ty_app ((("list" | "map" | "big_map" | "set") as top_stack_ty), _)] )
  | ( "UPDATE",
      [_; _; Ty_app ((("list" | "map" | "big_map" | "set") as top_stack_ty), _)]
    )
  | ( "GET_AND_UPDATE",
      [_; _; Ty_app ((("list" | "map" | "big_map" | "set") as top_stack_ty), _)]
    ) ->
      let prefix = match top_stack_ty with "option" -> "opt" | s -> s in
      Printf.sprintf "%s_%s" prefix concrete_syntax
  | ("LSL" | "LSR"), [Ty_app (a, []); Ty_app ("nat", [])] ->
      Printf.sprintf "%s_%s" concrete_syntax a
  | ("BYTES" | "NAT" | "INT" | "NEG" | "NOT"), [Ty_app (a, [])] ->
      Printf.sprintf "%s_%s" concrete_syntax a
  | "AND", [Ty_app ("int", []); Ty_app ("nat", [])] -> "AND_int_nat"
  | ("AND" | "OR" | "XOR"), [Ty_app (a, []); Ty_app (b, [])] when a = b ->
      Printf.sprintf "%s_%s" concrete_syntax a
  | "SLICE", [Ty_app ("nat", []); Ty_app ("nat", []); Ty_app (a, [])] ->
      Printf.sprintf "%s_%s" concrete_syntax a
  | "CONCAT", [Ty_app (a, []); Ty_app (b, [])] when a = b ->
      Printf.sprintf "CONCAT_%s_pair" a
  | "CONCAT", [Ty_app ("list", [Ty_app (a, [])])] ->
      Printf.sprintf "CONCAT_%s" a
  | _ -> fail_stack_type op_name input_type_list

let ad_hoc_renaming = function
  | "CONS" -> "CONS_LIST"
  | "LEFT" -> "CONS_LEFT"
  | "NONE" -> "CONS_NONE"
  | "PAIR" -> "CONS_PAIR"
  | "RIGHT" -> "CONS_RIGHT"
  | "SOME" -> "CONS_SOME"
  | s -> s

(* *****    *)
(* This hacks probably belongs to the ocaml backend *)
let record1 params instr_constructor_name name :
    Michelson_data_model.instr_param_list =
  match params with
  | [value] -> IAL_record [(name, value)]
  | _ -> failwith ("wrong number of arguments for" ^ instr_constructor_name)

let record2 params instr_constructor_name name1 name2 :
    Michelson_data_model.instr_param_list =
  match params with
  | [left; right] -> IAL_record [(name1, left); (name2, right)]
  | _ -> failwith ("wrong number of arguments for" ^ instr_constructor_name)

let enrich_instr_param_list ~params ~instr_constructor_name =
  match instr_constructor_name with
  | "IF" ->
      record2 params instr_constructor_name "branch_if_true" "branch_if_false"
  | "IF_LEFT" ->
      record2 params instr_constructor_name "branch_if_left" "branch_if_right"
  | "IF_CONS" ->
      record2 params instr_constructor_name "branch_if_cons" "branch_if_nil"
  | "IF_NONE" ->
      record2 params instr_constructor_name "branch_if_none" "branch_if_some"
  | "MAP" | "Opt_map" -> record1 params instr_constructor_name "body"
  | _ -> Michelson_data_model.IAL_list params

let instr_semantics ~instr_concrete_name =
  let open Semantics in
  match instr_concrete_name with
  | "CAR" ->
      let top_stack = "val" in
      let rule =
        {
          stack_prefix = Varlist [top_stack];
          bindings =
            [
              Destruct ({constructor = "Pair"; vars = ["first"; "_"]}, top_stack);
            ];
          output_prefix = ["first"];
        }
      in
      Semantics.consistent_rule rule ;
      rule
  | _ -> empty
