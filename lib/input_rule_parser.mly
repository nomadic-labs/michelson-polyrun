%{

let stackvar_normalise = function
  | "S1" | "A" -> "S"
  | "S2" | "B" -> "T"
  | "S3" | "C" -> "U"
  | "S4" | "D" -> "V"
  | any -> any

let tyvar_normalise = Fun.id
(* function
   * | "kty" | "cty" | "vty" | "ty" | "vty1" | "ty1" -> "a"
   * | "kty2" | "cty2" | "vty2" | "ty2" -> "b"
   * | "kty3" | "cty3" | "vty3" | "ty3" -> "c"
   * | "kty4" | "cty4" | "vty4" | "ty4" -> "d"
   * | any -> String.lowercase_ascii any *)
(* TODO: normalisation is broken, it needs mor context.
   We should probably delegate this to the enriching pass.
 *)

%}
%token CTX
%token VDASH
%token COLON
%token DOUBLE_COLON
%token FAT_ARROW
%token <string> DEPTH
%token <string> CONST
%token <string> MEMO
%token <string> TY_VAR
%token <string> CTY_VAR
%token <string> STACK_VAR
%token <string> CODE_VAR
%token <string> DATA_VAR
%token LPAREN
%token RPAREN
%token LBRACE
%token RBRACE
%token EOF

%start
< Michelson_descr.typing_judgment >
typing_judgment

%start
< Michelson_descr.typing_premisse >
typing_premisse

%start
< Michelson_descr.instr >
op_args

%%

typing_judgment:
  | CTX; VDASH; typing_judgment_instr = instr; DOUBLE_COLON; typing_judgment_input = stack_type; FAT_ARROW; typing_judgment_output = stack_type; EOF
    {
      Michelson_descr.{ typing_judgment_instr; typing_judgment_input; typing_judgment_output }
    }

typing_premisse:
  | typing_judgment = typing_judgment
    {
      Michelson_descr.TP_instr typing_judgment
    }
  | CTX; VDASH; data = DATA_VAR; DOUBLE_COLON; ty = ty; EOF
    {
      Michelson_descr.TP_data ( data, ty)
    }

op_args:
  | instr = instr; EOF { instr }

instr:
  | v = CODE_VAR { Michelson_descr.IVar v }
  | c = CONST; args = args { Michelson_descr.(IApp (c, args)) }

stack_type:
  | v = STACK_VAR { ([],  stackvar_normalise v) }
  | ty = ty; COLON; st = stack_type { let (st, v) = st in (ty :: st, v) }

ty_var:
  | v = TY_VAR { (tyvar_normalise v) }
  | v = CTY_VAR { (tyvar_normalise v) }

aty:
  | v = ty_var { Michelson_descr.Ty_var v }
  | n = MEMO { Michelson_descr.Ty_memo n }
  | c = CONST { Michelson_descr.Ty_app (c, [] ) }
  | LPAREN ty = ty RPAREN { ty }

ty:
  | LPAREN; ty= ty; RPAREN { ty }
  | v = ty_var { Michelson_descr.Ty_var v}
  | c = CONST; args = types { Michelson_descr.Ty_app (c, args) }

types: | l = list(aty) { l }

args: | l = list(arg) { l }

arg:
  | n = DEPTH { Michelson_descr.IA_depth n }
  | v = ty_var { Michelson_descr.IA_type v }
  | x = DATA_VAR { Michelson_descr.IA_data x }
  | LBRACE; v = CODE_VAR; RBRACE { Michelson_descr.IA_code v }
  | v = CODE_VAR { Michelson_descr.IA_code v }
