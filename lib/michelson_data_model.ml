(** This module defines a data model for the knowledge of the Michelson
   language. It differs from Michelson_descr, which is the tools' view
   of the language description given as input. The Michelson data
   model is meant to be rich and serves as information source for all
   the backends while the Michelson description is meant to be concise
   to ease maintenance of the language. *)

(** Functions converting a Michelson description into a data model are
   defined in module DM_enriching. *)

(** Definition of the syntax of types. *)

(** When defining a type, we must be explicit about comparability because it is enforced by the OCaml GADT.
   There are two aspects to this:
   - we must declare which type arguments are required to be comparable (for example map keys)
   - we must define in which cases the type we are defining is comparable.
*)

(** An argument to a Michelson type constructor is either a type, a comparable type, or a memo size. *)
type type_argument_constraint =
  | Any  (** This means that there is no constraint on the type argument *)
  | Comparable
      (** use this if you want to restrict a type argument to comparable
     types only (this is done for map keys, set elements, and ticket
     payloads) *)
  | Passable
      (** use this if you want to restrict a type argument to passable
     types only (this is done on "contract") *)
  | Big_map_value
      (** use this if you want to restrict a type argument to big_map value
     types only. *)
[@@deriving show]

type type_argument = Type of string * type_argument_constraint | Memo_size
[@@deriving show]

type comparable_flag =
  | Yes
      (** {Yes} means that the type is always comparable: most atomic types
     are in this case *)
  | No
      (** {No} means that the type is never comparable: for example
     [operation] and [list] are in this case. *)
  | Var of string
      (** {Var v} means that this type is comparable iff its parameter
     named {v} is comparable: for example [option 'v] is comparable
     iff ['v] is comparable. *)
  | And of string * string * string
      (** {And (v1, v2, v3)} means that this type is comparable iff both
     {v1} and {v2} are. For example [pair 'v1 'v2] is comparable iff
     both ['v1] and ['v2] are comparable.  The third name {v3} can be
     any name different from {v1} and {v2}; we must provide it because
     conjunction is defined as a relation (and not a function) in the
     GADT. *)
[@@deriving show]

(** Grammar *)

type type_def = {
  ty_name : string;  (** The name of the type in Michelson *)
  ty_arity : type_argument list;
      (** The names and comparability of the arguments of this type constructor *)
  ty_ocaml_repr : string;
      (** The ocaml type implementing this Michelson type. *)
  ty_rust_repr : string;  (** The Rust type implementing this Michelson type. *)
  ty_comparable : comparable_flag;  (** When is this type comparable *)
  ty_atomic : bool;
      (** True iff ty_arity is empty or contains only Memo_size, atomic
         types are handled specially in the GADT because their metadata
         are precomputed. *)
}
[@@deriving show]

type ty = Ty_var of string | Ty_memo of string | Ty_app of string * ty list
[@@deriving show]

type stack_ty = ty list * string [@@deriving show]

type instr_param =
  | IA_type of string
      (** Type parameter for an instruction (for example "'b" in "LEFT 'b") *)
  | IA_comparable_type of string
      (** Same but the type must be comparable (for example "'a" in "EMPTY_SET 'a") *)
  | IA_depth of string
      (** depth parameter for an instruction (for example "n" in "DIP n") *)
  | IA_data of (string * ty)
      (** Data parameter for an instruction (for example "'x" in "PUSH 'ty 'x") + its type *)
  | IA_code of (string * stack_ty * stack_ty)
      (** Named code parameter for an instruction together with its input and output types (for example "'i" in "DIP {'i}") *)
  | IA_extra of string
      (** Some extra data available from the OCaml GADT but not visible in the concrete syntax *)
[@@deriving show]

type instr_param_list =
  | IAL_list of instr_param list
  | IAL_record of (string * instr_param) list
[@@deriving show]

type instr_type = {
  input_type : stack_ty;
      (** The part of the input stack type that the instruction sees. *)
  output_type : stack_ty;
      (** The part of the output stack type that the instruction produces. *)
  top_remaining_input_type : string;
      (** A fresh type variable name for the top element of the remaining input stack (only for the OCaml GADT) *)
  top_remaining_output_type : string;
      (** A fresh type variable name for the top element of the remaining output stack (only for the OCaml GADT) *)
}
[@@deriving show]

type instruction_def = {
  instr_name : string;  (** The name of the instruction in Michelson *)
  instr_concrete_name : string;
      (** The concrete syntaxe of the instruction in Michelson, most of the time, its
     name execept for DIPN, and the like *)
  instr_constructor_name : string;
      (** The name of the corresponding constructor in the OCaml GADT *)
  instr_params : instr_param_list;  (**  instruction typing *)
  instr_type : instr_type;
      (** The list of params of the instruction in the OCaml GADT. *)
  instr_semantics : Semantics.semrule;  (** Semantic rule of the instruction. *)
}
[@@deriving show]

module type Env = sig
  val tys : (string, type_def) Hashtbl.t

  val instrs : (string, instruction_def) Hashtbl.t
end
