open Rust_backend

let print tys instrs fmt =
  let module Env : Env = struct
    let tys = tys

    let instrs = instrs
  end in
  let module Types_rs = Types_rs (Env) in
  let make =
    let open Types_rs in
    [%string
      {|
pub mod big_map;
pub mod contract;
pub mod core;
pub mod encoded;
pub mod int;
pub mod lambda;
pub mod list;
pub mod map;
pub mod mutez;
pub mod nat;
pub mod operation;
pub mod option;
pub mod or;
pub mod pair;
pub mod set;
pub mod timestamp;

use derive_more::{Display, From, TryInto};
use ibig::{IBig, UBig};
use std::collections::BTreeMap;
use tezos_core::types::{
    encoded::{Address, ChainId, ImplicitAddress, PublicKey, Signature},
    mutez::Mutez,
};
use tezos_michelson::micheline::Micheline;
use tezos_michelson::michelson::{data::Instruction, types::Type};

#[macro_export]
macro_rules! not_comparable {
    ($item: ty) => {
        impl PartialOrd for $item {
            fn partial_cmp(&self, _: &Self) -> Option<std::cmp::Ordering> {
                unreachable!("Not a comparable type")
            }
        }

        impl Ord for $item {
            fn cmp(&self, _: &Self) -> std::cmp::Ordering {
                unreachable!("Not a comparable type")
            }
        }

        impl Eq for $item {}
    };
}

macro_rules! define_item {
    ($name: ident, $impl: ty) => {
        #[derive(Debug, Clone, PartialEq, Eq, From)]
        pub struct $name($impl);
    };
}

macro_rules! define_item_ord {
    ($name: ident, $impl: ty) => {
        #[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Ord, From)]
        pub struct $name($impl);
    };
}

macro_rules! define_item_rec {
    ($name: ident, $val: ty, $typ: ty) => {
        #[derive(Debug, Clone)]
        pub struct $name {
            outer_value: $val,
            inner_type: $typ,
        }
    };
}

%{String.concat "" types}

%{String.concat "" not_comparable_types}

#[derive(Debug, Clone, PartialEq)]
pub enum InternalContent {
    Transaction {
        destination: Address,
        parameter: Micheline,
        amount: Mutez,
        source: ImplicitAddress,
    },
}

#[derive(Debug, Clone)]
pub struct OperationItem {
    // domain
    content: InternalContent,
    big_map_diff: Vec<BigMapDiff>,
}

#[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Ord)]
pub struct PairItem(Box<(StackItem, StackItem)>); // algebraic

#[derive(Debug, Clone)]
pub enum OptionItem {
    // algebraic
    None(Type),
    Some(Box<StackItem>),
}

#[derive(Debug, Clone)]
pub struct OrVariant {
    value: Box<StackItem>,
    other_type: Type,
}

#[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Ord)]
pub enum OrItem {
    // algebraic
    Left(OrVariant),
    Right(OrVariant),
}

#[derive(Debug, Clone)]
pub struct BigMapDiff {
    pub id: i64,
    pub inner_type: (Type, Type),
    pub updates: BTreeMap<String, (Micheline, Option<Micheline>)>,
    pub alloc: bool,
}

#[derive(Debug, Clone, PartialEq)]
pub enum BigMapItem {
    // collections
    Diff(BigMapDiff),
    Map(MapItem),
    Ptr(i64),
}

#[derive(Debug, Display, Clone, From, TryInto, PartialEq, PartialOrd, Eq, Ord)]
pub %{enum_stack_item}

impl AsMut<StackItem> for StackItem {
    fn as_mut(&mut self) -> &mut StackItem {
        self
    }
}
|}]
  in
  Format.fprintf fmt "%s" make
