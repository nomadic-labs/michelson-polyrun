open Rust_backend

let print tys instrs fmt =
  let module Env : Env = struct
    let tys = tys

    let instrs = instrs
  end in
  let module Interpreter_rs = Interpreter (Env) in
  let open Interpreter_rs in
  let make =
    [%string
      {|
use tezos_core::types::{
    encoded::{Address, ChainId, ContractAddress, ImplicitAddress},
    mutez::Mutez,
};
use tezos_michelson::micheline::Micheline;
use tezos_michelson::michelson::{data::Instruction, types::Type};

pub use tezos_ctx::InterpreterContext;

use crate::{
    err_unsupported,
    formatter::Formatter,
    stack::Stack,
    trace_enter, trace_exit,
    types::{BigMapDiff, StackItem},
    Result,
};

pub struct OperationScope {
    pub chain_id: ChainId,
    pub source: ImplicitAddress,
    pub sender: Address,
    pub amount: Mutez,
    pub balance: Mutez,
    pub parameters: Option<(String, Micheline)>,
    pub storage: Micheline,
    pub now: i64,
    pub self_address: ContractAddress,
    pub self_type: Micheline,
    pub level: i32,
}

pub trait Interpreter {
    fn execute(
        &self,
        stack: &mut Stack,
        scope: &OperationScope,
        context: &mut impl InterpreterContext,
    ) -> Result<()>;
}

pub trait PureInterpreter {
    fn execute(&self, stack: &mut Stack) -> Result<()>;
}

pub trait ScopedInterpreter {
    fn execute(&self, stack: &mut Stack, scope: &OperationScope) -> Result<()>;
}

pub trait ContextInterpreter {
    fn execute(&self, stack: &mut Stack, context: &mut impl InterpreterContext) -> Result<()>;
}

pub trait LazyStorage {
    fn try_acquire(
        &mut self,
        owner: &ContractAddress,
        context: &mut impl InterpreterContext,
    ) -> Result<()>;
    fn try_aggregate(&mut self, output: &mut Vec<BigMapDiff>, ty: &Type) -> Result<()>;
}

impl Interpreter for Instruction {
    fn execute(
        &self,
        stack: &mut Stack,
        scope: &OperationScope,
        context: &mut impl InterpreterContext,
    ) -> Result<()> {
        trace_enter!(self);
        let res = match self {
            %{instr_exec}
            _ => err_unsupported!(self.format()),
        };
        trace_exit!(res.as_ref().err(), format!("Len {}", &stack.len()).as_str());
        res
    }
}

impl LazyStorage for StackItem {
    fn try_acquire(
        &mut self,
        owner: &ContractAddress,
        context: &mut impl InterpreterContext,
    ) -> Result<()> {
        match self {
            StackItem::BigMap(item) => item.try_acquire(owner, context),
            StackItem::Option(item) => item.try_acquire(owner, context),
            StackItem::Or(item) => item.try_acquire(owner, context),
            StackItem::Pair(item) => item.try_acquire(owner, context),
            StackItem::List(item) => item.try_acquire(owner, context),
            StackItem::Map(item) => item.try_acquire(owner, context),
            _ => Ok(()),
        }
    }

    fn try_aggregate(&mut self, output: &mut Vec<BigMapDiff>, ty: &Type) -> Result<()> {
        match self {
            StackItem::BigMap(item) => item.try_aggregate(output, ty),
            StackItem::Option(item) => item.try_aggregate(output, ty),
            StackItem::Or(item) => item.try_aggregate(output, ty),
            StackItem::Pair(item) => item.try_aggregate(output, ty),
            StackItem::List(item) => item.try_aggregate(output, ty),
            StackItem::Map(item) => item.try_aggregate(output, ty),
            _ => Ok(()),
        }
    }
}
       |}]
  in
  Format.fprintf fmt "%s" make
