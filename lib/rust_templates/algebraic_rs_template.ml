open Rust_backend

let print tys instrs fmt =
  let module Env : Env = struct
    let tys = tys

    let instrs = instrs
  end in
  let module Algebraic_rs = Algebraic_rs (Env) in
  let open Algebraic_rs in
  let make =
    [%string
      {|
use tezos_michelson::michelson::{
    data::instructions::{Car, Cdr, Left, None, Pair, Right, Some, Unit, Unpair},
    data::Nat,
};

use crate::{
    err_mismatch,
    interpreter::PureInterpreter,
    pop_cast,
    stack::Stack,
    types::{OptionItem, OrItem, PairItem, StackItem},
    Result,
};

impl PureInterpreter for Unit {
    fn execute(&self, stack: &mut Stack) -> Result<()> {
        stack.push(StackItem::Unit(().into()))
    }
}

fn parse_arity(n: &Option<Nat>) -> Result<usize> {
    let n: usize = match n {
        Some(n) => n.try_into()?,
        None => 2,
    };
    if n < 2 {
        return err_mismatch!(">=2 args", n);
    }
    Ok(n)
}

%{supported_instructions}
  |}]
  in
  Format.fprintf fmt "%s" make
