(** [wrap_selected_names ~action ~__LOC__ context table names] check the list of
    [names] against the keys of the provided [table].
    Depending of the selected [action] it will either
    - `Warn: Simply output a warning informing about the missing elements in the
    table and in the list
    - `Fail: Fail if [names] is not a permutation of [table]'s keys
    - `Complete: complete [names] with missing keys from the [table], but fail
    if some names are not keys of [table]

    [__LOC__] and [context] strings are used to provide context information in
    printed messages.

    Default action is `Fail.
*)
val wrap_selected_names :
  ?action:[`Available_only of bool | `Complete | `Fail | `Warn] ->
  __LOC__:string ->
  string ->
  ?filter_table:(string -> 'a -> bool) ->
  (string, 'a) Hashtbl.t ->
  string list ->
  string list
