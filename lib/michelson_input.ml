(* This file contains a description of a fragment of the Michelson
   language represented in Ocaml using the syntax declared in module
   Michelson_descr. *)

open Michelson_descr

let tys = Input_parser.tys

type typing_rule = {premisses : string list; conclusion : string}

let define_instr ?constructor_name ~typing_rule ?(params = []) () =
  let typing_rule_premisses =
    List.map Input_parser.read_typing_premisse typing_rule.premisses
  in
  let typing_rule_conclusion =
    Input_parser.read_typing_judgment typing_rule.conclusion
  in
  let instr_concrete_name =
    match typing_rule_conclusion.typing_judgment_instr with
    | IVar c -> c
    | IApp (c, _) -> c
  in
  let instr_constructor_name =
    Option.value ~default:instr_concrete_name constructor_name
  in
  {
    instr_concrete_name;
    instr_constructor_name;
    instr_params = params;
    instr_typing_rules =
      [
        {
          name = instr_constructor_name;
          typing_rule_premisses;
          typing_rule_conclusion;
        };
      ];
  }

let instrs =
  Input_parser.instrs
    ~blacklist:
      [
        (*  PARSING ERRORS *)
        "CREATE_CONTRACT";
        "DIG";
        "DIP";
        "DIPN";
        "DROPN";
        "DUG";
        "DUPN";
        "GETN";
        "LAMBDA";
        "LAMBDA_REC";
        "NOOP";
        "PAIRN";
        "SAPLING_EMPTY_STATE";
        "SELF";
        "SEQ";
        "UNPAIRN";
        "UPDATEN";
      ]
    ()
