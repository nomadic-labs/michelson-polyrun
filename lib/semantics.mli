(* this alias is pure "type documentation" to mark position were strings
   represent variable names *)
type varname = string

(** Either the instruction uses a fixed prefix of the stack, then we can name
    each element to be used, or it's runtime defined, and there will be some
    sort of loop (cases to be explored latter) *)
type stack_prefix = Varlist of varname list | Runtime_depth

(** When operating on algebraic type, we want to pattern match them. This type
    defines the pattern shape *)
type algebraic_pattern = {constructor : string; vars : varname list}
[@@deriving show]

(** Sometimes we just want to combine some values with an 'externally defined'
    opeeration *)
type expr = {operator : string; operands : varname list}

(** Variable binding is either a pattern matching or an expr assignment*)
type var_bind =
  | Assign of (varname * expr)
  | Destruct of (algebraic_pattern * varname)

(** a semantic rule
   - describes the expected stack prefix,
   - binds some new variables to some "computation" over the prefix
   - describes the replacement of the prefix in the output stack

Expected invariant :
- order of bindings matters
  - a binding's expr cannot use free variables
  - var shadowing is forbidden.
*)
type semrule = {
  stack_prefix : stack_prefix;
  bindings : var_bind list;
  output_prefix : varname list;
}
[@@deriving show]

val empty : semrule

module Set : Set.S with type elt = string

(** Asserting consistency of a smeantic rule  *)
val consistent_rule : semrule -> unit
