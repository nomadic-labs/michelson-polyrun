open Michelson_data_model

let print_ty_grammar_entry tys fmt ty_name =
  let ty_arity = (Hashtbl.find tys ty_name).ty_arity in
  let print_ty_argument fmt = function
    | Type (alpha, _constraint) -> Format.fprintf fmt "%s" alpha
    | Memo_size -> Format.fprintf fmt "memo_size"
  in
  let print_ty_arguments fmt () =
    match ty_arity with
    | [] -> ()
    | _ ->
        Format.fprintf
          fmt
          " %a"
          (Format.pp_print_list
             ~pp_sep:(fun fmt () -> Format.fprintf fmt " ")
             print_ty_argument)
          ty_arity
  in
  Format.fprintf fmt "| %s%a" ty_name print_ty_arguments ()

let print_tys_grammar_entries fmt tys =
  let ty_names =
    Hashtbl.to_seq_keys tys |> List.of_seq |> List.sort String.compare
  in
  Format.fprintf
    fmt
    "@[<v 2>ty ::=@;%a@]@."
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt "@;")
       (print_ty_grammar_entry tys))
    ty_names

let print_instr_grammar_entry instrs fmt instr_name =
  let {
    instr_name = _;
    instr_concrete_name;
    instr_constructor_name = _;
    instr_type =
      {
        top_remaining_input_type = _;
        top_remaining_output_type = _;
        input_type = input_tys, input_stack_var;
        output_type = output_tys, output_stack_var;
      };
    instr_params;
    instr_semantics;
  } =
    Hashtbl.find instrs instr_name
  in
  let remaining_input_stack_var = String.uppercase_ascii input_stack_var in
  let remaining_output_stack_var = String.uppercase_ascii output_stack_var in
  let print_type_param fmt alpha = Format.fprintf fmt " '%s" alpha in
  let print_data_param fmt (x, _ty) = Format.fprintf fmt " '%s" x in
  let print_code_param fmt (i, _, _) = Format.fprintf fmt " { '%s }" i in
  let print_param fmt = function
    | IA_type alpha -> print_type_param fmt alpha
    | IA_comparable_type alpha -> print_type_param fmt alpha
    | IA_data x -> print_data_param fmt x
    | IA_code i -> print_code_param fmt i
    | IA_depth n -> Format.fprintf fmt " %s" n
    | IA_extra _ -> ()
  in
  let print_params fmt () =
    let l =
      match instr_params with IAL_list l -> l | IAL_record r -> List.map snd r
    in
    Format.pp_print_list ~pp_sep:(fun _fmt () -> ()) print_param fmt l
  in
  let rec print_ty fmt = function
    | Ty_var alpha -> Format.fprintf fmt "'%s" alpha
    | Ty_memo ty -> Format.fprintf fmt "%s" ty
    | Ty_app (ty, l) ->
        Format.fprintf
          fmt
          "%s%a"
          ty
          (Format.pp_print_list
             ~pp_sep:(fun _fmt () -> ())
             (fun fmt ty -> Format.fprintf fmt " %a" print_ty_with_paren ty))
          l
  and print_ty_with_paren fmt ty =
    match ty with
    | Ty_app (_, _ :: _) -> Format.fprintf fmt "(%a)" print_ty ty
    | ty -> print_ty fmt ty
  in
  let print_stack fmt stack =
    Format.pp_print_list
      ~pp_sep:(fun fmt () -> Format.fprintf fmt " : ")
      print_ty
      fmt
      stack
  in
  let print_stack_sem fmt stack =
    let open Semantics in
    match stack with
    | Varlist stack ->
        Format.pp_print_list
          ~pp_sep:(fun fmt () -> Format.fprintf fmt " : ")
          Format.pp_print_string
          fmt
          stack
    | Runtime_depth -> Format.fprintf fmt "<..>"
  in
  let print_output_stack fmt stack =
    Format.pp_print_list
      ~pp_sep:(fun fmt () -> Format.fprintf fmt " : ")
      Format.pp_print_string
      fmt
      stack
  in
  let print_bindings fmt bindings =
    Format.(
      pp_print_list
        ~pp_sep:(fun fmt () -> Format.fprintf fmt " : ")
        (fun fmt binding ->
          match binding with
          | Semantics.Assign (var, {operator; operands}) ->
              fprintf
                fmt
                "%s=%s(%a))"
                var
                operator
                (pp_print_list pp_print_string)
                operands
          | Destruct (pattern, var) ->
              fprintf
                fmt
                "(%a) = %s)"
                Semantics.pp_algebraic_pattern
                pattern
                var))
      fmt
      bindings
  in
  let print_semrule fmt instr_semantics =
    if instr_semantics = Semantics.empty then ()
    else
      Format.fprintf
        fmt
        "  { %a:S  ~> %a:S where %a }"
        print_stack_sem
        instr_semantics.stack_prefix
        print_output_stack
        instr_semantics.output_prefix
        print_bindings
        instr_semantics.bindings
  in

  Format.fprintf
    fmt
    "| %s%a :: %a  =>  %a%a"
    instr_concrete_name
    print_params
    ()
    print_stack
    (input_tys @ [Ty_var remaining_input_stack_var])
    print_stack
    (output_tys @ [Ty_var remaining_output_stack_var])
    print_semrule
    instr_semantics

let print_instrs_gramar_entries fmt instrs =
  let instr_names =
    Hashtbl.to_seq_keys instrs |> List.of_seq |> List.sort String.compare
  in
  Format.fprintf
    fmt
    "@[<v 2>instr ::=@;%a@]@."
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt "@;")
       (print_instr_grammar_entry instrs))
    instr_names
