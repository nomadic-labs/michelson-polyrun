open Michelson_data_model

module type Env = Env

open Common_backend

let yes = Ocaml_syntax.type_constant "yes"

let no = Ocaml_syntax.type_constant "no"

let ocaml_ty_constructor_name s = String.capitalize_ascii s ^ "_t"

let gen_ty a b = Ocaml_syntax.Typeconstr ([a; b], "ty")

let gen_comparable_ty a = Ocaml_syntax.Typeconstr ([a], "comparable_ty")

let gen_kinstr (a, b) (c, d) = Ocaml_syntax.Typeconstr ([a; b; c; d], "kinstr")

let gen_kdescr (a, b) (c, d) = Ocaml_syntax.Typeconstr ([a; b; c; d], "kdescr")

let gen_ty_metadata a = Ocaml_syntax.Typeconstr ([a], "ty_metadata")

let gen_dand a b c = Ocaml_syntax.Typeconstr ([a; b; c], "dand")

let gen_dbool a = Ocaml_syntax.Typeconstr ([a], "dbool")

let memo_size_t = Ocaml_syntax.type_constant "Sapling.Memo_size.t"

let location_type = Ocaml_syntax.type_constant "Script.location"

let script_node = Ocaml_syntax.type_constant "Script.node"

let empty_cell = Ocaml_syntax.type_constant "empty_cell"

let end_of_stack = Ocaml_syntax.type_constant "end_of_stack"

let gen_lambda a b = Ocaml_syntax.Typeconstr ([a; b], "lambda")

let gen_stack_ty a b = Ocaml_syntax.Typeconstr ([a; b], "stack_ty")

let generate_ty_ocaml_gadt
    {
      ty_name;
      ty_arity;
      ty_ocaml_repr;
      ty_rust_repr = _;
      ty_comparable;
      ty_atomic;
    } =
  let constructor_name = ocaml_ty_constructor_name ty_name in
  let gen_repr =
    let gen_type_argument = function
      | Type (alpha, _constraint) -> Some (Ocaml_syntax.Typevar alpha)
      | Memo_size -> None
    in
    Ocaml_syntax.Typeconstr
      (List.filter_map gen_type_argument ty_arity, ty_ocaml_repr)
  in
  let gen_comparable_flag =
    match ty_comparable with
    | Yes -> yes
    | No -> no
    | Var alpha | And (_, _, alpha) -> Ocaml_syntax.Typevar (alpha ^ "c")
  in
  let gen_arg = function
    | Type (alpha, (Any | Passable | Big_map_value)) ->
        gen_ty (Ocaml_syntax.Typevar alpha) (Ocaml_syntax.Typevar (alpha ^ "c"))
    | Type (alpha, Comparable) -> gen_comparable_ty (Ocaml_syntax.Typevar alpha)
    | Memo_size -> memo_size_t
  in
  let gen_metadata = if ty_atomic then [] else [gen_ty_metadata gen_repr] in
  let gen_comparable_flag_bool_witness =
    match ty_comparable with
    | And (alpha, beta, gamma) ->
        [
          gen_dand
            (Ocaml_syntax.Typevar (alpha ^ "c"))
            (Ocaml_syntax.Typevar (beta ^ "c"))
            (Ocaml_syntax.Typevar (gamma ^ "c"));
        ]
    | Var alpha -> [gen_dbool (Ocaml_syntax.Typevar (alpha ^ "c"))]
    | Yes | No -> []
  in
  let gen_args =
    List.map gen_arg ty_arity @ gen_metadata @ gen_comparable_flag_bool_witness
  in
  ( constructor_name,
    Ocaml_syntax.Product gen_args,
    gen_ty gen_repr gen_comparable_flag )

let ty_gadt_case_str ty =
  let gadt_case = generate_ty_ocaml_gadt ty in
  Format.asprintf "%a" Ocaml_syntax.print_typedef_body_gadt_case gadt_case

let ty_gadt_cases_str ty_table ty_names =
  String.concat "\n"
  @@ List.map
       (fun ty_name -> ty_gadt_case_str @@ Hashtbl.find ty_table ty_name)
       ty_names

exception Undeclared_type_constructor of string

exception Multiple_declarations_for_type_constructor of string

let ocaml_constructor_name s =
  "I" ^ String.capitalize_ascii (String.lowercase_ascii s)

let generate_instr_gadt tys
    {
      instr_name = _;
      instr_concrete_name = _;
      instr_constructor_name;
      instr_type;
      instr_params;
      instr_semantics = _;
    } =
  let constructor_name = ocaml_constructor_name instr_constructor_name in
  let find_ocaml_repr_for_type ty l =
    match Hashtbl.find_opt tys ty with
    | None -> raise (Undeclared_type_constructor ty)
    | Some ty_def -> Ocaml_syntax.Typeconstr (l, ty_def.ty_ocaml_repr)
  in
  let gen_type_memo ty = Ocaml_syntax.Typeconstr ([], ty) in
  let rec gen_type = function
    | Ty_var alpha -> Ocaml_syntax.Typevar alpha
    | Ty_memo ty -> gen_type_memo ty
    | Ty_app (ty, l) -> find_ocaml_repr_for_type ty (List.map gen_type l)
  in
  let rec gen_stack_tail = function
    | [] -> raise (Invalid_argument "generate_instr_gadt/gen_stack_tail")
    | [a] -> gen_type a
    | hd :: tl -> Ocaml_syntax.Prod [gen_type hd; gen_stack_tail tl]
  in
  let gen_stack = function
    | [] -> raise (Invalid_argument "generate_instr_gadt/gen_stack")
    | hd :: tl -> (gen_type hd, gen_stack_tail tl)
  in
  let gen_type_param alpha =
    gen_ty (Ocaml_syntax.Typevar alpha) Ocaml_syntax.Typewildcard
  in
  let gen_comparable_type_param alpha =
    gen_comparable_ty (Ocaml_syntax.Typevar alpha)
  in
  let gen_data_param (_x, alpha) = gen_type alpha in
  let gen_code_param (_i, (input, input_stack_var), (output, output_stack_var))
      =
    gen_kinstr
      (gen_stack
         (input
         @ [Ty_var instr_type.top_remaining_input_type; Ty_var input_stack_var]
         ))
      (gen_stack
         (output
         @ [
             Ty_var instr_type.top_remaining_output_type; Ty_var output_stack_var;
           ]))
  in
  let gen_param = function
    | IA_type alpha -> gen_type_param alpha
    | IA_comparable_type alpha -> gen_comparable_type_param alpha
    | IA_data x -> gen_data_param x
    | IA_code i -> gen_code_param i
    | IA_depth _ -> failwith "Unsupported feature: stack or comb depth"
    | IA_extra s -> Ocaml_syntax.Extra s
  in
  let gen_params =
    let full_output_stack =
      let output, output_stack_var = instr_type.output_type in
      output
      @ [Ty_var instr_type.top_remaining_output_type; Ty_var output_stack_var]
    in
    match instr_params with
    | IAL_list l ->
        Ocaml_syntax.Product
          ((location_type :: List.map gen_param l)
          @ [
              gen_kinstr
                (gen_stack full_output_stack)
                (Ocaml_syntax.Typevar "r", Ocaml_syntax.Typevar "F");
            ])
    | IAL_record r ->
        Ocaml_syntax.Record
          (("loc", location_type)
           :: List.map (fun (l, param) -> (l, gen_param param)) r
          @ [
              ( "k",
                gen_kinstr
                  (gen_stack full_output_stack)
                  (Ocaml_syntax.Typevar "r", Ocaml_syntax.Typevar "F") );
            ])
  in
  ( constructor_name,
    gen_params,
    gen_kinstr
      (gen_stack
         (let input, input_stack_var = instr_type.input_type in
          input
          @ [Ty_var instr_type.top_remaining_input_type; Ty_var input_stack_var]))
      (Ocaml_syntax.Typevar "r", Ocaml_syntax.Typevar "F") )

let kinstr_gadt_case_str tys instr =
  let gadt_case = generate_instr_gadt tys instr in
  Format.asprintf "%a" Ocaml_syntax.print_typedef_body_gadt_case gadt_case

let kinstr_gadt_cases_str tys instr_table instr_names =
  String.concat "\n"
  @@ List.map
       (fun instr_name ->
         kinstr_gadt_case_str tys @@ Hashtbl.find instr_table instr_name)
       instr_names

let generate_kinstr_pattern {instr_constructor_name; instr_params; _} =
  let constructor_name = ocaml_constructor_name instr_constructor_name in
  let pattern_of_IA = function
    | IA_type alpha -> Ocaml_syntax.PVariable alpha
    | IA_comparable_type alpha -> Ocaml_syntax.PVariable alpha
    | IA_data (x, _ty) -> Ocaml_syntax.PVariable x
    | IA_code (i, _ty1, _ty2) -> Ocaml_syntax.PVariable i
    | IA_extra _ -> Ocaml_syntax.Wildcard
    | IA_depth _ -> Ocaml_syntax.Wildcard
  in
  let subpatterns : Ocaml_syntax.pattern list =
    match instr_params with
    | IAL_list l ->
        (Ocaml_syntax.PVariable "loc" :: List.map pattern_of_IA l)
        @ [Ocaml_syntax.PVariable "k"]
    | IAL_record l -> [Ocaml_syntax.PRecord ("loc" :: "k" :: List.map fst l)]
  in
  Ocaml_syntax.PApp (constructor_name, subpatterns)

let generate_kinstr_location_function_case i =
  Ocaml_syntax.
    {
      pattern = generate_kinstr_pattern i;
      when_clause = None;
      body = Variable "loc";
    }

let kinstr_location_function_case_str instr =
  let case = generate_kinstr_location_function_case instr in
  Format.asprintf "%a" Ocaml_syntax.print_pattern_matching case

let kinstr_location_function_cases_str instr_table instr_names =
  String.concat "\n"
  @@ List.map
       (fun instr_name ->
         kinstr_location_function_case_str
         @@ Hashtbl.find instr_table instr_name)
       instr_names

let generate_ty_pattern {ty_arity; ty_name; ty_comparable; ty_atomic; _} =
  let constructor_name = ocaml_ty_constructor_name ty_name in
  let has_metadata = not ty_atomic in
  let gen_comparable_pattern =
    match ty_comparable with
    | And _ -> [Ocaml_syntax.PVariable "dand"]
    | Var _ -> [Ocaml_syntax.PVariable "cmp"]
    | Yes | No -> []
  in
  let gen_args =
    List.map
      (function
        | Type (v, _) -> Ocaml_syntax.PVariable v
        | Memo_size -> Ocaml_syntax.PVariable "memo_size")
      ty_arity
    @ (if has_metadata then [Ocaml_syntax.PVariable "meta"] else [])
    @ gen_comparable_pattern
  in
  Ocaml_syntax.PApp (constructor_name, gen_args)

let generate_ty_metadata_function_case ty =
  let body =
    if ty.ty_atomic then Ocaml_syntax.Variable "meta_basic"
    else Ocaml_syntax.Variable "meta"
  in
  Ocaml_syntax.{pattern = generate_ty_pattern ty; when_clause = None; body}

let ty_metadata_function_cases_str ty_table ty_names =
  Format.asprintf "%a" Ocaml_syntax.print_pattern_matching_list
  @@ List.map
       (fun ty_name ->
         generate_ty_metadata_function_case @@ Hashtbl.find ty_table ty_name)
       ty_names

let generate_is_comparable_case ty =
  let body =
    match ty.ty_comparable with
    | And _ ->
        Ocaml_syntax.app
          (Ocaml_syntax.Variable "dbool_of_dand")
          [Ocaml_syntax.Variable "dand"]
    | Var _ -> Ocaml_syntax.Variable "cmp"
    | Yes -> Ocaml_syntax.Variable "Yes"
    | No -> Ocaml_syntax.Variable "No"
  in
  Ocaml_syntax.{pattern = generate_ty_pattern ty; when_clause = None; body}

let generate_is_comparable_function_cases ty_table ty_names =
  List.map
    (fun ty_name ->
      generate_is_comparable_case @@ Hashtbl.find ty_table ty_name)
    ty_names

let generate_ty_traverse_function_case ty =
  let body =
    match ty.ty_arity with
    | [] | [Memo_size] ->
        Ocaml_syntax.app
          ~tailcall:true
          (Ocaml_syntax.Variable "continue")
          [Ocaml_syntax.Variable "accu"]
    | [Type (ty, _)] ->
        Ocaml_syntax.app
          ~tailcall:true
          (Ocaml_syntax.Variable "aux")
          [
            Ocaml_syntax.Variable "f";
            Ocaml_syntax.Variable "accu";
            Ocaml_syntax.Variable ty;
            Ocaml_syntax.Variable "continue";
          ]
    | [Type (ty1, _); Type (ty2, _)] ->
        Ocaml_syntax.app
          ~tailcall:true
          (Ocaml_syntax.Variable "next2")
          [
            Ocaml_syntax.Variable "f";
            Ocaml_syntax.Variable "accu";
            Ocaml_syntax.Variable ty1;
            Ocaml_syntax.Variable ty2;
            Ocaml_syntax.Variable "continue";
          ]
    | _ -> invalid_arg "Proto backend: generate_ty_traverse_function_case"
  in
  Ocaml_syntax.{pattern = generate_ty_pattern ty; when_clause = None; body}

let ty_traverse_function_cases_str ty_table ty_names =
  Format.asprintf "%a" Ocaml_syntax.print_pattern_matching_list
  @@ List.map
       (fun ty_name ->
         generate_ty_traverse_function_case (Hashtbl.find ty_table ty_name))
       ty_names

module Script_typed_ir (Env : Env) = struct
  open Env

  let instr_names_for_gadt =
    wrap_selected_names
      ~__LOC__
      "kinstr GADT"
      instrs
      [
        "DROP";
        "DUP";
        "SWAP";
        "PUSH";
        "UNIT";
        "PAIR";
        "CAR";
        "CDR";
        "UNPAIR";
        "SOME";
        "NONE";
        "IF_NONE";
        "MAP__option";
        "LEFT";
        "RIGHT";
        "IF_LEFT";
        "CONS";
        "NIL";
        "IF_CONS";
        "MAP__list";
        "ITER__list";
        "SIZE__list";
        "EMPTY_SET";
        "ITER__set";
        "MEM__set";
        "UPDATE__set";
        "SIZE__set";
        "EMPTY_MAP";
        "MAP__map";
        "ITER__map";
        "MEM__map";
        "GET__map";
        "UPDATE__map";
        "GET_AND_UPDATE__map";
        "SIZE__map";
        "EMPTY_BIG_MAP";
        "MEM__big_map";
        "GET__big_map";
        "UPDATE__big_map";
        "GET_AND_UPDATE___big_map";
        "CONCAT__string_list";
        "CONCAT__string";
        "SLICE__string";
        "SIZE__string";
        "CONCAT__bytes_list";
        "CONCAT__bytes";
        "SLICE__bytes";
        "SIZE__bytes";
        "LSL__bytes";
        "LSR__bytes";
        "OR__bytes";
        "AND__bytes";
        "XOR__bytes";
        "NOT__bytes";
        "NAT__bytes";
        "BYTES__nat";
        "INT__bytes";
        "BYTES__int";
        "ADD__int_timestamp";
        "ADD__timestamp_int";
        "SUB__timestamp_int";
        "SUB__timestamp_timestamp";
        "ADD__mutez_mutez_mutez";
        "SUB__mutez_mutez";
        "SUB_MUTEZ";
        "MUL__mutez_nat";
        "MUL__nat_mutez";
        "EDIV__mutez_nat";
        "EDIV__mutez_mutez";
        "OR__bool";
        "AND__bool";
        "XOR__bool";
        "NOT__bool";
        "ISNAT";
        "NEG__nat";
        "NEG__int";
        "ABS";
        "INT__nat";
        "ADD__int_int";
        "ADD__int_nat";
        "ADD__nat_int";
        "ADD__nat_nat";
        "SUB__int_int";
        "SUB__int_nat";
        "SUB__nat_int";
        "SUB__nat_nat";
        "MUL__int_int";
        "MUL__int_nat";
        "MUL__nat_int";
        "MUL__nat_nat";
        "EDIV__int_int";
        "EDIV__int_nat";
        "EDIV__nat_int";
        "EDIV__nat_nat";
        "LSL__nat";
        "LSR__nat";
        "OR__nat";
        "AND__nat_nat";
        "AND__int_nat";
        "XOR__nat";
        "NOT__int";
        "NOT__nat";
        "IF";
        "LOOP";
        "LOOP_LEFT";
        "EXEC";
        "APPLY";
        "FAILWITH";
        "COMPARE";
        "EQ";
        "NEQ";
        "LT";
        "GT";
        "LE";
        "GE";
        "ADDRESS";
        "CONTRACT";
        "TRANSFER_TOKENS";
        "IMPLICIT_ACCOUNT";
        "SET_DELEGATE";
        "NOW";
        "MIN_BLOCK_TIME";
        "BALANCE";
        "LEVEL";
        "CHECK_SIGNATURE";
        "HASH_KEY";
        "PACK";
        "UNPACK";
        "BLAKE2B";
        "SHA256";
        "SHA512";
        "SOURCE";
        "SENDER";
        "SELF_ADDRESS";
        "AMOUNT";
        "SAPLING_VERIFY_UPDATE";
        "CHAIN_ID";
        "NEVER";
        "VOTING_POWER";
        "TOTAL_VOTING_POWER";
        "KECCAK";
        "SHA3";
        "ADD__g1";
        "ADD__g2";
        "ADD__fr";
        "MUL__g1_fr";
        "MUL__g2_fr";
        "MUL__fr_fr";
        "MUL__fr_int";
        "MUL__fr_nat";
        "MUL__int_fr";
        "MUL__nat_fr";
        "INT__bls12_381_fr";
        "NEG__g1";
        "NEG__g2";
        "NEG__fr";
        "PAIRING_CHECK";
        "TICKET";
        "READ_TICKET";
        "SPLIT_TICKET";
        "JOIN_TICKETS";
      ]

  let instr_names_for_location_function =
    wrap_selected_names
      ~__LOC__
      "kinstr location"
      instrs
      [
        "DROP";
        "DUP";
        "SWAP";
        "PUSH";
        "UNIT";
        "PAIR";
        "CAR";
        "CDR";
        "UNPAIR";
        "SOME";
        "NONE";
        "IF_NONE";
        "MAP__option";
        "LEFT";
        "RIGHT";
        "IF_LEFT";
        "CONS";
        "NIL";
        "IF_CONS";
        "MAP__list";
        "ITER__list";
        "SIZE__list";
        "EMPTY_SET";
        "ITER__set";
        "MEM__set";
        "UPDATE__set";
        "SIZE__set";
        "EMPTY_MAP";
        "MAP__map";
        "ITER__map";
        "MEM__map";
        "GET__map";
        "MEM__map";
        "UPDATE__map";
        "GET_AND_UPDATE__map";
        "SIZE__map";
        "EMPTY_BIG_MAP";
        "MEM__big_map";
        "GET__big_map";
        "UPDATE__big_map";
        "GET_AND_UPDATE___big_map";
        "CONCAT__string_list";
        "CONCAT__string";
        "SLICE__string";
        "SIZE__string";
        "CONCAT__bytes_list";
        "CONCAT__bytes";
        "SLICE__bytes";
        "SIZE__bytes";
        "LSL__bytes";
        "LSR__bytes";
        "OR__bytes";
        "AND__bytes";
        "XOR__bytes";
        "NOT__bytes";
        "NAT__bytes";
        "BYTES__nat";
        "INT__bytes";
        "BYTES__int";
        "ADD__int_timestamp";
        "ADD__timestamp_int";
        "SUB__timestamp_int";
        "SUB__timestamp_timestamp";
        "ADD__mutez_mutez_mutez";
        "SUB__mutez_mutez";
        "SUB_MUTEZ";
        "MUL__mutez_nat";
        "MUL__nat_mutez";
        "EDIV__mutez_nat";
        "EDIV__mutez_mutez";
        "OR__bool";
        "AND__bool";
        "XOR__bool";
        "NOT__bool";
        "ISNAT";
        "NEG__nat";
        "NEG__int";
        "ABS";
        "INT__nat";
        "ADD__int_int";
        "ADD__int_nat";
        "ADD__nat_int";
        "ADD__nat_nat";
        "SUB__int_int";
        "SUB__int_nat";
        "SUB__nat_int";
        "SUB__nat_nat";
        "MUL__int_int";
        "MUL__int_nat";
        "MUL__nat_int";
        "MUL__nat_nat";
        "EDIV__int_int";
        "EDIV__int_nat";
        "EDIV__nat_int";
        "EDIV__nat_nat";
        "LSL__nat";
        "LSR__nat";
        "OR__nat";
        "AND__nat_nat";
        "AND__int_nat";
        "XOR__nat";
        "NOT__int";
        "NOT__nat";
        "IF";
        "LOOP";
        "LOOP_LEFT";
        "EXEC";
        "APPLY";
        "FAILWITH";
        "COMPARE";
        "EQ";
        "NEQ";
        "LT";
        "GT";
        "LE";
        "GE";
        "ADDRESS";
        "CONTRACT";
        "TRANSFER_TOKENS";
        "IMPLICIT_ACCOUNT";
        "SET_DELEGATE";
        "NOW";
        "MIN_BLOCK_TIME";
        "BALANCE";
        "LEVEL";
        "CHECK_SIGNATURE";
        "HASH_KEY";
        "PACK";
        "UNPACK";
        "BLAKE2B";
        "SHA256";
        "SHA512";
        "SOURCE";
        "SENDER";
        "SELF_ADDRESS";
        "AMOUNT";
        "SAPLING_VERIFY_UPDATE";
        "CHAIN_ID";
        "NEVER";
        "VOTING_POWER";
        "TOTAL_VOTING_POWER";
        "KECCAK";
        "SHA3";
        "ADD__g1";
        "ADD__g2";
        "ADD__fr";
        "MUL__g1_fr";
        "MUL__g2_fr";
        "MUL__fr_fr";
        "MUL__fr_int";
        "MUL__fr_nat";
        "MUL__int_fr";
        "MUL__nat_fr";
        "INT__bls12_381_fr";
        "NEG__g1";
        "NEG__g2";
        "NEG__fr";
        "PAIRING_CHECK";
        "TICKET";
        "READ_TICKET";
        "SPLIT_TICKET";
        "JOIN_TICKETS";
      ]

  let ty_names_for_gadt =
    wrap_selected_names
      ~__LOC__
      "ty GADT"
      tys
      [
        "unit";
        "int";
        "nat";
        "signature";
        "string";
        "bytes";
        "mutez";
        "key_hash";
        "key";
        "timestamp";
        "address";
        "bool";
        "pair";
        "or";
        "lambda";
        "option";
        "list";
        "set";
        "map";
        "big_map";
        "contract";
        "sapling_transaction";
        "sapling_transaction_deprecated";
        "sapling_state";
        "operation";
        "chain_id";
        "never";
        "bls12_381_g1";
        "bls12_381_g2";
        "bls12_381_fr";
        "ticket";
        "chest_key";
        "chest";
      ]

  let ty_names_for_ty_metadata =
    wrap_selected_names
      ~__LOC__
      "ty metadata"
      tys
      [
        "unit";
        "never";
        "int";
        "nat";
        "signature";
        "string";
        "bytes";
        "mutez";
        "bool";
        "key_hash";
        "key";
        "timestamp";
        "chain_id";
        "address";
        "pair";
        "or";
        "option";
        "lambda";
        "list";
        "set";
        "map";
        "big_map";
        "ticket";
        "contract";
        "sapling_transaction";
        "sapling_transaction_deprecated";
        "sapling_state";
        "operation";
        "bls12_381_g1";
        "bls12_381_g2";
        "bls12_381_fr";
        "chest";
        "chest_key";
      ]

  let ty_names_for_is_comparable =
    wrap_selected_names
      ~__LOC__
      "ty is_comparable"
      tys
      [
        "never";
        "unit";
        "int";
        "nat";
        "signature";
        "string";
        "bytes";
        "mutez";
        "bool";
        "key_hash";
        "key";
        "timestamp";
        "chain_id";
        "address";
        "pair";
        "or";
        "option";
        "lambda";
        "list";
        "set";
        "map";
        "big_map";
        "ticket";
        "contract";
        "sapling_transaction";
        "sapling_transaction_deprecated";
        "sapling_state";
        "operation";
        "bls12_381_g1";
        "bls12_381_g2";
        "bls12_381_fr";
        "chest";
        "chest_key";
      ]

  let ty_names_for_ty_traverse =
    wrap_selected_names
      ~__LOC__
      "ty traverse"
      tys
      [
        "unit";
        "int";
        "nat";
        "signature";
        "string";
        "bytes";
        "mutez";
        "key_hash";
        "key";
        "timestamp";
        "address";
        "bool";
        "sapling_transaction";
        "sapling_transaction_deprecated";
        "sapling_state";
        "operation";
        "chain_id";
        "never";
        "bls12_381_g1";
        "bls12_381_g2";
        "bls12_381_fr";
        "ticket";
        "chest_key";
        "chest";
        "pair";
        "or";
        "lambda";
        "option";
        "list";
        "set";
        "map";
        "big_map";
        "contract";
      ]

  let kinstr_gadt_cases = kinstr_gadt_cases_str tys instrs instr_names_for_gadt

  let ty_gadt_cases = ty_gadt_cases_str tys ty_names_for_gadt

  let kinstr_location_function_cases =
    kinstr_location_function_cases_str instrs instr_names_for_location_function

  let ty_metadata_function_cases =
    ty_metadata_function_cases_str tys ty_names_for_ty_metadata

  let is_comparable_function_cases =
    Format.asprintf
      "%a"
      Ocaml_syntax.print_pattern_matching_list
      (generate_is_comparable_function_cases tys ty_names_for_is_comparable)

  let kinstr_traverse_function_cases = []

  let ty_traverse_function_cases =
    ty_traverse_function_cases_str tys ty_names_for_ty_traverse

  let value_traverse_function_cases = ["| _ -> failwith \"TODO\""]
end

module Script_ir_translator (Env : Env) = struct
  let check_dupable_comparable_ty_cases = "_ -> failwith \"TODO\""

  let check_dupable_ty_cases = "_ -> failwith \"TODO\""

  let ty_eq_function_cases = "_ -> failwith \"TODO\""

  let parse_ty_function_cases = "_ -> failwith \"TODO\""

  let check_packable_function_cases = "_ -> failwith \"TODO\""

  let parse_data_functions = "let _ = failwith \"TODO\""

  let parse_data_function_cases = "_ -> failwith \"TODO\""

  let parse_instr_function_cases = "_ -> failwith \"TODO\""

  let has_lazy_storage_function_cases = "_ -> failwith \"TODO\""
end
