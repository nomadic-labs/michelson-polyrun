open Log
(* Convert a consise description of the Michelson language as defined
   in the Michelson_desc module into a rich Michelson data model as
   defined in Michelson_data_model. *)

let enrich_comparable_flag :
    Michelson_descr.comparable_flag -> Michelson_data_model.comparable_flag =
  function
  | Yes -> Yes
  | No -> No
  | List [] ->
      (* (Comparable iff empty condition) means always comparable. *)
      Yes
  | List [v] -> Var v
  | List [v1; v2] ->
      let default_result_var_name = "r" in
      if v1 = default_result_var_name || v2 = default_result_var_name then
        failwith
          (Printf.sprintf
             "Name %s is reserved, please chose another one"
             default_result_var_name) ;
      And (v1, v2, default_result_var_name)
  | List (_ :: _ :: _ :: _) ->
      (* Currently unsupported but could be added if the need arises by
         generating fresh names for intermediate results of binary And. *)
      failwith
        (Printf.sprintf
           "Too many type variables in a type attribute constraint.")

let enrich_type_argument ty_name :
    Michelson_descr.type_argument -> Michelson_data_model.type_argument =
  function
  | Any_type s -> Dm_enriching_custom_hooks.enrich_type_argument ~arg:s ~ty_name
  | Comparable_type s -> Type (s, Comparable)
  | Memo_size -> Memo_size

let enrich_type_def
    ({ty_name; ty_arity; ty_ocaml_repr; ty_rust_repr; ty_comparable} :
      Michelson_descr.type_def) : Michelson_data_model.type_def =
  let ty_arity = List.map (enrich_type_argument ty_name) ty_arity in
  {
    ty_name;
    ty_arity;
    ty_ocaml_repr;
    ty_rust_repr;
    ty_comparable = enrich_comparable_flag ty_comparable;
    ty_atomic =
      List.for_all
        (function
          | Michelson_data_model.Memo_size -> true
          | Michelson_data_model.Type _ -> false)
        ty_arity;
  }

let enrich_type_defs (tys : Michelson_descr.type_def list) :
    (string, Michelson_data_model.type_def) Hashtbl.t =
  let table = Hashtbl.create 100 in
  List.iter
    (fun ty ->
      let ty = enrich_type_def ty in
      Hashtbl.add table ty.Michelson_data_model.ty_name ty)
    tys ;
  table

let rec enrich_ty : Michelson_descr.ty -> Michelson_data_model.ty = function
  | Ty_var v -> Ty_var v
  | Ty_memo ty -> Ty_memo ty
  | Ty_app (s, l) -> Ty_app (s, List.map enrich_ty l)

let enrich_stack_ty (l, s) = (List.map enrich_ty l, s)

let rec list_free_type_vars ty_list =
  match (ty_list : Michelson_data_model.ty list) with
  | [] -> []
  | Ty_var v :: tl -> v :: list_free_type_vars tl
  | Ty_memo _ :: tl -> list_free_type_vars tl
  | Ty_app (_, ty_lst) :: tl ->
      list_free_type_vars ty_lst @ list_free_type_vars tl

let fresh_var kind instr_input_type instr_output_type =
  let used = list_free_type_vars (instr_input_type @ instr_output_type) in
  try
    List.find
      (fun varname -> not @@ List.mem varname used)
      (match kind with
      | `ty -> ["a"; "b"; "c"; "d"; "e"]
      | `stack_ty -> ["S"; "T"; "U"; "V"; "W"])
  with Not_found ->
    failwith "DM_enriching.fresh_var: fail to find a fresh name"

let enrich_instr_param data_env code_env :
    Michelson_descr.instr_param -> Michelson_data_model.instr_param = function
  | IA_type v -> IA_type v
  | IA_comparable_type v -> IA_comparable_type v
  | IA_data x -> (
      match List.assoc_opt x data_env with
      | Some ty -> IA_data (x, enrich_ty ty)
      | None ->
          invalid_arg
            (Printf.sprintf
               "Unknown type for variable '%s, please add a premise in the \
                typing rule"
               x))
  | IA_code c -> (
      match List.assoc_opt c code_env with
      | Some (input_ty, output_ty) ->
          IA_code (c, enrich_stack_ty input_ty, enrich_stack_ty output_ty)
      | None ->
          invalid_arg
            (Printf.sprintf
               "Unknown type for code variable '%s, please add a premise in \
                the typing rule"
               c))
  | IA_depth s -> IA_depth s
  | IA_extra s -> IA_extra s

let enrich_instr_param_list data_env code_env instr_constructor_name
    (params : Michelson_descr.instr_param list) :
    Michelson_data_model.instr_param_list =
  let params = List.map (enrich_instr_param data_env code_env) params in
  Dm_enriching_custom_hooks.enrich_instr_param_list
    ~params
    ~instr_constructor_name

let remove_prefix instr_name =
  let prefix = "t_instr_" in
  let offset = String.length prefix in
  let len = String.length instr_name - offset in
  let new_name =
    if String.starts_with ~prefix instr_name then
      String.sub instr_name offset len
    else instr_name
  in
  new_name

let enrich_instr_def
    ({
       instr_concrete_name;
       instr_constructor_name;
       instr_params;
       instr_typing_rules;
     } as instr_def :
      Michelson_descr.instruction_def) :
    Michelson_data_model.instruction_def list =
  log "Enriching instruction %a@." Michelson_descr.pp_instruction_def instr_def ;
  let open Michelson_descr in
  let def ~overload {name; typing_rule_premisses; typing_rule_conclusion} =
    let {
      typing_judgment_instr = _;
      typing_judgment_input;
      typing_judgment_output;
    } =
      typing_rule_conclusion
    in
    let instr_constructor_name =
      Dm_enriching_custom_hooks.ad_hoc_renaming
      @@
      if overload then
        Dm_enriching_custom_hooks.overloading_dispatch
          instr_concrete_name
          instr_constructor_name
          (fst typing_judgment_input)
      else instr_constructor_name
    in
    let ((top_stack_input, input_remaining_stack_name) as input_type) =
      enrich_stack_ty typing_judgment_input
    in
    let ((top_stack_output, output_remaining_stack_name) as output_type) =
      enrich_stack_ty typing_judgment_output
    in
    (* In the description we have a variable name for the remaining stack,
       wheras in the OCaml gadt, the remaining stack is always a pair, so we
       generate a fresh name for the top of the remaining stack *)
    let fresh_a = fresh_var `ty top_stack_input top_stack_output in
    let top_remaining_input_type = fresh_a in
    let top_remaining_output_type =
      (* if remaining stack are the same, the have the same top element type,
         else we generate two names *)
      if input_remaining_stack_name = output_remaining_stack_name then fresh_a
      else fresh_var `ty (Ty_var fresh_a :: top_stack_input) top_stack_output
    in
    let data_env =
      List.filter_map
        (function
          | Michelson_descr.TP_data (x, ty) -> Some (x, ty) | TP_instr _ -> None)
        typing_rule_premisses
    in
    let code_env =
      List.filter_map
        (* computing typing environment which associate types to "instructions" variable *)
          (function
          | (TP_instr ({typing_judgment_instr = IVar i; _} as j) :
              Michelson_descr.typing_premisse) ->
              Some (i, (j.typing_judgment_input, j.typing_judgment_output))
          | TP_instr {typing_judgment_instr = IApp _; _} | TP_data _ -> None)
          (* concrete instruction application or data are not code variables  *)
        typing_rule_premisses
    in
    let instr_type : Michelson_data_model.instr_type =
      {
        input_type;
        output_type;
        top_remaining_input_type;
        top_remaining_output_type;
      }
    in
    let instr_semantics =
      Dm_enriching_custom_hooks.instr_semantics ~instr_concrete_name
    in
    ({
       instr_name = remove_prefix name;
       instr_concrete_name;
       instr_constructor_name;
       instr_type;
       instr_params =
         enrich_instr_param_list
           data_env
           code_env
           instr_constructor_name
           instr_params;
       instr_semantics;
     }
      : Michelson_data_model.instruction_def)
  in
  (* Instructions with multiple typing rules are the one that have overloading.
     We assume one separate definition per typing rule. *)
  match instr_typing_rules with
  | [] ->
      failwith ("rule for " ^ instr_constructor_name ^ " misses typing rules")
  | [rule] -> [def ~overload:false rule] (* TODO standard treatment ? *)
  | _ -> List.map (def ~overload:true) instr_typing_rules

let enrich_instr_defs (l : Michelson_descr.instruction_def list) :
    (string, Michelson_data_model.instruction_def) Hashtbl.t =
  let table = Hashtbl.create 100 in
  List.iter
    (fun i ->
      let l = enrich_instr_def i in
      (List.iter (fun i ->
           Hashtbl.add table i.Michelson_data_model.instr_name i))
        l)
    l ;
  table
