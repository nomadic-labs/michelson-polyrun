(** Here we define a minimalist language to express semantic rules.

    Apart from control structures, executing any Michelson instruction
    can be decomposed in the following steps:
    - pop some values from the stack,
    - scrutinize the algebraic structure of some of them,
    - perform some computations,
    - push the results.

    This leads to the following definition, a semantic rule is composed of:
    - a list [stack_prefix] of variables to store the poped values,
    - a list of [bindings] for new variables, which are either
      - introduced by pattern matchings of the form "C x1 ... xn = y" where
        "C" is a Michelson data constructor of arity n, the "xi" variables are
        being bound and the "y" variable was previously bound, or
      - computations of the form "x = f y1 ... yn" where "x" is being bound,
        "f" is either a Michelson data constructor or an external function of
        arity "n", and the "yi" variables were previously bound,
    - a list [output_prefix] of variables being pushed back to the stack.
  *)

(** this alias is pure "type documentation" to mark position were strings
   represent variable names *)
type varname = string [@@deriving show]

(** Either the instruction uses a fixed prefix of the stack, then we can name
    each element to be used, or it's runtime defined, and there will be some
    sort of loop (cases to be explored latter) *)
type stack_prefix = Varlist of varname list | Runtime_depth [@@deriving show]

(* when operating on algebraic type, we want to pattern match them   *)
(* here is a definition of patterns *)
type algebraic_pattern = {constructor : string; vars : varname list}

let pp_algebraic_pattern ppf ap =
  Format.(
    fprintf
      ppf
      "%s %a"
      ap.constructor
      (pp_print_list ~pp_sep:(fun ppf () -> fprintf ppf " ") pp_print_string)
      ap.vars)

let show_algebraic_pattern ap = Format.asprintf "%a" pp_algebraic_pattern ap

(* For simplicity we consider expressions in A-normal form (https://en.wikipedia.org/wiki/A-normal_form).
   Non-variable expressions are built by applying an operator to a possibly empty list of variables. The
   operators can either be Michelson data constructors (for example for the semantic rule of the [PAIR]
   instruction, the [Pair] data constructor is used as an operator) or externally defined operations (for
   example for the semantic rule of the [BLAKE2B] instruction). *)
type expr = {operator : string; operands : varname list} [@@deriving show]

(* variable binding is either a pattern matching or an expr assignment*)
type var_bind =
  | Assign of (varname * expr)
  | Destruct of (algebraic_pattern * varname)

let pp_var_bind ppf vb =
  match vb with
  | Assign (var, expr) -> Format.(fprintf ppf "@[<h>%s=%a@]" var pp_expr expr)
  | Destruct (pat_ls, var) ->
      Format.(fprintf ppf "@[<h>%a=%s@]" pp_algebraic_pattern pat_ls var)

(** a semantic rule
   - describes the expected stack prefix,
   - binds some new variables to some "computation" over the prefix
   - describe the replacement of the prefix in the output stack

Expected invariant :
  - order of bindings matters
  - a binding's expr cannot use free variables
  - TBD: do we allow var shadowing ? not initially at least.
*)
type semrule = {
  stack_prefix : stack_prefix;
  bindings : var_bind list;
  output_prefix : varname list;
}
[@@deriving show]

let empty = {stack_prefix = Varlist []; bindings = []; output_prefix = []}

let vars_of_bind (vb : var_bind) =
  match vb with Assign (v, _) -> [v] | Destruct (pat, _) -> pat.vars

module Set = Set.Make (String)

let no_free_var (env : Set.t) (vb : var_bind) =
  match vb with
  | Assign (_, expr) -> List.for_all (fun var -> Set.mem var env) expr.operands
  | Destruct (_, v) -> Set.mem v env

(* This function extracts the vars bound in the rule while checking that the rule is well-formed. *)
let vars_from_rule stack_prefix bindings =
  let vars =
    match stack_prefix with
    | Varlist l -> Set.of_list l
    | Runtime_depth -> Set.empty
  in
  List.fold_left
    (fun vars binding ->
      assert (no_free_var vars binding) ;
      (* TODO: more informative failure (binding's expr cannot use free variables) *)
      List.fold_left
        (fun env var ->
          (* TODO: more informative failure (shadowing forbiden) *)
          assert (not @@ Set.mem var env) ;
          Set.add var env)
        vars
        (vars_of_bind binding))
    vars
    bindings

(** Asserting consistency of a smeantic rule  *)
let consistent_rule {stack_prefix; bindings; output_prefix} =
  (* TODO: more informative failure (undefined variable in outpus stack) *)
  assert (
    Set.subset
      (Set.of_list output_prefix)
      (vars_from_rule stack_prefix bindings))

(* TODO : write some smart constructors to ensure rule invariants.  Also they
   could generate fresh varname on the fly *)
