open Michelson_input

let print_in_file_in_directory ~directory ~filename pp =
  let output_file = Filename.concat directory filename in
  let output_chan = open_out output_file in
  let fmt = Format.formatter_of_out_channel output_chan in
  Format.printf "Generating %s@." output_file ;
  pp fmt ;
  close_out output_chan

let print_ocaml_protocol_file ~filename pp =
  print_in_file_in_directory ~directory:"generated/protocol" ~filename pp

let print_rust_file ~filename pp =
  print_in_file_in_directory
    ~directory:"generated/tezos-on-tezos/tezos_vm/src"
    ~filename
    pp

let () =
  let print_separation_line fmt () =
    Format.fprintf fmt "---------------------------------------@."
  in
  let text_output_file = "generated/text/output.txt" in
  let text_output_chan = open_out text_output_file in
  let text_output_fmt = Format.formatter_of_out_channel text_output_chan in
  let tys = DM_enriching.enrich_type_defs tys in
  let instrs = DM_enriching.enrich_instr_defs instrs in
  Ocaml_proto_backend_printer.print_ocaml_output
    ~print_file:print_ocaml_protocol_file
    tys
    instrs ;
  Format.fprintf
    text_output_fmt
    "%a%a%a"
    Text_backend.print_tys_grammar_entries
    tys
    print_separation_line
    ()
    Text_backend.print_instrs_gramar_entries
    instrs ;
  Rust_backend_printer.print_rust_output ~print_file:print_rust_file tys instrs ;
  flush_all () ;
  close_out text_output_chan
