(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2020 Metastate AG <hello@metastate.dev>                     *)
(* Copyright (c) 2021-2022 Nomadic Labs <contact@nomadic-labs.com>           *)
(* Copyright (c) 2022 Trili Tech <contact@trili.tech>                        *)
(* Copyright (c) 2022 DaiLambda, Inc. <contact@dailambda,jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Alpha_context
open Micheline
open Script
open Script_tc_errors
open Script_ir_annot
open Script_typed_ir
open Script_ir_unparser
module Typecheck_costs = Michelson_v1_gas.Cost_of.Typechecking
module Unparse_costs = Michelson_v1_gas.Cost_of.Unparsing
module Tc_context = Script_tc_context

type elab_conf = Script_ir_translator_config.elab_config

type ex_stack_ty = Ex_stack_ty : ('a, 's) stack_ty -> ex_stack_ty

(* Equality witnesses *)
type ('ta, 'tb) eq = Eq : ('same, 'same) eq

(*

   The following type represents an instruction parameterized by its
   continuation. During the elaboration of the typed term, a sequence
   of instructions in Micheline is read from left to right: hence, the
   elaboration needs to wait for the next instruction to be elaborated
   to be able to construct the current instruction.

*)
type ('a, 's, 'b, 'u) cinstr = {
  apply : 'r 'f. ('b, 'u, 'r, 'f) kinstr -> ('a, 's, 'r, 'f) kinstr;
}
[@@ocaml.unboxed]

(*

   While a [Script_typed_ir.descr] contains a fully defined
   instruction, [descr] contains a [cinstr], that is an instruction
   parameterized by the next instruction, as explained in the previous
   comment.

*)
type ('a, 's, 'b, 'u) descr = {
  loc : Script.location;
  bef : ('a, 's) stack_ty;
  aft : ('b, 'u) stack_ty;
  instr : ('a, 's, 'b, 'u) cinstr;
}

let close_descr {loc; bef; aft; instr} =
  let kinstr = instr.apply (IHalt loc) in
  {kloc = loc; kbef = bef; kaft = aft; kinstr}

let compose_descr :
    type a s b u c v.
    Script.location ->
    (a, s, b, u) descr ->
    (b, u, c, v) descr ->
    (a, s, c, v) descr =
 fun loc d1 d2 ->
  {
    loc;
    bef = d1.bef;
    aft = d2.aft;
    instr = {apply = (fun k -> d1.instr.apply (d2.instr.apply k))};
  }

type tc_context = Tc_context.t

(* ---- Error helpers -------------------------------------------------------*)

let location = function
  | Prim (loc, _, _, _)
  | Int (loc, _)
  | String (loc, _)
  | Bytes (loc, _)
  | Seq (loc, _) ->
      loc

let kind_equal a b =
  match (a, b) with
  | Int_kind, Int_kind
  | String_kind, String_kind
  | Bytes_kind, Bytes_kind
  | Prim_kind, Prim_kind
  | Seq_kind, Seq_kind ->
      true
  | _ -> false

let kind = function
  | Int _ -> Int_kind
  | String _ -> String_kind
  | Bytes _ -> Bytes_kind
  | Prim _ -> Prim_kind
  | Seq _ -> Seq_kind

let unexpected expr exp_kinds exp_ns exp_prims =
  match expr with
  | Int (loc, _) -> Invalid_kind (loc, Prim_kind :: exp_kinds, Int_kind)
  | String (loc, _) -> Invalid_kind (loc, Prim_kind :: exp_kinds, String_kind)
  | Bytes (loc, _) -> Invalid_kind (loc, Prim_kind :: exp_kinds, Bytes_kind)
  | Seq (loc, _) -> Invalid_kind (loc, Prim_kind :: exp_kinds, Seq_kind)
  | Prim (loc, name, _, _) -> (
      let open Michelson_v1_primitives in
      match (namespace name, exp_ns) with
      | Type_namespace, Type_namespace
      | Instr_namespace, Instr_namespace
      | Constant_namespace, Constant_namespace ->
          Invalid_primitive (loc, exp_prims, name)
      | ns, _ -> Invalid_namespace (loc, name, exp_ns, ns))

let check_kind kinds expr =
  let kind = kind expr in
  if List.exists (kind_equal kind) kinds then Result.return_unit
  else
    let loc = location expr in
    error (Invalid_kind (loc, kinds, kind))

let check_comparable :
    type a ac.
    Script.location -> (a, ac) ty -> (ac, Dependent_bool.yes) eq tzresult =
 fun loc ty ->
  match is_comparable ty with
  | Yes -> ok Eq
  | No ->
      let t = Script_ir_unparser.serialize_ty_for_error ty in
      error (Comparable_type_expected (loc, t))

let pack_node unparsed ctxt =
  let bytes =
    Data_encoding.(
      Binary.to_bytes_exn (tup2 (Fixed.string Plain 1) expr_encoding))
      ("\x05", unparsed)
  in
  (bytes, ctxt)

let pack_comparable_data ctxt ty data =
  unparse_comparable_data ctxt Optimized_legacy ty data
  >|=? fun (unparsed, ctxt) -> pack_node unparsed ctxt

let hash_bytes ctxt bytes =
  Gas.consume ctxt (Michelson_v1_gas.Cost_of.Interpreter.blake2b bytes)
  >|? fun ctxt -> (Script_expr_hash.(hash_bytes [bytes]), ctxt)

let hash_comparable_data ctxt ty data =
  pack_comparable_data ctxt ty data >>=? fun (bytes, ctxt) ->
  Lwt.return @@ hash_bytes ctxt bytes

(* ---- Tickets ------------------------------------------------------------ *)

(*
   All comparable types are dupable, this function exists only to not forget
   checking this property when adding new types.
*)
let check_dupable_comparable_ty : type a. a comparable_ty -> unit = function
  | _ -> failwith "TODO"

let check_dupable_ty ctxt loc ty =
  let rec aux : type a ac. location -> (a, ac) ty -> (unit, error) Gas_monad.t =
   fun loc ty ->
    let open Gas_monad.Syntax in
    let* () = Gas_monad.consume_gas Typecheck_costs.check_dupable_cycle in
    match ty with _ -> failwith "TODO"
  in
  let gas = aux loc ty in
  Gas_monad.run ctxt gas >>? fun (res, ctxt) ->
  match res with Ok () -> ok ctxt | Error e -> error e

let type_metadata_eq :
    type error_trace.
    error_details:(_, error_trace) error_details ->
    'a ty_metadata ->
    'b ty_metadata ->
    (unit, error_trace) result =
 fun ~error_details {size = size_a} {size = size_b} ->
  Type_size.check_eq ~error_details size_a size_b

let default_ty_eq_error loc ty1 ty2 =
  let ty1 = serialize_ty_for_error ty1 in
  let ty2 = serialize_ty_for_error ty2 in
  Inconsistent_types (loc, ty1, ty2)

let memo_size_eq :
    type error_trace.
    error_details:(_, error_trace) error_details ->
    Sapling.Memo_size.t ->
    Sapling.Memo_size.t ->
    (unit, error_trace) result =
 fun ~error_details ms1 ms2 ->
  if Sapling.Memo_size.equal ms1 ms2 then Result.return_unit
  else
    Error
      (match error_details with
      | Fast -> Inconsistent_types_fast
      | Informative _ -> trace_of_error @@ Inconsistent_memo_sizes (ms1, ms2))

(* Check that two types are equal.

   The result is an equality witness between the types of the two inputs within
   the gas monad (for gas consumption).
*)
let ty_eq :
    type a ac b bc error_trace.
    error_details:(Script.location, error_trace) error_details ->
    (a, ac) ty ->
    (b, bc) ty ->
    (((a, ac) ty, (b, bc) ty) eq, error_trace) Gas_monad.t =
 fun ~error_details ty1 ty2 ->
  let type_metadata_eq meta1 meta2 =
    Gas_monad.of_result (type_metadata_eq ~error_details meta1 meta2)
    |> Gas_monad.record_trace_eval ~error_details (fun loc ->
           default_ty_eq_error loc ty1 ty2)
  in
  let memo_size_eq ms1 ms2 =
    Gas_monad.of_result (memo_size_eq ~error_details ms1 ms2)
  in
  let rec help :
      type ta tac tb tbc.
      (ta, tac) ty ->
      (tb, tbc) ty ->
      (((ta, tac) ty, (tb, tbc) ty) eq, error_trace) Gas_monad.t =
   fun ty1 ty2 ->
    help0 ty1 ty2
    |> Gas_monad.record_trace_eval ~error_details (fun loc ->
           default_ty_eq_error loc ty1 ty2)
  and help0 :
      type ta tac tb tbc.
      (ta, tac) ty ->
      (tb, tbc) ty ->
      (((ta, tac) ty, (tb, tbc) ty) eq, error_trace) Gas_monad.t =
   fun ty1 ty2 ->
    let open Gas_monad.Syntax in
    let* () = Gas_monad.consume_gas Typecheck_costs.merge_cycle in
    let not_equal () =
      Gas_monad.of_result
      @@ Error
           (match error_details with
           | Fast -> (Inconsistent_types_fast : error_trace)
           | Informative loc ->
               trace_of_error @@ default_ty_eq_error loc ty1 ty2)
    in
    match (ty1, ty2) with _ -> failwith "TODO"
  in
  help ty1 ty2

(* Same as ty_eq but for stacks.
   A single error monad is used here because there is no need to
   recover from stack merging errors. *)
let rec stack_eq :
    type ta tb ts tu.
    Script.location ->
    context ->
    int ->
    (ta, ts) stack_ty ->
    (tb, tu) stack_ty ->
    (((ta, ts) stack_ty, (tb, tu) stack_ty) eq * context) tzresult =
 fun loc ctxt lvl stack1 stack2 ->
  match (stack1, stack2) with
  | Bot_t, Bot_t -> ok (Eq, ctxt)
  | Item_t (ty1, rest1), Item_t (ty2, rest2) ->
      Gas_monad.run ctxt @@ ty_eq ~error_details:(Informative loc) ty1 ty2
      |> record_trace (Bad_stack_item lvl)
      >>? fun (eq, ctxt) ->
      eq >>? fun Eq ->
      stack_eq loc ctxt (lvl + 1) rest1 rest2 >|? fun (Eq, ctxt) ->
      ((Eq : ((ta, ts) stack_ty, (tb, tu) stack_ty) eq), ctxt)
  | _, _ -> error Bad_stack_length

(* ---- Type checker results -------------------------------------------------*)

type ('a, 's) judgement =
  | Typed : ('a, 's, 'b, 'u) descr -> ('a, 's) judgement
  | Failed : {
      descr : 'b 'u. ('b, 'u) stack_ty -> ('a, 's, 'b, 'u) descr;
    }
      -> ('a, 's) judgement

(* ---- Type checker (Untyped expressions -> Typed IR) ----------------------*)

type ('a, 's, 'b, 'u, 'c, 'v) branch = {
  branch :
    'r 'f.
    ('a, 's, 'r, 'f) descr -> ('b, 'u, 'r, 'f) descr -> ('c, 'v, 'r, 'f) descr;
}
[@@unboxed]

let merge_branches :
    type a s b u c v.
    context ->
    Script.location ->
    (a, s) judgement ->
    (b, u) judgement ->
    (a, s, b, u, c, v) branch ->
    ((c, v) judgement * context) tzresult =
 fun ctxt loc btr bfr {branch} ->
  match (btr, bfr) with
  | Typed ({aft = aftbt; _} as dbt), Typed ({aft = aftbf; _} as dbf) ->
      let unmatched_branches () =
        let aftbt = serialize_stack_for_error ctxt aftbt in
        let aftbf = serialize_stack_for_error ctxt aftbf in
        Unmatched_branches (loc, aftbt, aftbf)
      in
      record_trace_eval
        unmatched_branches
        ( stack_eq loc ctxt 1 aftbt aftbf >|? fun (Eq, ctxt) ->
          (Typed (branch dbt dbf), ctxt) )
  | Failed {descr = descrt}, Failed {descr = descrf} ->
      let descr ret = branch (descrt ret) (descrf ret) in
      ok (Failed {descr}, ctxt)
  | Typed dbt, Failed {descr = descrf} ->
      ok (Typed (branch dbt (descrf dbt.aft)), ctxt)
  | Failed {descr = descrt}, Typed dbf ->
      ok (Typed (branch (descrt dbf.aft) dbf), ctxt)

let parse_memo_size (n : (location, _) Micheline.node) :
    Sapling.Memo_size.t tzresult =
  match n with
  | Int (_, z) -> (
      match Sapling.Memo_size.parse_z z with
      | Ok _ as ok_memo_size -> ok_memo_size
      | Error msg ->
          error
          @@ Invalid_syntactic_constant (location n, strip_locations n, msg))
  | _ -> error @@ Invalid_kind (location n, [Int_kind], kind n)

type ex_comparable_ty =
  | Ex_comparable_ty : 'a comparable_ty -> ex_comparable_ty

type ex_parameter_ty_and_entrypoints_node =
  | Ex_parameter_ty_and_entrypoints_node : {
      arg_type : ('a, _) ty;
      entrypoints : 'a entrypoints_node;
    }
      -> ex_parameter_ty_and_entrypoints_node

(** [parse_ty] can be used to parse regular types as well as parameter types
    together with their entrypoints.

    In the first case, use [~ret:Don't_parse_entrypoints], [parse_ty] will
    return an [ex_ty].

    In the second case, use [~ret:Parse_entrypoints], [parse_ty] will return
    an [ex_parameter_ty_and_entrypoints_node].
*)
type ('ret, 'name) parse_ty_ret =
  | Don't_parse_entrypoints : (ex_ty, unit) parse_ty_ret
  | Parse_entrypoints
      : (ex_parameter_ty_and_entrypoints_node, Entrypoint.t option) parse_ty_ret

let rec parse_ty :
    type ret name.
    context ->
    stack_depth:int ->
    legacy:bool ->
    allow_lazy_storage:bool ->
    allow_operation:bool ->
    allow_contract:bool ->
    allow_ticket:bool ->
    ret:(ret, name) parse_ty_ret ->
    Script.node ->
    (ret * context) tzresult =
 fun ctxt
     ~stack_depth
     ~legacy
     ~allow_lazy_storage
     ~allow_operation
     ~allow_contract
     ~allow_ticket
     ~ret
     node ->
  Gas.consume ctxt Typecheck_costs.parse_type_cycle >>? fun ctxt ->
  if Compare.Int.(stack_depth > 10000) then
    error Typechecking_too_many_recursive_calls
  else
    (match ret with
    | Don't_parse_entrypoints -> ok (node, (() : name))
    | Parse_entrypoints -> extract_entrypoint_annot node)
    >>? fun (node, name) ->
    let return ctxt ty : ret * context =
      match ret with
      | Don't_parse_entrypoints -> (Ex_ty ty, ctxt)
      | Parse_entrypoints ->
          let at_node =
            Option.map (fun name -> {name; original_type_expr = node}) name
          in
          ( Ex_parameter_ty_and_entrypoints_node
              {
                arg_type = ty;
                entrypoints = {at_node; nested = Entrypoints_None};
              },
            ctxt )
    in
    match node with _ -> failwith "TODO"

and parse_comparable_ty :
    context ->
    stack_depth:int ->
    Script.node ->
    (ex_comparable_ty * context) tzresult =
 fun ctxt ~stack_depth node ->
  parse_ty
    ~ret:Don't_parse_entrypoints
    ctxt
    ~stack_depth:(stack_depth + 1)
    ~legacy:false
    ~allow_lazy_storage:false
    ~allow_operation:false
    ~allow_contract:false
    ~allow_ticket:false
    node
  >>? fun (Ex_ty t, ctxt) ->
  match is_comparable t with
  | Yes -> ok (Ex_comparable_ty t, ctxt)
  | No ->
      error
        (Comparable_type_expected (location node, Micheline.strip_locations node))

and parse_passable_ty :
    type ret name.
    context ->
    stack_depth:int ->
    legacy:bool ->
    ret:(ret, name) parse_ty_ret ->
    Script.node ->
    (ret * context) tzresult =
 fun ctxt ~stack_depth ~legacy ->
  (parse_ty [@tailcall])
    ctxt
    ~stack_depth
    ~legacy
    ~allow_lazy_storage:true
    ~allow_operation:false
    ~allow_contract:true
    ~allow_ticket:true

and parse_any_ty :
    context ->
    stack_depth:int ->
    legacy:bool ->
    Script.node ->
    (ex_ty * context) tzresult =
 fun ctxt ~stack_depth ~legacy ->
  (parse_ty [@tailcall])
    ctxt
    ~stack_depth
    ~legacy
    ~allow_lazy_storage:true
    ~allow_operation:true
    ~allow_contract:true
    ~allow_ticket:true
    ~ret:Don't_parse_entrypoints

and parse_big_map_ty ctxt ~stack_depth ~legacy big_map_loc args map_annot =
  Gas.consume ctxt Typecheck_costs.parse_type_cycle >>? fun ctxt ->
  match args with
  | [key_ty; value_ty] ->
      check_type_annot big_map_loc map_annot >>? fun () ->
      parse_comparable_ty ~stack_depth:(stack_depth + 1) ctxt key_ty
      >>? fun (Ex_comparable_ty key_ty, ctxt) ->
      parse_big_map_value_ty
        ctxt
        ~stack_depth:(stack_depth + 1)
        ~legacy
        value_ty
      >>? fun (Ex_ty value_ty, ctxt) ->
      big_map_t big_map_loc key_ty value_ty >|? fun big_map_ty ->
      (Ex_ty big_map_ty, ctxt)
  | args -> error @@ Invalid_arity (big_map_loc, T_big_map, 2, List.length args)

and parse_big_map_value_ty ctxt ~stack_depth ~legacy value_ty =
  (parse_ty [@tailcall])
    ctxt
    ~stack_depth
    ~legacy
    ~allow_lazy_storage:false
    ~allow_operation:false
    ~allow_contract:false
    ~allow_ticket:true
    ~ret:Don't_parse_entrypoints
    value_ty

let parse_packable_ty ctxt ~stack_depth ~legacy node =
  (parse_ty [@tailcall])
    ctxt
    ~stack_depth
    ~legacy
    ~allow_lazy_storage:false
    ~allow_operation:false
    ~allow_contract:false
      (* type contract is forbidden in UNPACK because of
         https://gitlab.com/tezos/tezos/-/issues/301 *)
    ~allow_ticket:false
    ~ret:Don't_parse_entrypoints
    node

let parse_view_input_ty ctxt ~stack_depth ~legacy node =
  (parse_ty [@tailcall])
    ctxt
    ~stack_depth
    ~legacy
    ~allow_lazy_storage:false
    ~allow_operation:false
    ~allow_contract:true
    ~allow_ticket:false
    ~ret:Don't_parse_entrypoints
    node

let parse_view_output_ty ctxt ~stack_depth ~legacy node =
  (parse_ty [@tailcall])
    ctxt
    ~stack_depth
    ~legacy
    ~allow_lazy_storage:false
    ~allow_operation:false
    ~allow_contract:true
    ~allow_ticket:false
    ~ret:Don't_parse_entrypoints
    node

let parse_normal_storage_ty ctxt ~stack_depth ~legacy node =
  (parse_ty [@tailcall])
    ctxt
    ~stack_depth
    ~legacy
    ~allow_lazy_storage:true
    ~allow_operation:false
    ~allow_contract:false
    ~allow_ticket:true
    ~ret:Don't_parse_entrypoints
    node

let parse_storage_ty :
    context ->
    stack_depth:int ->
    legacy:bool ->
    Script.node ->
    (ex_ty * context) tzresult =
 fun ctxt ~stack_depth ~legacy node ->
  match node with
  | Prim
      ( loc,
        T_pair,
        [Prim (big_map_loc, T_big_map, args, map_annot); remaining_storage],
        storage_annot )
    when legacy (* Legacy check introduced before Ithaca. *) -> (
      match storage_annot with
      | [] ->
          (parse_normal_storage_ty [@tailcall]) ctxt ~stack_depth ~legacy node
      | [single]
        when Compare.Int.(String.length single > 0)
             && Compare.Char.(single.[0] = '%') ->
          (parse_normal_storage_ty [@tailcall]) ctxt ~stack_depth ~legacy node
      | _ ->
          (* legacy semantics of big maps used the wrong annotation parser *)
          Gas.consume ctxt Typecheck_costs.parse_type_cycle >>? fun ctxt ->
          parse_big_map_ty
            ctxt
            ~stack_depth:(stack_depth + 1)
            ~legacy
            big_map_loc
            args
            map_annot
          >>? fun (Ex_ty big_map_ty, ctxt) ->
          parse_normal_storage_ty
            ctxt
            ~stack_depth:(stack_depth + 1)
            ~legacy
            remaining_storage
          >>? fun (Ex_ty remaining_storage, ctxt) ->
          check_composed_type_annot loc storage_annot >>? fun () ->
          pair_t loc big_map_ty remaining_storage >|? fun (Ty_ex_c ty) ->
          (Ex_ty ty, ctxt))
  | _ -> (parse_normal_storage_ty [@tailcall]) ctxt ~stack_depth ~legacy node

(* check_packable: determine if a `ty` is packable into Michelson *)
let check_packable ~allow_contract loc root =
  let rec check : type t tc. (t, tc) ty -> unit tzresult = function
    (* /!\ When adding new lazy storage kinds, be sure to return an error. /!\
       Lazy storage should not be packable. *)
    | _ -> failwith "TODO"
  in
  check root

type toplevel = {
  code_field : Script.node;
  arg_type : Script.node;
  storage_type : Script.node;
  views : view_map;
}

type ('arg, 'storage) code =
  | Code : {
      code :
        (('arg, 'storage) pair, (operation Script_list.t, 'storage) pair) lambda;
      arg_type : ('arg, _) ty;
      storage_type : ('storage, _) ty;
      views : view_map;
      entrypoints : 'arg entrypoints;
      code_size : Cache_memory_helpers.sint;
    }
      -> ('arg, 'storage) code

type ex_script = Ex_script : ('a, 'c) Script_typed_ir.script -> ex_script

type ex_code = Ex_code : ('a, 'c) code -> ex_code

type 'storage typed_view =
  | Typed_view : {
      input_ty : ('input, _) ty;
      output_ty : ('output, _) ty;
      kinstr : ('input * 'storage, end_of_stack, 'output, end_of_stack) kinstr;
      original_code_expr : Script.node;
    }
      -> 'storage typed_view

type 'storage typed_view_map = (Script_string.t, 'storage typed_view) map

type (_, _) dig_proof_argument =
  | Dig_proof_argument :
      ('x, 'a * 's, 'a, 's, 'b, 't, 'c, 'u) stack_prefix_preservation_witness
      * ('x, _) ty
      * ('c, 'u) stack_ty
      -> ('b, 't) dig_proof_argument

type (_, _, _) dug_proof_argument =
  | Dug_proof_argument :
      (('a, 's, 'x, 'a * 's, 'b, 't, 'c, 'u) stack_prefix_preservation_witness
      * ('c, 'u) stack_ty)
      -> ('b, 't, 'x) dug_proof_argument

type (_, _) dipn_proof_argument =
  | Dipn_proof_argument :
      ('fa, 'fs, 'fb, 'fu, 'a, 's, 'b, 'u) stack_prefix_preservation_witness
      * context
      * ('fa, 'fs, 'fb, 'fu) descr
      * ('b, 'u) stack_ty
      -> ('a, 's) dipn_proof_argument

type (_, _) dropn_proof_argument =
  | Dropn_proof_argument :
      ('fa, 'fs, 'fa, 'fs, 'a, 's, 'a, 's) stack_prefix_preservation_witness
      * ('fa, 'fs) stack_ty
      -> ('a, 's) dropn_proof_argument

type (_, _, _) comb_proof_argument =
  | Comb_proof_argument :
      ('a, 'b, 's, 'c, 'd, 't) comb_gadt_witness * ('c, 'd * 't) stack_ty
      -> ('a, 'b, 's) comb_proof_argument

type (_, _, _) uncomb_proof_argument =
  | Uncomb_proof_argument :
      ('a, 'b, 's, 'c, 'd, 't) uncomb_gadt_witness * ('c, 'd * 't) stack_ty
      -> ('a, 'b, 's) uncomb_proof_argument

type 'before comb_get_proof_argument =
  | Comb_get_proof_argument :
      ('before, 'after) comb_get_gadt_witness * ('after, _) ty
      -> 'before comb_get_proof_argument

type ('rest, 'before) comb_set_proof_argument =
  | Comb_set_proof_argument :
      ('rest, 'before, 'after) comb_set_gadt_witness * ('after, _) ty
      -> ('rest, 'before) comb_set_proof_argument

type (_, _, _) dup_n_proof_argument =
  | Dup_n_proof_argument :
      ('a, 'b, 's, 't) dup_n_gadt_witness * ('t, _) ty
      -> ('a, 'b, 's) dup_n_proof_argument

let rec make_dug_proof_argument :
    type a s x xc.
    location ->
    int ->
    (x, xc) ty ->
    (a, s) stack_ty ->
    (a, s, x) dug_proof_argument option =
 fun loc n x stk ->
  match (n, stk) with
  | 0, rest -> Some (Dug_proof_argument (KRest, Item_t (x, rest)))
  | n, Item_t (v, rest) ->
      make_dug_proof_argument loc (n - 1) x rest
      |> Option.map @@ fun (Dug_proof_argument (n', aft')) ->
         Dug_proof_argument (KPrefix (loc, v, n'), Item_t (v, aft'))
  | _, _ -> None

let rec make_comb_get_proof_argument :
    type b bc. int -> (b, bc) ty -> b comb_get_proof_argument option =
 fun n ty ->
  match (n, ty) with
  | 0, value_ty -> Some (Comb_get_proof_argument (Comb_get_zero, value_ty))
  | 1, Pair_t (hd_ty, _, _annot, _) ->
      Some (Comb_get_proof_argument (Comb_get_one, hd_ty))
  | n, Pair_t (_, tl_ty, _annot, _) ->
      make_comb_get_proof_argument (n - 2) tl_ty
      |> Option.map
         @@ fun (Comb_get_proof_argument (comb_get_left_witness, ty')) ->
         Comb_get_proof_argument (Comb_get_plus_two comb_get_left_witness, ty')
  | _ -> None

let rec make_comb_set_proof_argument :
    type value valuec before beforec a s.
    context ->
    (a, s) stack_ty ->
    location ->
    int ->
    (value, valuec) ty ->
    (before, beforec) ty ->
    (value, before) comb_set_proof_argument tzresult =
 fun ctxt stack_ty loc n value_ty ty ->
  match (n, ty) with
  | 0, _ -> ok @@ Comb_set_proof_argument (Comb_set_zero, value_ty)
  | 1, Pair_t (_hd_ty, tl_ty, _, _) ->
      pair_t loc value_ty tl_ty >|? fun (Ty_ex_c after_ty) ->
      Comb_set_proof_argument (Comb_set_one, after_ty)
  | n, Pair_t (hd_ty, tl_ty, _, _) ->
      make_comb_set_proof_argument ctxt stack_ty loc (n - 2) value_ty tl_ty
      >>? fun (Comb_set_proof_argument (comb_set_left_witness, tl_ty')) ->
      pair_t loc hd_ty tl_ty' >|? fun (Ty_ex_c after_ty) ->
      Comb_set_proof_argument (Comb_set_plus_two comb_set_left_witness, after_ty)
  | _ ->
      let whole_stack = serialize_stack_for_error ctxt stack_ty in
      error (Bad_stack (loc, I_UPDATE, 2, whole_stack))

type 'a ex_ty_cstr =
  | Ex_ty_cstr : {
      ty : ('b, _) Script_typed_ir.ty;
      construct : 'b -> 'a;
      original_type_expr : Script.node;
    }
      -> 'a ex_ty_cstr

let find_entrypoint (type full fullc error_context error_trace)
    ~(error_details : (error_context, error_trace) error_details)
    (full : (full, fullc) ty) (entrypoints : full entrypoints) entrypoint :
    (full ex_ty_cstr, error_trace) Gas_monad.t =
  let open Gas_monad.Syntax in
  let rec find_entrypoint :
      type t tc.
      (t, tc) ty ->
      t entrypoints_node ->
      Entrypoint.t ->
      (t ex_ty_cstr, unit) Gas_monad.t =
   fun ty entrypoints entrypoint ->
    let* () = Gas_monad.consume_gas Typecheck_costs.find_entrypoint_cycle in
    match (ty, entrypoints) with
    | _, {at_node = Some {name; original_type_expr}; _}
      when Entrypoint.(name = entrypoint) ->
        return (Ex_ty_cstr {ty; construct = (fun e -> e); original_type_expr})
    | Or_t (tl, tr, _, _), {nested = Entrypoints_Or {left; right}; _} -> (
        Gas_monad.bind_recover (find_entrypoint tl left entrypoint) @@ function
        | Ok (Ex_ty_cstr {ty; construct; original_type_expr}) ->
            return
              (Ex_ty_cstr
                 {
                   ty;
                   construct = (fun e -> L (construct e));
                   original_type_expr;
                 })
        | Error () ->
            let+ (Ex_ty_cstr {ty; construct; original_type_expr}) =
              find_entrypoint tr right entrypoint
            in
            Ex_ty_cstr
              {ty; construct = (fun e -> R (construct e)); original_type_expr})
    | _, {nested = Entrypoints_None; _} -> Gas_monad.of_result (Error ())
  in
  let {root; original_type_expr} = entrypoints in
  Gas_monad.bind_recover (find_entrypoint full root entrypoint) @@ function
  | Ok f_t -> return f_t
  | Error () ->
      if Entrypoint.is_default entrypoint then
        return
          (Ex_ty_cstr {ty = full; construct = (fun e -> e); original_type_expr})
      else
        Gas_monad.of_result
        @@ Error
             (match error_details with
             | Fast -> (Inconsistent_types_fast : error_trace)
             | Informative _ -> trace_of_error @@ No_such_entrypoint entrypoint)

let find_entrypoint_for_type (type full fullc exp expc error_trace)
    ~error_details ~(full : (full, fullc) ty) ~(expected : (exp, expc) ty)
    entrypoints entrypoint :
    (Entrypoint.t * (exp, expc) ty, error_trace) Gas_monad.t =
  let open Gas_monad.Syntax in
  let* res = find_entrypoint ~error_details full entrypoints entrypoint in
  match res with
  | Ex_ty_cstr {ty; _} -> (
      match entrypoints.root.at_node with
      | Some {name; original_type_expr = _}
        when Entrypoint.is_root name && Entrypoint.is_default entrypoint ->
          Gas_monad.bind_recover
            (ty_eq ~error_details:Fast ty expected)
            (function
              | Ok Eq -> return (Entrypoint.default, (ty : (exp, expc) ty))
              | Error Inconsistent_types_fast ->
                  let+ Eq = ty_eq ~error_details full expected in
                  (Entrypoint.root, (full : (exp, expc) ty)))
      | _ ->
          let+ Eq = ty_eq ~error_details ty expected in
          (entrypoint, (ty : (exp, expc) ty)))

let well_formed_entrypoints (type full fullc) (full : (full, fullc) ty)
    entrypoints =
  let merge path (type t tc) (ty : (t, tc) ty)
      (entrypoints : t entrypoints_node) reachable
      ((first_unreachable, all) as acc) =
    match entrypoints.at_node with
    | None ->
        ok
          ( (if reachable then acc
            else
              match ty with
              | Or_t _ -> acc
              | _ -> (
                  match first_unreachable with
                  | None -> (Some (List.rev path), all)
                  | Some _ -> acc)),
            reachable )
    | Some {name; original_type_expr = _} ->
        if Entrypoint.Set.mem name all then error (Duplicate_entrypoint name)
        else ok ((first_unreachable, Entrypoint.Set.add name all), true)
  in
  let rec check :
      type t tc.
      (t, tc) ty ->
      t entrypoints_node ->
      prim list ->
      bool ->
      prim list option * Entrypoint.Set.t ->
      (prim list option * Entrypoint.Set.t) tzresult =
   fun t entrypoints path reachable acc ->
    match (t, entrypoints) with
    | Or_t (tl, tr, _, _), {nested = Entrypoints_Or {left; right}; _} ->
        merge (D_Left :: path) tl left reachable acc
        >>? fun (acc, l_reachable) ->
        merge (D_Right :: path) tr right reachable acc
        >>? fun (acc, r_reachable) ->
        check tl left (D_Left :: path) l_reachable acc >>? fun acc ->
        check tr right (D_Right :: path) r_reachable acc
    | _ -> ok acc
  in
  let init, reachable =
    match entrypoints.at_node with
    | None -> (Entrypoint.Set.empty, false)
    | Some {name; original_type_expr = _} ->
        (Entrypoint.Set.singleton name, true)
  in
  check full entrypoints [] reachable (None, init)
  >>? fun (first_unreachable, all) ->
  if not (Entrypoint.Set.mem Entrypoint.default all) then Result.return_unit
  else
    match first_unreachable with
    | None -> Result.return_unit
    | Some path -> error (Unreachable_entrypoint path)

type ex_parameter_ty_and_entrypoints =
  | Ex_parameter_ty_and_entrypoints : {
      arg_type : ('a, _) ty;
      entrypoints : 'a entrypoints;
    }
      -> ex_parameter_ty_and_entrypoints

let parse_parameter_ty_and_entrypoints :
    context ->
    stack_depth:int ->
    legacy:bool ->
    Script.node ->
    (ex_parameter_ty_and_entrypoints * context) tzresult =
 fun ctxt ~stack_depth ~legacy node ->
  parse_passable_ty
    ctxt
    ~stack_depth:(stack_depth + 1)
    ~legacy
    node
    ~ret:Parse_entrypoints
  >>? fun (Ex_parameter_ty_and_entrypoints_node {arg_type; entrypoints}, ctxt)
    ->
  (if legacy (* Legacy check introduced before Ithaca. *) then
   Result.return_unit
  else well_formed_entrypoints arg_type entrypoints)
  >|? fun () ->
  let entrypoints = {root = entrypoints; original_type_expr = node} in
  (Ex_parameter_ty_and_entrypoints {arg_type; entrypoints}, ctxt)

let parse_passable_ty = parse_passable_ty ~ret:Don't_parse_entrypoints

let parse_uint ~nb_bits =
  assert (Compare.Int.(nb_bits >= 0 && nb_bits <= 30)) ;
  let max_int = (1 lsl nb_bits) - 1 in
  let max_z = Z.of_int max_int in
  function
  | Micheline.Int (_, n) when Compare.Z.(Z.zero <= n) && Compare.Z.(n <= max_z)
    ->
      ok (Z.to_int n)
  | node ->
      error
      @@ Invalid_syntactic_constant
           ( location node,
             strip_locations node,
             "a positive " ^ string_of_int nb_bits
             ^ "-bit integer (between 0 and " ^ string_of_int max_int ^ ")" )

let parse_uint10 = parse_uint ~nb_bits:10

let parse_uint11 = parse_uint ~nb_bits:11

(* The type returned by this function is used to:
   - serialize and deserialize tickets when they are stored or transferred,
   - type the READ_TICKET instruction. *)
let opened_ticket_type loc ty = comparable_pair_3_t loc address_t ty nat_t

let comb_witness1 : type t tc. (t, tc) ty -> (t, unit -> unit) comb_witness =
  function
  | Pair_t _ -> Comb_Pair Comb_Any
  | _ -> Comb_Any

let _ = failwith "TODO"

let parse_view_name ctxt : Script.node -> (Script_string.t * context) tzresult =
  function
  | String (loc, v) as expr ->
      (* The limitation of length of string is same as entrypoint *)
      if Compare.Int.(String.length v > 31) then error (View_name_too_long v)
      else
        let rec check_char i =
          if Compare.Int.(i < 0) then ok v
          else if Script_ir_annot.is_allowed_char v.[i] then check_char (i - 1)
          else error (Bad_view_name loc)
        in
        Gas.consume ctxt (Typecheck_costs.check_printable v) >>? fun ctxt ->
        record_trace
          (Invalid_syntactic_constant
             ( loc,
               strip_locations expr,
               "string [a-zA-Z0-9_.%@] and the maximum string length of 31 \
                characters" ))
          ( check_char (String.length v - 1) >>? fun v ->
            Script_string.of_string v >|? fun s -> (s, ctxt) )
  | expr -> error @@ Invalid_kind (location expr, [String_kind], kind expr)

let parse_toplevel : context -> Script.expr -> (toplevel * context) tzresult =
 fun ctxt toplevel ->
  record_trace (Ill_typed_contract (toplevel, []))
  @@
  match root toplevel with
  | Int (loc, _) -> error (Invalid_kind (loc, [Seq_kind], Int_kind))
  | String (loc, _) -> error (Invalid_kind (loc, [Seq_kind], String_kind))
  | Bytes (loc, _) -> error (Invalid_kind (loc, [Seq_kind], Bytes_kind))
  | Prim (loc, _, _, _) -> error (Invalid_kind (loc, [Seq_kind], Prim_kind))
  | Seq (_, fields) -> (
      let rec find_fields ctxt p s c views fields =
        match fields with
        | [] -> ok (ctxt, (p, s, c, views))
        | Int (loc, _) :: _ -> error (Invalid_kind (loc, [Prim_kind], Int_kind))
        | String (loc, _) :: _ ->
            error (Invalid_kind (loc, [Prim_kind], String_kind))
        | Bytes (loc, _) :: _ ->
            error (Invalid_kind (loc, [Prim_kind], Bytes_kind))
        | Seq (loc, _) :: _ -> error (Invalid_kind (loc, [Prim_kind], Seq_kind))
        | Prim (loc, K_parameter, [arg], annot) :: rest -> (
            match p with
            | None -> find_fields ctxt (Some (arg, loc, annot)) s c views rest
            | Some _ -> error (Duplicate_field (loc, K_parameter)))
        | Prim (loc, K_storage, [arg], annot) :: rest -> (
            match s with
            | None -> find_fields ctxt p (Some (arg, loc, annot)) c views rest
            | Some _ -> error (Duplicate_field (loc, K_storage)))
        | Prim (loc, K_code, [arg], annot) :: rest -> (
            match c with
            | None -> find_fields ctxt p s (Some (arg, loc, annot)) views rest
            | Some _ -> error (Duplicate_field (loc, K_code)))
        | Prim (loc, ((K_parameter | K_storage | K_code) as name), args, _) :: _
          ->
            error (Invalid_arity (loc, name, 1, List.length args))
        | Prim (loc, K_view, [name; input_ty; output_ty; view_code], _) :: rest
          ->
            parse_view_name ctxt name >>? fun (str, ctxt) ->
            Gas.consume
              ctxt
              (Michelson_v1_gas.Cost_of.Interpreter.view_update str views)
            >>? fun ctxt ->
            if Script_map.mem str views then error (Duplicated_view_name loc)
            else
              let views' =
                Script_map.update
                  str
                  (Some {input_ty; output_ty; view_code})
                  views
              in
              find_fields ctxt p s c views' rest
        | Prim (loc, K_view, args, _) :: _ ->
            error (Invalid_arity (loc, K_view, 4, List.length args))
        | Prim (loc, name, _, _) :: _ ->
            let allowed = [K_parameter; K_storage; K_code; K_view] in
            error (Invalid_primitive (loc, allowed, name))
      in
      find_fields ctxt None None None (Script_map.empty string_t) fields
      >>? fun (ctxt, toplevel) ->
      match toplevel with
      | None, _, _, _ -> error (Missing_field K_parameter)
      | Some _, None, _, _ -> error (Missing_field K_storage)
      | Some _, Some _, None, _ -> error (Missing_field K_code)
      | ( Some (p, ploc, pannot),
          Some (s, sloc, sannot),
          Some (c, cloc, cannot),
          views ) ->
          Script_ir_annot.error_unexpected_annot ploc pannot >>? fun () ->
          Script_ir_annot.error_unexpected_annot cloc cannot >>? fun () ->
          Script_ir_annot.error_unexpected_annot sloc sannot >|? fun () ->
          ({code_field = c; arg_type = p; views; storage_type = s}, ctxt))

(* Normalize lambdas during parsing *)

let normalized_lam ~unparse_code_rec ~stack_depth ctxt kdescr code_field =
  unparse_code_rec ctxt ~stack_depth:(stack_depth + 1) Optimized code_field
  >|=? fun (code_field, ctxt) -> (Lam (kdescr, code_field), ctxt)

let normalized_lam_rec ~unparse_code_rec ~stack_depth ctxt kdescr code_field =
  unparse_code_rec ctxt ~stack_depth:(stack_depth + 1) Optimized code_field
  >|=? fun (code_field, ctxt) -> (LamRec (kdescr, code_field), ctxt)

(* -- parse data of any type -- *)

(*
  Some values, such as operations, tickets, or big map ids, are used only
  internally and are not allowed to be forged by users.
  In [parse_data], [allow_forged] should be [false] for:
  - PUSH
  - UNPACK
  - user-provided script parameters
  - storage on origination
  And [true] for:
  - internal calls parameters
  - storage after origination
*)

let rec parse_data :
    type a ac.
    unparse_code_rec:Script_ir_unparser.unparse_code_rec ->
    elab_conf:elab_conf ->
    stack_depth:int ->
    context ->
    allow_forged:bool ->
    (a, ac) ty ->
    Script.node ->
    (a * context) tzresult Lwt.t =
 fun ~unparse_code_rec ~elab_conf ~stack_depth ctxt ~allow_forged ty script_data ->
  Gas.consume ctxt Typecheck_costs.parse_data_cycle >>?= fun ctxt ->
  let non_terminal_recursion ctxt ty script_data =
    if Compare.Int.(stack_depth > 10_000) then
      tzfail Typechecking_too_many_recursive_calls
    else
      parse_data
        ~unparse_code_rec
        ~elab_conf
        ~stack_depth:(stack_depth + 1)
        ctxt
        ~allow_forged
        ty
        script_data
  in
  let parse_data_error () =
    let ty = serialize_ty_for_error ty in
    Invalid_constant (location script_data, strip_locations script_data, ty)
  in
  let fail_parse_data () = tzfail (parse_data_error ()) in
  let traced_no_lwt body = record_trace_eval parse_data_error body in
  let traced body = trace_eval parse_data_error body in
  let traced_fail err = Lwt.return @@ traced_no_lwt (error err) in
  let parse_items ctxt expr key_type value_type items item_wrapper =
    List.fold_left_es
      (fun (last_value, map, ctxt) item ->
        match item with
        | Prim (loc, D_Elt, [k; v], annot) ->
            (if elab_conf.legacy (* Legacy check introduced before Ithaca. *)
            then Result.return_unit
            else error_unexpected_annot loc annot)
            >>?= fun () ->
            non_terminal_recursion ctxt key_type k >>=? fun (k, ctxt) ->
            non_terminal_recursion ctxt value_type v >>=? fun (v, ctxt) ->
            Lwt.return
              ( (match last_value with
                | Some value ->
                    Gas.consume
                      ctxt
                      (Michelson_v1_gas.Cost_of.Interpreter.compare
                         key_type
                         value
                         k)
                    >>? fun ctxt ->
                    let c =
                      Script_comparable.compare_comparable key_type value k
                    in
                    if Compare.Int.(0 <= c) then
                      if Compare.Int.(0 = c) then
                        error (Duplicate_map_keys (loc, strip_locations expr))
                      else
                        error (Unordered_map_keys (loc, strip_locations expr))
                    else ok ctxt
                | None -> ok ctxt)
              >>? fun ctxt ->
                Gas.consume
                  ctxt
                  (Michelson_v1_gas.Cost_of.Interpreter.map_update k map)
                >|? fun ctxt ->
                (Some k, Script_map.update k (Some (item_wrapper v)) map, ctxt)
              )
        | Prim (loc, D_Elt, l, _) ->
            tzfail @@ Invalid_arity (loc, D_Elt, 2, List.length l)
        | Prim (loc, name, _, _) ->
            tzfail @@ Invalid_primitive (loc, [D_Elt], name)
        | Int _ | String _ | Bytes _ | Seq _ -> fail_parse_data ())
      (None, Script_map.empty key_type, ctxt)
      items
    |> traced
    >|=? fun (_, items, ctxt) -> (items, ctxt)
  in
  let parse_big_map_items (type t) ctxt expr (key_type : t comparable_ty)
      value_type items item_wrapper =
    List.fold_left_es
      (fun (last_key, {map; size}, ctxt) item ->
        match item with
        | Prim (loc, D_Elt, [k; v], annot) ->
            (if elab_conf.legacy (* Legacy check introduced before Ithaca. *)
            then Result.return_unit
            else error_unexpected_annot loc annot)
            >>?= fun () ->
            non_terminal_recursion ctxt key_type k >>=? fun (k, ctxt) ->
            hash_comparable_data ctxt key_type k >>=? fun (key_hash, ctxt) ->
            non_terminal_recursion ctxt value_type v >>=? fun (v, ctxt) ->
            Lwt.return
              ( (match last_key with
                | Some last_key ->
                    Gas.consume
                      ctxt
                      (Michelson_v1_gas.Cost_of.Interpreter.compare
                         key_type
                         last_key
                         k)
                    >>? fun ctxt ->
                    let c =
                      Script_comparable.compare_comparable key_type last_key k
                    in
                    if Compare.Int.(0 <= c) then
                      if Compare.Int.(0 = c) then
                        error (Duplicate_map_keys (loc, strip_locations expr))
                      else
                        error (Unordered_map_keys (loc, strip_locations expr))
                    else ok ctxt
                | None -> ok ctxt)
              >>? fun ctxt ->
                Gas.consume
                  ctxt
                  (Michelson_v1_gas.Cost_of.Interpreter.big_map_update
                     {map; size})
                >>? fun ctxt ->
                if Big_map_overlay.mem key_hash map then
                  error (Duplicate_map_keys (loc, strip_locations expr))
                else
                  ok
                    ( Some k,
                      {
                        map =
                          Big_map_overlay.add key_hash (k, item_wrapper v) map;
                        size = size + 1;
                      },
                      ctxt ) )
        | Prim (loc, D_Elt, l, _) ->
            tzfail @@ Invalid_arity (loc, D_Elt, 2, List.length l)
        | Prim (loc, name, _, _) ->
            tzfail @@ Invalid_primitive (loc, [D_Elt], name)
        | Int _ | String _ | Bytes _ | Seq _ -> fail_parse_data ())
      (None, {map = Big_map_overlay.empty; size = 0}, ctxt)
      items
    |> traced
    >|=? fun (_, map, ctxt) -> (map, ctxt)
  in
  let legacy = elab_conf.legacy in
  match (ty, script_data) with _ -> failwith "TODO"

and parse_view :
    type storage storagec.
    unparse_code_rec:Script_ir_unparser.unparse_code_rec ->
    elab_conf:elab_conf ->
    context ->
    (storage, storagec) ty ->
    view ->
    (storage typed_view * context) tzresult Lwt.t =
 fun ~unparse_code_rec
     ~elab_conf
     ctxt
     storage_type
     {input_ty; output_ty; view_code} ->
  let legacy = elab_conf.legacy in
  let input_ty_loc = location input_ty in
  record_trace_eval
    (fun () ->
      Ill_formed_type
        (Some "arg of view", strip_locations input_ty, input_ty_loc))
    (parse_view_input_ty ctxt ~stack_depth:0 ~legacy input_ty)
  >>?= fun (Ex_ty input_ty, ctxt) ->
  let output_ty_loc = location output_ty in
  record_trace_eval
    (fun () ->
      Ill_formed_type
        (Some "return of view", strip_locations output_ty, output_ty_loc))
    (parse_view_output_ty ctxt ~stack_depth:0 ~legacy output_ty)
  >>?= fun (Ex_ty output_ty, ctxt) ->
  pair_t input_ty_loc input_ty storage_type >>?= fun (Ty_ex_c pair_ty) ->
  parse_instr
    ~unparse_code_rec
    ~elab_conf
    ~stack_depth:0
    Tc_context.view
    ctxt
    view_code
    (Item_t (pair_ty, Bot_t))
  >>=? fun (judgement, ctxt) ->
  Lwt.return
  @@
  match judgement with
  | Failed {descr} ->
      let {kinstr; _} = close_descr (descr (Item_t (output_ty, Bot_t))) in
      ok
        ( Typed_view
            {input_ty; output_ty; kinstr; original_code_expr = view_code},
          ctxt )
  | Typed ({loc; aft; _} as descr) -> (
      let ill_type_view stack_ty loc =
        let actual = serialize_stack_for_error ctxt stack_ty in
        let expected_stack = Item_t (output_ty, Bot_t) in
        let expected = serialize_stack_for_error ctxt expected_stack in
        Ill_typed_view {loc; actual; expected}
      in
      match aft with
      | Item_t (ty, Bot_t) ->
          let error_details = Informative loc in
          Gas_monad.run ctxt
          @@ Gas_monad.record_trace_eval ~error_details (fun loc ->
                 ill_type_view aft loc)
          @@ ty_eq ~error_details ty output_ty
          >>? fun (eq, ctxt) ->
          eq >|? fun Eq ->
          let {kinstr; _} = close_descr descr in
          ( Typed_view
              {input_ty; output_ty; kinstr; original_code_expr = view_code},
            ctxt )
      | _ -> error (ill_type_view aft loc))

and parse_views :
    type storage storagec.
    unparse_code_rec:Script_ir_unparser.unparse_code_rec ->
    elab_conf:elab_conf ->
    context ->
    (storage, storagec) ty ->
    view_map ->
    (storage typed_view_map * context) tzresult Lwt.t =
 fun ~unparse_code_rec ~elab_conf ctxt storage_type views ->
  let aux ctxt name cur_view =
    Gas.consume
      ctxt
      (Michelson_v1_gas.Cost_of.Interpreter.view_update name views)
    >>?= fun ctxt ->
    parse_view ~unparse_code_rec ~elab_conf ctxt storage_type cur_view
  in
  Script_map.map_es_in_context aux ctxt views

and parse_kdescr :
    type arg argc ret retc.
    unparse_code_rec:Script_ir_unparser.unparse_code_rec ->
    elab_conf:elab_conf ->
    stack_depth:int ->
    tc_context ->
    context ->
    (arg, argc) ty ->
    (ret, retc) ty ->
    Script.node ->
    ((arg, end_of_stack, ret, end_of_stack) kdescr * context) tzresult Lwt.t =
 fun ~unparse_code_rec
     ~elab_conf
     ~stack_depth
     tc_context
     ctxt
     arg
     ret
     script_instr ->
  parse_instr
    ~unparse_code_rec
    ~elab_conf
    tc_context
    ctxt
    ~stack_depth:(stack_depth + 1)
    script_instr
    (Item_t (arg, Bot_t))
  >>=? function
  | Typed ({loc; aft = Item_t (ty, Bot_t) as stack_ty; _} as descr), ctxt ->
      Lwt.return
        (let error_details = Informative loc in
         Gas_monad.run ctxt
         @@ Gas_monad.record_trace_eval ~error_details (fun loc ->
                let ret = serialize_ty_for_error ret in
                let stack_ty = serialize_stack_for_error ctxt stack_ty in
                Bad_return (loc, stack_ty, ret))
         @@ ty_eq ~error_details ty ret
         >>? fun (eq, ctxt) ->
         eq >|? fun Eq ->
         ( (close_descr descr : (arg, end_of_stack, ret, end_of_stack) kdescr),
           ctxt ))
  | Typed {loc; aft = stack_ty; _}, ctxt ->
      let ret = serialize_ty_for_error ret in
      let stack_ty = serialize_stack_for_error ctxt stack_ty in
      tzfail @@ Bad_return (loc, stack_ty, ret)
  | Failed {descr}, ctxt ->
      return
        ( (close_descr (descr (Item_t (ret, Bot_t)))
            : (arg, end_of_stack, ret, end_of_stack) kdescr),
          ctxt )

and parse_lam_rec :
    type arg argc ret retc.
    unparse_code_rec:Script_ir_unparser.unparse_code_rec ->
    elab_conf:elab_conf ->
    stack_depth:int ->
    tc_context ->
    context ->
    (arg, argc) ty ->
    (ret, retc) ty ->
    ((arg, ret) lambda, _) ty ->
    Script.node ->
    ((arg, ret) lambda * context) tzresult Lwt.t =
 fun ~unparse_code_rec
     ~elab_conf
     ~stack_depth
     tc_context
     ctxt
     arg
     ret
     lambda_rec_ty
     script_instr ->
  parse_instr
    ~unparse_code_rec
    ~elab_conf
    tc_context
    ctxt
    ~stack_depth:(stack_depth + 1)
    script_instr
    (Item_t (arg, Item_t (lambda_rec_ty, Bot_t)))
  >>=? function
  | Typed ({loc; aft = Item_t (ty, Bot_t) as stack_ty; _} as descr), ctxt ->
      Lwt.return
        (let error_details = Informative loc in
         Gas_monad.run ctxt
         @@ Gas_monad.record_trace_eval ~error_details (fun loc ->
                let ret = serialize_ty_for_error ret in
                let stack_ty = serialize_stack_for_error ctxt stack_ty in
                Bad_return (loc, stack_ty, ret))
         @@ ty_eq ~error_details ty ret
         >>? fun (eq, ctxt) ->
         eq >|? fun Eq ->
         ( (close_descr descr
             : (arg, (arg, ret) lambda * end_of_stack, ret, end_of_stack) kdescr),
           ctxt ))
      >>=? fun (closed_descr, ctxt) ->
      (normalized_lam_rec [@ocaml.tailcall])
        ~unparse_code_rec
        ~stack_depth
        ctxt
        closed_descr
        script_instr
  | Typed {loc; aft = stack_ty; _}, ctxt ->
      let ret = serialize_ty_for_error ret in
      let stack_ty = serialize_stack_for_error ctxt stack_ty in
      tzfail @@ Bad_return (loc, stack_ty, ret)
  | Failed {descr}, ctxt ->
      (normalized_lam_rec [@ocaml.tailcall])
        ~unparse_code_rec
        ~stack_depth
        ctxt
        (close_descr (descr (Item_t (ret, Bot_t))))
        script_instr

and parse_instr :
    type a s.
    unparse_code_rec:Script_ir_unparser.unparse_code_rec ->
    elab_conf:elab_conf ->
    stack_depth:int ->
    tc_context ->
    context ->
    Script.node ->
    (a, s) stack_ty ->
    ((a, s) judgement * context) tzresult Lwt.t =
 fun ~unparse_code_rec
     ~elab_conf
     ~stack_depth
     tc_context
     ctxt
     script_instr
     stack_ty ->
  let for_logging_only x =
    if elab_conf.keep_extra_types_for_interpreter_logging then Some x else None
  in
  let check_item_ty (type a ac b bc) ctxt (exp : (a, ac) ty) (got : (b, bc) ty)
      loc name n m : ((a, b) eq * context) tzresult =
    record_trace_eval (fun () ->
        let stack_ty = serialize_stack_for_error ctxt stack_ty in
        Bad_stack (loc, name, m, stack_ty))
    @@ record_trace
         (Bad_stack_item n)
         ( Gas_monad.run ctxt @@ ty_eq ~error_details:(Informative loc) exp got
         >>? fun (eq, ctxt) ->
           eq >|? fun Eq -> ((Eq : (a, b) eq), ctxt) )
  in
  let log_stack loc stack_ty aft =
    match (elab_conf.type_logger, script_instr) with
    | None, _ | Some _, (Int _ | String _ | Bytes _) -> ()
    | Some log, (Prim _ | Seq _) ->
        (* Unparsing for logging is not carbonated as this
              is used only by the client and not the protocol *)
        let stack_ty_before = unparse_stack_uncarbonated stack_ty in
        let stack_ty_after = unparse_stack_uncarbonated aft in
        log loc ~stack_ty_before ~stack_ty_after
  in
  let typed_no_lwt ctxt loc instr aft =
    log_stack loc stack_ty aft ;
    let j = Typed {loc; instr; bef = stack_ty; aft} in
    Ok (j, ctxt)
  in
  let typed ctxt loc instr aft =
    Lwt.return @@ typed_no_lwt ctxt loc instr aft
  in
  Gas.consume ctxt Typecheck_costs.parse_instr_cycle >>?= fun ctxt ->
  let non_terminal_recursion tc_context ctxt script_instr stack_ty =
    if Compare.Int.(stack_depth > 10000) then
      tzfail Typechecking_too_many_recursive_calls
    else
      parse_instr
        ~unparse_code_rec
        ~elab_conf
        tc_context
        ctxt
        ~stack_depth:(stack_depth + 1)
        script_instr
        stack_ty
  in
  let bad_stack_error ctxt loc prim relevant_stack_portion =
    let whole_stack = serialize_stack_for_error ctxt stack_ty in
    error (Bad_stack (loc, prim, relevant_stack_portion, whole_stack))
  in
  let legacy = elab_conf.legacy in
  match (script_instr, stack_ty) with
  | _ -> failwith "TODO"
  (* Primitive parsing errors *)
  | ( Prim
        ( loc,
          (( I_DUP | I_SWAP | I_SOME | I_UNIT | I_PAIR | I_UNPAIR | I_CAR
           | I_CDR | I_CONS | I_CONCAT | I_SLICE | I_MEM | I_UPDATE | I_GET
           | I_EXEC | I_FAILWITH | I_SIZE | I_ADD | I_SUB | I_SUB_MUTEZ | I_MUL
           | I_EDIV | I_OR | I_AND | I_XOR | I_NOT | I_ABS | I_NEG | I_LSL
           | I_LSR | I_COMPARE | I_EQ | I_NEQ | I_LT | I_GT | I_LE | I_GE
           | I_TRANSFER_TOKENS | I_SET_DELEGATE | I_NOW | I_MIN_BLOCK_TIME
           | I_IMPLICIT_ACCOUNT | I_AMOUNT | I_BALANCE | I_LEVEL
           | I_CHECK_SIGNATURE | I_HASH_KEY | I_SOURCE | I_SENDER | I_BLAKE2B
           | I_SHA256 | I_SHA512 | I_ADDRESS | I_RENAME | I_PACK | I_ISNAT
           | I_INT | I_SELF | I_CHAIN_ID | I_NEVER | I_VOTING_POWER
           | I_TOTAL_VOTING_POWER | I_KECCAK | I_SHA3 | I_PAIRING_CHECK
           | I_TICKET | I_READ_TICKET | I_SPLIT_TICKET | I_JOIN_TICKETS
           | I_OPEN_CHEST ) as name),
          (_ :: _ as l),
          _ ),
      _ ) ->
      tzfail (Invalid_arity (loc, name, 0, List.length l))
  | ( Prim
        ( loc,
          (( I_NONE | I_LEFT | I_RIGHT | I_NIL | I_MAP | I_ITER | I_EMPTY_SET
           | I_LOOP | I_LOOP_LEFT | I_CONTRACT | I_CAST | I_UNPACK
           | I_CREATE_CONTRACT | I_EMIT ) as name),
          (([] | _ :: _ :: _) as l),
          _ ),
      _ ) ->
      tzfail (Invalid_arity (loc, name, 1, List.length l))
  | ( Prim
        ( loc,
          (( I_PUSH | I_VIEW | I_IF_NONE | I_IF_LEFT | I_IF_CONS | I_EMPTY_MAP
           | I_EMPTY_BIG_MAP | I_IF ) as name),
          (([] | [_] | _ :: _ :: _ :: _) as l),
          _ ),
      _ ) ->
      tzfail (Invalid_arity (loc, name, 2, List.length l))
  | ( Prim (loc, I_LAMBDA, (([] | [_] | [_; _] | _ :: _ :: _ :: _ :: _) as l), _),
      _ ) ->
      tzfail (Invalid_arity (loc, I_LAMBDA, 3, List.length l))
  (* Stack errors *)
  | ( Prim
        ( loc,
          (( I_ADD | I_SUB | I_SUB_MUTEZ | I_MUL | I_EDIV | I_AND | I_OR | I_XOR
           | I_LSL | I_LSR | I_CONCAT | I_PAIRING_CHECK ) as name),
          [],
          _ ),
      Item_t (ta, Item_t (tb, _)) ) ->
      let ta = serialize_ty_for_error ta in
      let tb = serialize_ty_for_error tb in
      tzfail (Undefined_binop (loc, name, ta, tb))
  | ( Prim
        ( loc,
          (( I_NEG | I_ABS | I_NOT | I_SIZE | I_EQ | I_NEQ | I_LT | I_GT | I_LE
           | I_GE
           (* CONCAT is both unary and binary; this case can only be triggered
               on a singleton stack *)
           | I_CONCAT ) as name),
          [],
          _ ),
      Item_t (t, _) ) ->
      let t = serialize_ty_for_error t in
      tzfail (Undefined_unop (loc, name, t))
  | Prim (loc, ((I_UPDATE | I_SLICE | I_OPEN_CHEST) as name), [], _), stack ->
      Lwt.return
        (let stack = serialize_stack_for_error ctxt stack in
         error (Bad_stack (loc, name, 3, stack)))
  | Prim (loc, I_CREATE_CONTRACT, _, _), stack ->
      let stack = serialize_stack_for_error ctxt stack in
      tzfail (Bad_stack (loc, I_CREATE_CONTRACT, 7, stack))
  | Prim (loc, I_TRANSFER_TOKENS, [], _), stack ->
      Lwt.return
        (let stack = serialize_stack_for_error ctxt stack in
         error (Bad_stack (loc, I_TRANSFER_TOKENS, 4, stack)))
  | ( Prim
        ( loc,
          (( I_DROP | I_DUP | I_CAR | I_CDR | I_UNPAIR | I_SOME | I_BLAKE2B
           | I_SHA256 | I_SHA512 | I_DIP | I_IF_NONE | I_LEFT | I_RIGHT
           | I_IF_LEFT | I_IF | I_LOOP | I_IF_CONS | I_IMPLICIT_ACCOUNT | I_NEG
           | I_ABS | I_INT | I_NOT | I_HASH_KEY | I_EQ | I_NEQ | I_LT | I_GT
           | I_LE | I_GE | I_SIZE | I_FAILWITH | I_RENAME | I_PACK | I_ISNAT
           | I_ADDRESS | I_SET_DELEGATE | I_CAST | I_MAP | I_ITER | I_LOOP_LEFT
           | I_UNPACK | I_CONTRACT | I_NEVER | I_KECCAK | I_SHA3 | I_READ_TICKET
           | I_JOIN_TICKETS ) as name),
          _,
          _ ),
      stack ) ->
      Lwt.return
        (let stack = serialize_stack_for_error ctxt stack in
         error (Bad_stack (loc, name, 1, stack)))
  | ( Prim
        ( loc,
          (( I_SWAP | I_PAIR | I_CONS | I_GET | I_MEM | I_EXEC
           | I_CHECK_SIGNATURE | I_ADD | I_SUB | I_SUB_MUTEZ | I_MUL | I_EDIV
           | I_AND | I_OR | I_XOR | I_LSL | I_LSR | I_COMPARE | I_PAIRING_CHECK
           | I_TICKET | I_SPLIT_TICKET ) as name),
          _,
          _ ),
      stack ) ->
      Lwt.return
        (let stack = serialize_stack_for_error ctxt stack in
         error (Bad_stack (loc, name, 2, stack)))
  (* Generic parsing errors *)
  | expr, _ ->
      tzfail
      @@ unexpected
           expr
           [Seq_kind]
           Instr_namespace
           [
             I_ABS;
             I_ADD;
             I_AMOUNT;
             I_AND;
             I_BALANCE;
             I_BLAKE2B;
             I_CAR;
             I_CDR;
             I_CHECK_SIGNATURE;
             I_COMPARE;
             I_CONCAT;
             I_CONS;
             I_CREATE_CONTRACT;
             I_DIG;
             I_DIP;
             I_DROP;
             I_DUG;
             I_DUP;
             I_EDIV;
             I_EMPTY_BIG_MAP;
             I_EMPTY_MAP;
             I_EMPTY_SET;
             I_EQ;
             I_EXEC;
             I_FAILWITH;
             I_GE;
             I_GET;
             I_GET_AND_UPDATE;
             I_GT;
             I_HASH_KEY;
             I_IF;
             I_IF_CONS;
             I_IF_LEFT;
             I_IF_NONE;
             I_IMPLICIT_ACCOUNT;
             I_INT;
             I_ITER;
             I_JOIN_TICKETS;
             I_KECCAK;
             I_LAMBDA;
             I_LE;
             I_LEFT;
             I_LEVEL;
             I_LOOP;
             I_LSL;
             I_LSR;
             I_LT;
             I_MAP;
             I_MEM;
             I_MIN_BLOCK_TIME;
             I_MUL;
             I_NEG;
             I_NEQ;
             I_NEVER;
             I_NIL;
             I_NONE;
             I_NOT;
             I_NOW;
             I_OPEN_CHEST;
             I_OR;
             I_PAIR;
             I_PAIRING_CHECK;
             I_PUSH;
             I_READ_TICKET;
             I_RIGHT;
             I_SAPLING_EMPTY_STATE;
             I_SAPLING_VERIFY_UPDATE;
             I_SELF;
             I_SELF_ADDRESS;
             I_SENDER;
             I_SHA256;
             I_SHA3;
             I_SHA512;
             I_SIZE;
             I_SOME;
             I_SOURCE;
             I_SPLIT_TICKET;
             I_SUB;
             I_SUB_MUTEZ;
             I_SWAP;
             I_TICKET;
             I_TOTAL_VOTING_POWER;
             I_TRANSFER_TOKENS;
             I_UNIT;
             I_UNPAIR;
             I_UPDATE;
             I_VIEW;
             I_VOTING_POWER;
             I_XOR;
           ]

and parse_contract_data :
    type arg argc.
    stack_depth:int ->
    context ->
    Script.location ->
    (arg, argc) ty ->
    Destination.t ->
    entrypoint:Entrypoint.t ->
    (context * arg typed_contract) tzresult Lwt.t =
 fun ~stack_depth ctxt loc arg destination ~entrypoint ->
  let error_details = Informative loc in
  parse_contract
    ~stack_depth:(stack_depth + 1)
    ctxt
    ~error_details
    loc
    arg
    destination
    ~entrypoint
  >>=? fun (ctxt, res) -> Lwt.return (res >|? fun res -> (ctxt, res))

(* [parse_contract] is used both to:
   - parse contract data by [parse_data] ([parse_contract_data])
   - to execute the [CONTRACT] instruction ([parse_contract_for_script]).

   The return type resembles the [Gas_monad]:
   - the outer [tzresult] is for gas exhaustion and internal errors
   - the inner [result] is for other legitimate cases of failure.

   The inner [result] is turned into an [option] by [parse_contract_for_script].
   Both [tzresult] are merged by [parse_contract_data].
*)
and parse_contract :
    type arg argc err.
    stack_depth:int ->
    context ->
    error_details:(location, err) error_details ->
    Script.location ->
    (arg, argc) ty ->
    Destination.t ->
    entrypoint:Entrypoint.t ->
    (context * (arg typed_contract, err) result) tzresult Lwt.t =
 fun ~stack_depth ctxt ~error_details loc arg destination ~entrypoint ->
  let error ctxt f_err : context * (_, err) result =
    ( ctxt,
      Error
        (match error_details with
        | Fast -> (Inconsistent_types_fast : err)
        | Informative loc -> trace_of_error @@ f_err loc) )
  in
  Gas.consume ctxt Typecheck_costs.parse_instr_cycle >>?= fun ctxt ->
  match destination with
  | Contract contract -> (
      match contract with
      | Implicit destination ->
          Lwt.return
          @@
          if Entrypoint.is_default entrypoint then
            (* An implicit account on the "default" entrypoint always exists and has type unit
               or (ticket cty). *)
            let typecheck =
              let open Gas_monad.Syntax in
              let* () = Gas_monad.consume_gas Typecheck_costs.merge_cycle in
              match arg with
              | Unit_t ->
                  return (Typed_implicit destination : arg typed_contract)
              | Ticket_t _ as ticket_ty ->
                  return (Typed_implicit_with_ticket {ticket_ty; destination})
              | _ ->
                  Gas_monad.of_result
                  @@ Error
                       (match error_details with
                       | Fast -> (Inconsistent_types_fast : err)
                       | Informative loc ->
                           trace_of_error @@ default_ty_eq_error loc arg unit_t)
            in
            Gas_monad.run ctxt typecheck >|? fun (v, ctxt) -> (ctxt, v)
          else
            (* An implicit account on any other entrypoint is not a valid contract. *)
            ok @@ error ctxt (fun _loc -> No_such_entrypoint entrypoint)
      | Originated contract_hash ->
          trace
            (Invalid_contract (loc, contract))
            ( Contract.get_script_code ctxt contract_hash
            >>=? fun (ctxt, code) ->
              Lwt.return
                (match code with
                | None ->
                    ok
                      (error ctxt (fun loc -> Invalid_contract (loc, contract)))
                | Some code ->
                    Script.force_decode_in_context
                      ~consume_deserialization_gas:When_needed
                      ctxt
                      code
                    >>? fun (code, ctxt) ->
                    (* can only fail because of gas *)
                    parse_toplevel ctxt code >>? fun ({arg_type; _}, ctxt) ->
                    parse_parameter_ty_and_entrypoints
                      ctxt
                      ~stack_depth:(stack_depth + 1)
                      ~legacy:true
                      arg_type
                    >>? fun ( Ex_parameter_ty_and_entrypoints
                                {arg_type = targ; entrypoints},
                              ctxt ) ->
                    Gas_monad.run ctxt
                    @@ find_entrypoint_for_type
                         ~error_details
                         ~full:targ
                         ~expected:arg
                         entrypoints
                         entrypoint
                    >|? fun (entrypoint_arg, ctxt) ->
                    ( ctxt,
                      entrypoint_arg >|? fun (entrypoint, arg_ty) ->
                      Typed_originated {arg_ty; contract_hash; entrypoint} )) ))
  | Zk_rollup zk_rollup ->
      Zk_rollup.assert_exist ctxt zk_rollup >|=? fun ctxt ->
      if Entrypoint.(is_deposit entrypoint) then
        match arg with
        | Pair_t (Ticket_t (_, _), Bytes_t, _, _) ->
            ( ctxt,
              ok
              @@ (Typed_zk_rollup {arg_ty = arg; zk_rollup}
                   : arg typed_contract) )
        | _ ->
            error ctxt (fun loc ->
                Zk_rollup_bad_deposit_parameter (loc, serialize_ty_for_error arg))
      else error ctxt (fun _loc -> No_such_entrypoint entrypoint)
  | Sc_rollup sc_rollup ->
      Sc_rollup.parameters_type ctxt sc_rollup
      >>=? fun (parameters_type, ctxt) ->
      Lwt.return
        (match parameters_type with
        | None ->
            ok
              (error ctxt (fun _loc ->
                   Sc_rollup.Errors.Sc_rollup_does_not_exist sc_rollup))
        | Some parameters_type ->
            Script.force_decode_in_context
              ~consume_deserialization_gas:When_needed
              ctxt
              parameters_type
            >>? fun (parameters_type, ctxt) ->
            parse_parameter_ty_and_entrypoints
              ctxt
              ~stack_depth:(stack_depth + 1)
              ~legacy:true
              (root parameters_type)
            >>? fun ( Ex_parameter_ty_and_entrypoints
                        {arg_type = full; entrypoints},
                      ctxt ) ->
            Gas_monad.run ctxt
            @@ find_entrypoint_for_type
                 ~error_details
                 ~full
                 ~expected:arg
                 entrypoints
                 entrypoint
            >|? fun (entrypoint_arg, ctxt) ->
            ( ctxt,
              entrypoint_arg >|? fun (entrypoint, arg_ty) ->
              Typed_sc_rollup {arg_ty; sc_rollup; entrypoint} ))

(* Same as [parse_contract], but does not fail when the contact is missing or
   if the expected type doesn't match the actual one. In that case None is
   returned and some overapproximation of the typechecking gas is consumed.
   This can still fail on gas exhaustion. *)
let parse_contract_for_script :
    type arg argc.
    context ->
    Script.location ->
    (arg, argc) ty ->
    Destination.t ->
    entrypoint:Entrypoint.t ->
    (context * arg typed_contract option) tzresult Lwt.t =
 fun ctxt loc arg destination ~entrypoint ->
  parse_contract
    ~stack_depth:0
    ctxt
    ~error_details:Fast
    loc
    arg
    destination
    ~entrypoint
  >|=? fun (ctxt, res) ->
  ( ctxt,
    match res with Ok res -> Some res | Error Inconsistent_types_fast -> None )

let view_size view =
  let open Script_typed_ir_size in
  node_size view.view_code ++ node_size view.input_ty
  ++ node_size view.output_ty

let code_size ctxt code views =
  let open Script_typed_ir_size in
  let views_size = Script_map.fold (fun _ v s -> view_size v ++ s) views zero in
  (* The size of the storage_type and the arg_type is counted by
     [lambda_size]. *)
  let ir_size = lambda_size code in
  let nodes, code_size = views_size ++ ir_size in
  (* We consume gas after the fact in order to not have to instrument
     [node_size] (for efficiency).
     This is safe, as we already pay gas proportional to [views_size] and
     [ir_size] during their typechecking. *)
  Gas.consume ctxt (Script_typed_ir_size_costs.nodes_cost ~nodes)
  >|? fun ctxt -> (code_size, ctxt)

let parse_code :
    unparse_code_rec:Script_ir_unparser.unparse_code_rec ->
    elab_conf:elab_conf ->
    context ->
    code:lazy_expr ->
    (ex_code * context) tzresult Lwt.t =
 fun ~unparse_code_rec ~elab_conf ctxt ~code ->
  Script.force_decode_in_context
    ~consume_deserialization_gas:When_needed
    ctxt
    code
  >>?= fun (code, ctxt) ->
  let legacy = elab_conf.legacy in
  Global_constants_storage.expand ctxt code >>=? fun (ctxt, code) ->
  parse_toplevel ctxt code
  >>?= fun ({arg_type; storage_type; code_field; views}, ctxt) ->
  let arg_type_loc = location arg_type in
  record_trace
    (Ill_formed_type (Some "parameter", code, arg_type_loc))
    (parse_parameter_ty_and_entrypoints ctxt ~stack_depth:0 ~legacy arg_type)
  >>?= fun (Ex_parameter_ty_and_entrypoints {arg_type; entrypoints}, ctxt) ->
  let storage_type_loc = location storage_type in
  record_trace
    (Ill_formed_type (Some "storage", code, storage_type_loc))
    (parse_storage_ty ctxt ~stack_depth:0 ~legacy storage_type)
  >>?= fun (Ex_ty storage_type, ctxt) ->
  pair_t storage_type_loc arg_type storage_type
  >>?= fun (Ty_ex_c arg_type_full) ->
  pair_t storage_type_loc list_operation_t storage_type
  >>?= fun (Ty_ex_c ret_type_full) ->
  trace
    (Ill_typed_contract (code, []))
    (parse_kdescr
       ~unparse_code_rec
       Tc_context.(toplevel ~storage_type ~param_type:arg_type ~entrypoints)
       ~elab_conf
       ctxt
       ~stack_depth:0
       arg_type_full
       ret_type_full
       code_field)
  >>=? fun (kdescr, ctxt) ->
  let code = Lam (kdescr, code_field) in
  Lwt.return
    ( code_size ctxt code views >>? fun (code_size, ctxt) ->
      ok
        ( Ex_code
            (Code {code; arg_type; storage_type; views; entrypoints; code_size}),
          ctxt ) )

let parse_storage :
    unparse_code_rec:Script_ir_unparser.unparse_code_rec ->
    elab_conf:elab_conf ->
    context ->
    allow_forged:bool ->
    ('storage, _) ty ->
    storage:lazy_expr ->
    ('storage * context) tzresult Lwt.t =
 fun ~unparse_code_rec ~elab_conf ctxt ~allow_forged storage_type ~storage ->
  Script.force_decode_in_context
    ~consume_deserialization_gas:When_needed
    ctxt
    storage
  >>?= fun (storage, ctxt) ->
  trace_eval
    (fun () ->
      let storage_type = serialize_ty_for_error storage_type in
      Ill_typed_data (None, storage, storage_type))
    (parse_data
       ~unparse_code_rec
       ~elab_conf
       ~stack_depth:0
       ctxt
       ~allow_forged
       storage_type
       (root storage))

let parse_script :
    unparse_code_rec:Script_ir_unparser.unparse_code_rec ->
    elab_conf:elab_conf ->
    context ->
    allow_forged_in_storage:bool ->
    Script.t ->
    (ex_script * context) tzresult Lwt.t =
 fun ~unparse_code_rec ~elab_conf ctxt ~allow_forged_in_storage {code; storage} ->
  parse_code ~unparse_code_rec ~elab_conf ctxt ~code
  >>=? fun ( Ex_code
               (Code
                 {code; arg_type; storage_type; views; entrypoints; code_size}),
             ctxt ) ->
  parse_storage
    ~unparse_code_rec
    ~elab_conf
    ctxt
    ~allow_forged:allow_forged_in_storage
    storage_type
    ~storage
  >|=? fun (storage, ctxt) ->
  ( Ex_script
      (Script
         {code_size; code; arg_type; storage; storage_type; views; entrypoints}),
    ctxt )

type typechecked_code_internal =
  | Typechecked_code_internal : {
      toplevel : toplevel;
      arg_type : ('arg, _) ty;
      storage_type : ('storage, _) ty;
      entrypoints : 'arg entrypoints;
      typed_views : 'storage typed_view_map;
      type_map : type_map;
    }
      -> typechecked_code_internal

let typecheck_code :
    unparse_code_rec:Script_ir_unparser.unparse_code_rec ->
    legacy:bool ->
    show_types:bool ->
    context ->
    Script.expr ->
    (typechecked_code_internal * context) tzresult Lwt.t =
 fun ~unparse_code_rec ~legacy ~show_types ctxt code ->
  (* Constants need to be expanded or [parse_toplevel] may fail. *)
  Global_constants_storage.expand ctxt code >>=? fun (ctxt, code) ->
  parse_toplevel ctxt code >>?= fun (toplevel, ctxt) ->
  let {arg_type; storage_type; code_field; views} = toplevel in
  let type_map = ref [] in
  let arg_type_loc = location arg_type in
  record_trace
    (Ill_formed_type (Some "parameter", code, arg_type_loc))
    (parse_parameter_ty_and_entrypoints ctxt ~stack_depth:0 ~legacy arg_type)
  >>?= fun (Ex_parameter_ty_and_entrypoints {arg_type; entrypoints}, ctxt) ->
  let storage_type_loc = location storage_type in
  record_trace
    (Ill_formed_type (Some "storage", code, storage_type_loc))
    (parse_storage_ty ctxt ~stack_depth:0 ~legacy storage_type)
  >>?= fun (ex_storage_type, ctxt) ->
  let (Ex_ty storage_type) = ex_storage_type in
  pair_t storage_type_loc arg_type storage_type
  >>?= fun (Ty_ex_c arg_type_full) ->
  pair_t storage_type_loc list_operation_t storage_type
  >>?= fun (Ty_ex_c ret_type_full) ->
  let type_logger loc ~stack_ty_before ~stack_ty_after =
    type_map := (loc, (stack_ty_before, stack_ty_after)) :: !type_map
  in
  let type_logger = if show_types then Some type_logger else None in
  let elab_conf = Script_ir_translator_config.make ~legacy ?type_logger () in
  let result =
    parse_kdescr
      ~unparse_code_rec
      (Tc_context.toplevel ~storage_type ~param_type:arg_type ~entrypoints)
      ctxt
      ~elab_conf
      ~stack_depth:0
      arg_type_full
      ret_type_full
      code_field
  in
  trace (Ill_typed_contract (code, !type_map)) result
  >>=? fun ((_ : (_, _, _, _) kdescr), ctxt) ->
  let views_result =
    parse_views ~unparse_code_rec ctxt ~elab_conf storage_type views
  in
  trace (Ill_typed_contract (code, !type_map)) views_result
  >|=? fun (typed_views, ctxt) ->
  ( Typechecked_code_internal
      {
        toplevel;
        arg_type;
        storage_type;
        entrypoints;
        typed_views;
        type_map = !type_map;
      },
    ctxt )

(* Uncarbonated because used only in RPCs *)
let list_entrypoints_uncarbonated (type full fullc) (full : (full, fullc) ty)
    (entrypoints : full entrypoints) =
  let merge path (type t tc) (ty : (t, tc) ty)
      (entrypoints : t entrypoints_node) reachable ((unreachables, all) as acc)
      =
    match entrypoints.at_node with
    | None ->
        ( (if reachable then acc
          else
            match ty with
            | Or_t _ -> acc
            | _ -> (List.rev path :: unreachables, all)),
          reachable )
    | Some {name; original_type_expr} ->
        ( (if Entrypoint.Map.mem name all then
           (List.rev path :: unreachables, all)
          else
            ( unreachables,
              Entrypoint.Map.add name (Ex_ty ty, original_type_expr) all )),
          true )
  in
  let rec fold_tree :
      type t tc.
      (t, tc) ty ->
      t entrypoints_node ->
      prim list ->
      bool ->
      prim list list * (ex_ty * Script.node) Entrypoint.Map.t ->
      prim list list * (ex_ty * Script.node) Entrypoint.Map.t =
   fun t entrypoints path reachable acc ->
    match (t, entrypoints) with
    | Or_t (tl, tr, _, _), {nested = Entrypoints_Or {left; right}; _} ->
        let acc, l_reachable = merge (D_Left :: path) tl left reachable acc in
        let acc, r_reachable = merge (D_Right :: path) tr right reachable acc in
        let acc = fold_tree tl left (D_Left :: path) l_reachable acc in
        fold_tree tr right (D_Right :: path) r_reachable acc
    | _ -> acc
  in
  let init, reachable =
    match entrypoints.root.at_node with
    | None -> (Entrypoint.Map.empty, false)
    | Some {name; original_type_expr} ->
        (Entrypoint.Map.singleton name (Ex_ty full, original_type_expr), true)
  in
  fold_tree full entrypoints.root [] reachable ([], init)

include Data_unparser (struct
  let opened_ticket_type = opened_ticket_type

  let parse_packable_ty = parse_packable_ty

  let parse_data = parse_data
end)

let unparse_code_rec : unparse_code_rec =
 fun ctxt ~stack_depth mode node ->
  unparse_code ctxt ~stack_depth mode node >>=? fun (code, ctxt) ->
  return (Micheline.root code, ctxt)

let parse_and_unparse_script_unaccounted ctxt ~legacy ~allow_forged_in_storage
    mode ~normalize_types {code; storage} =
  Script.force_decode_in_context
    ~consume_deserialization_gas:When_needed
    ctxt
    code
  >>?= fun (code, ctxt) ->
  typecheck_code ~unparse_code_rec ~legacy ~show_types:false ctxt code
  >>=? fun ( Typechecked_code_internal
               {
                 toplevel =
                   {
                     code_field;
                     arg_type = original_arg_type_expr;
                     storage_type = original_storage_type_expr;
                     views;
                   };
                 arg_type;
                 storage_type;
                 entrypoints;
                 typed_views;
                 type_map = _;
               },
             ctxt ) ->
  parse_storage
    ~unparse_code_rec
    ~elab_conf:(Script_ir_translator_config.make ~legacy ())
    ctxt
    ~allow_forged:allow_forged_in_storage
    storage_type
    ~storage
  >>=? fun (storage, ctxt) ->
  unparse_code ctxt ~stack_depth:0 mode code_field >>=? fun (code, ctxt) ->
  unparse_data ctxt ~stack_depth:0 mode storage_type storage
  >>=? fun (storage, ctxt) ->
  let loc = Micheline.dummy_location in
  (if normalize_types then
   unparse_parameter_ty ~loc ctxt arg_type ~entrypoints
   >>?= fun (arg_type, ctxt) ->
   unparse_ty ~loc ctxt storage_type >>?= fun (storage_type, ctxt) ->
   Script_map.map_es_in_context
     (fun ctxt
          _name
          (Typed_view {input_ty; output_ty; kinstr = _; original_code_expr}) ->
       Lwt.return
         ( unparse_ty ~loc ctxt input_ty >>? fun (input_ty, ctxt) ->
           unparse_ty ~loc ctxt output_ty >|? fun (output_ty, ctxt) ->
           ({input_ty; output_ty; view_code = original_code_expr}, ctxt) ))
     ctxt
     typed_views
   >|=? fun (views, ctxt) -> (arg_type, storage_type, views, ctxt)
  else return (original_arg_type_expr, original_storage_type_expr, views, ctxt))
  >>=? fun (arg_type, storage_type, views, ctxt) ->
  Script_map.map_es_in_context
    (fun ctxt _name {input_ty; output_ty; view_code} ->
      unparse_code ctxt ~stack_depth:0 mode view_code
      >|=? fun (view_code, ctxt) ->
      let view_code = Micheline.root view_code in
      ({input_ty; output_ty; view_code}, ctxt))
    ctxt
    views
  >>=? fun (views, ctxt) ->
  let open Micheline in
  let unparse_view_unaccounted name {input_ty; output_ty; view_code} views =
    Prim
      ( loc,
        K_view,
        [
          String (loc, Script_string.to_string name);
          input_ty;
          output_ty;
          view_code;
        ],
        [] )
    :: views
  in
  let views = Script_map.fold unparse_view_unaccounted views [] |> List.rev in
  let code =
    Seq
      ( loc,
        [
          Prim (loc, K_parameter, [arg_type], []);
          Prim (loc, K_storage, [storage_type], []);
          Prim (loc, K_code, [Micheline.root code], []);
        ]
        @ views )
  in
  return
    ( {code = lazy_expr (strip_locations code); storage = lazy_expr storage},
      ctxt )

let pack_data_with_mode ctxt ty data ~mode =
  unparse_data ~stack_depth:0 ctxt mode ty data >|=? fun (unparsed, ctxt) ->
  pack_node unparsed ctxt

let hash_data ctxt ty data =
  pack_data_with_mode ctxt ty data ~mode:Optimized_legacy
  >>=? fun (bytes, ctxt) -> Lwt.return @@ hash_bytes ctxt bytes

let pack_data ctxt ty data =
  pack_data_with_mode ctxt ty data ~mode:Optimized_legacy

(* ---------------- Lazy storage---------------------------------------------*)

type lazy_storage_ids = Lazy_storage.IdSet.t

let no_lazy_storage_id = Lazy_storage.IdSet.empty

let diff_of_big_map ctxt mode ~temporary ~ids_to_copy
    (Big_map {id; key_type; value_type; diff}) =
  (match id with
  | Some id ->
      if Lazy_storage.IdSet.mem Big_map id ids_to_copy then
        Big_map.fresh ~temporary ctxt >|=? fun (ctxt, duplicate) ->
        (ctxt, Lazy_storage.Copy {src = id}, duplicate)
      else
        (* The first occurrence encountered of a big_map reuses the
             ID. This way, the payer is only charged for the diff.
             For this to work, this diff has to be put at the end of
             the global diff, otherwise the duplicates will use the
             updated version as a base. This is true because we add
             this diff first in the accumulator of
             `extract_lazy_storage_updates`, and this accumulator is not
             reversed. *)
        return (ctxt, Lazy_storage.Existing, id)
  | None ->
      Big_map.fresh ~temporary ctxt >>=? fun (ctxt, id) ->
      Lwt.return
        (let kt = unparse_comparable_ty_uncarbonated ~loc:() key_type in
         Gas.consume ctxt (Script.strip_locations_cost kt) >>? fun ctxt ->
         unparse_ty ~loc:() ctxt value_type >>? fun (kv, ctxt) ->
         Gas.consume ctxt (Script.strip_locations_cost kv) >|? fun ctxt ->
         let key_type = Micheline.strip_locations kt in
         let value_type = Micheline.strip_locations kv in
         (ctxt, Lazy_storage.(Alloc Big_map.{key_type; value_type}), id)))
  >>=? fun (ctxt, init, id) ->
  let pairs =
    Big_map_overlay.fold
      (fun key_hash (key, value) acc -> (key_hash, key, value) :: acc)
      diff.map
      []
  in
  List.fold_left_es
    (fun (acc, ctxt) (key_hash, key, value) ->
      Gas.consume ctxt Typecheck_costs.parse_instr_cycle >>?= fun ctxt ->
      unparse_comparable_data ctxt mode key_type key >>=? fun (key, ctxt) ->
      (match value with
      | None -> return (None, ctxt)
      | Some x ->
          unparse_data ~stack_depth:0 ctxt mode value_type x
          >|=? fun (node, ctxt) -> (Some node, ctxt))
      >|=? fun (value, ctxt) ->
      let diff_item = Big_map.{key; key_hash; value} in
      (diff_item :: acc, ctxt))
    ([], ctxt)
    (List.rev pairs)
  >|=? fun (updates, ctxt) -> (Lazy_storage.Update {init; updates}, id, ctxt)

let diff_of_sapling_state ctxt ~temporary ~ids_to_copy
    ({id; diff; memo_size} : Sapling.state) =
  (match id with
  | Some id ->
      if Lazy_storage.IdSet.mem Sapling_state id ids_to_copy then
        Sapling.fresh ~temporary ctxt >|=? fun (ctxt, duplicate) ->
        (ctxt, Lazy_storage.Copy {src = id}, duplicate)
      else return (ctxt, Lazy_storage.Existing, id)
  | None ->
      Sapling.fresh ~temporary ctxt >|=? fun (ctxt, id) ->
      (ctxt, Lazy_storage.Alloc Sapling.{memo_size}, id))
  >|=? fun (ctxt, init, id) ->
  (Lazy_storage.Update {init; updates = diff}, id, ctxt)

(**
    Witness flag for whether a type can be populated by a value containing a
    lazy storage.
    [False_f] must be used only when a value of the type cannot contain a lazy
    storage.

    This flag is built in [has_lazy_storage] and used only in
    [extract_lazy_storage_updates] and [collect_lazy_storage].

    This flag is necessary to avoid these two functions to have a quadratic
    complexity in the size of the type.

    Add new lazy storage kinds here.

    Please keep the usage of this GADT local.
*)

type 'ty has_lazy_storage =
  | Big_map_f : ('a, 'b) big_map has_lazy_storage
  | Sapling_state_f : Sapling.state has_lazy_storage
  | False_f : _ has_lazy_storage
  | Pair_f :
      'a has_lazy_storage * 'b has_lazy_storage
      -> ('a, 'b) pair has_lazy_storage
  | Or_f :
      'a has_lazy_storage * 'b has_lazy_storage
      -> ('a, 'b) or_ has_lazy_storage
  | Option_f : 'a has_lazy_storage -> 'a option has_lazy_storage
  | List_f : 'a has_lazy_storage -> 'a Script_list.t has_lazy_storage
  | Map_f : 'v has_lazy_storage -> (_, 'v) map has_lazy_storage

(**
    This function is called only on storage and parameter types of contracts,
    once per typechecked contract. It has a complexity linear in the size of
    the types, which happen to be literally written types, so the gas for them
    has already been paid.
*)
let rec has_lazy_storage : type t tc. (t, tc) ty -> t has_lazy_storage =
 fun ty ->
  let aux1 cons t =
    match has_lazy_storage t with False_f -> False_f | h -> cons h
  in
  let aux2 cons t1 t2 =
    match (has_lazy_storage t1, has_lazy_storage t2) with
    | False_f, False_f -> False_f
    | h1, h2 -> cons h1 h2
  in
  match ty with _ -> failwith "TODO"

(**
  Transforms a value potentially containing lazy storage in an intermediary
  state to a value containing lazy storage only represented by identifiers.

  Returns the updated value, the updated set of ids to copy, and the lazy
  storage diff to show on the receipt and apply on the storage.

*)
let extract_lazy_storage_updates ctxt mode ~temporary ids_to_copy acc ty x =
  let rec aux :
      type a ac.
      context ->
      unparsing_mode ->
      temporary:bool ->
      Lazy_storage.IdSet.t ->
      Lazy_storage.diffs ->
      (a, ac) ty ->
      a ->
      has_lazy_storage:a has_lazy_storage ->
      (context * a * Lazy_storage.IdSet.t * Lazy_storage.diffs) tzresult Lwt.t =
   fun ctxt mode ~temporary ids_to_copy acc ty x ~has_lazy_storage ->
    Gas.consume ctxt Typecheck_costs.parse_instr_cycle >>?= fun ctxt ->
    match (has_lazy_storage, ty, x) with
    | False_f, _, _ -> return (ctxt, x, ids_to_copy, acc)
    | Big_map_f, Big_map_t (_, _, _), map ->
        diff_of_big_map ctxt mode ~temporary ~ids_to_copy map
        >|=? fun (diff, id, ctxt) ->
        let map =
          let (Big_map map) = map in
          Big_map
            {
              map with
              diff = {map = Big_map_overlay.empty; size = 0};
              id = Some id;
            }
        in
        let diff = Lazy_storage.make Big_map id diff in
        let ids_to_copy = Lazy_storage.IdSet.add Big_map id ids_to_copy in
        (ctxt, map, ids_to_copy, diff :: acc)
    | Sapling_state_f, Sapling_state_t _, sapling_state ->
        diff_of_sapling_state ctxt ~temporary ~ids_to_copy sapling_state
        >|=? fun (diff, id, ctxt) ->
        let sapling_state =
          Sapling.empty_state ~id ~memo_size:sapling_state.memo_size ()
        in
        let diff = Lazy_storage.make Sapling_state id diff in
        let ids_to_copy = Lazy_storage.IdSet.add Sapling_state id ids_to_copy in
        (ctxt, sapling_state, ids_to_copy, diff :: acc)
    | Pair_f (hl, hr), Pair_t (tyl, tyr, _, _), (xl, xr) ->
        aux ctxt mode ~temporary ids_to_copy acc tyl xl ~has_lazy_storage:hl
        >>=? fun (ctxt, xl, ids_to_copy, acc) ->
        aux ctxt mode ~temporary ids_to_copy acc tyr xr ~has_lazy_storage:hr
        >|=? fun (ctxt, xr, ids_to_copy, acc) ->
        (ctxt, (xl, xr), ids_to_copy, acc)
    | Or_f (has_lazy_storage, _), Or_t (ty, _, _, _), L x ->
        aux ctxt mode ~temporary ids_to_copy acc ty x ~has_lazy_storage
        >|=? fun (ctxt, x, ids_to_copy, acc) -> (ctxt, L x, ids_to_copy, acc)
    | Or_f (_, has_lazy_storage), Or_t (_, ty, _, _), R x ->
        aux ctxt mode ~temporary ids_to_copy acc ty x ~has_lazy_storage
        >|=? fun (ctxt, x, ids_to_copy, acc) -> (ctxt, R x, ids_to_copy, acc)
    | Option_f has_lazy_storage, Option_t (ty, _, _), Some x ->
        aux ctxt mode ~temporary ids_to_copy acc ty x ~has_lazy_storage
        >|=? fun (ctxt, x, ids_to_copy, acc) -> (ctxt, Some x, ids_to_copy, acc)
    | List_f has_lazy_storage, List_t (ty, _), l ->
        List.fold_left_es
          (fun (ctxt, l, ids_to_copy, acc) x ->
            aux ctxt mode ~temporary ids_to_copy acc ty x ~has_lazy_storage
            >|=? fun (ctxt, x, ids_to_copy, acc) ->
            (ctxt, Script_list.cons x l, ids_to_copy, acc))
          (ctxt, Script_list.empty, ids_to_copy, acc)
          l.elements
        >|=? fun (ctxt, l, ids_to_copy, acc) ->
        let reversed = Script_list.rev l in
        (ctxt, reversed, ids_to_copy, acc)
    | Map_f has_lazy_storage, Map_t (_, ty, _), map ->
        let (module M) = Script_map.get_module map in
        let bindings m = M.OPS.fold (fun k v bs -> (k, v) :: bs) m [] in
        List.fold_left_es
          (fun (ctxt, m, ids_to_copy, acc) (k, x) ->
            aux ctxt mode ~temporary ids_to_copy acc ty x ~has_lazy_storage
            >|=? fun (ctxt, x, ids_to_copy, acc) ->
            (ctxt, M.OPS.add k x m, ids_to_copy, acc))
          (ctxt, M.OPS.empty, ids_to_copy, acc)
          (bindings M.boxed)
        >|=? fun (ctxt, m, ids_to_copy, acc) ->
        let module M = struct
          module OPS = M.OPS

          type key = M.key

          type value = M.value

          let boxed = m

          let size = M.size
        end in
        ( ctxt,
          Script_map.make
            (module M : Boxed_map
              with type key = M.key
               and type value = M.value),
          ids_to_copy,
          acc )
    | _, Option_t (_, _, _), None -> return (ctxt, None, ids_to_copy, acc)
  in
  let has_lazy_storage = has_lazy_storage ty in
  aux ctxt mode ~temporary ids_to_copy acc ty x ~has_lazy_storage

(** We namespace an error type for [fold_lazy_storage]. The error case is only
    available when the ['error] parameter is equal to unit. *)
module Fold_lazy_storage = struct
  type ('acc, 'error) result =
    | Ok : 'acc -> ('acc, 'error) result
    | Error : ('acc, unit) result
end

(** Prematurely abort if [f] generates an error. Use this function without the
    [unit] type for [error] if you are in a case where errors are impossible.
*)
let rec fold_lazy_storage :
    type a ac error.
    f:('acc, error) Fold_lazy_storage.result Lazy_storage.IdSet.fold_f ->
    init:'acc ->
    context ->
    (a, ac) ty ->
    a ->
    has_lazy_storage:a has_lazy_storage ->
    (('acc, error) Fold_lazy_storage.result * context) tzresult =
 fun ~f ~init ctxt ty x ~has_lazy_storage ->
  Gas.consume ctxt Typecheck_costs.parse_instr_cycle >>? fun ctxt ->
  match (has_lazy_storage, ty, x) with
  | Big_map_f, Big_map_t (_, _, _), Big_map {id = Some id; _} ->
      Gas.consume ctxt Typecheck_costs.parse_instr_cycle >>? fun ctxt ->
      ok (f.f Big_map id (Fold_lazy_storage.Ok init), ctxt)
  | Sapling_state_f, Sapling_state_t _, {id = Some id; _} ->
      Gas.consume ctxt Typecheck_costs.parse_instr_cycle >>? fun ctxt ->
      ok (f.f Sapling_state id (Fold_lazy_storage.Ok init), ctxt)
  | False_f, _, _ -> ok (Fold_lazy_storage.Ok init, ctxt)
  | Big_map_f, Big_map_t (_, _, _), Big_map {id = None; _} ->
      ok (Fold_lazy_storage.Ok init, ctxt)
  | Sapling_state_f, Sapling_state_t _, {id = None; _} ->
      ok (Fold_lazy_storage.Ok init, ctxt)
  | Pair_f (hl, hr), Pair_t (tyl, tyr, _, _), (xl, xr) -> (
      fold_lazy_storage ~f ~init ctxt tyl xl ~has_lazy_storage:hl
      >>? fun (init, ctxt) ->
      match init with
      | Fold_lazy_storage.Ok init ->
          fold_lazy_storage ~f ~init ctxt tyr xr ~has_lazy_storage:hr
      | Fold_lazy_storage.Error -> ok (init, ctxt))
  | Or_f (has_lazy_storage, _), Or_t (ty, _, _, _), L x ->
      fold_lazy_storage ~f ~init ctxt ty x ~has_lazy_storage
  | Or_f (_, has_lazy_storage), Or_t (_, ty, _, _), R x ->
      fold_lazy_storage ~f ~init ctxt ty x ~has_lazy_storage
  | _, Option_t (_, _, _), None -> ok (Fold_lazy_storage.Ok init, ctxt)
  | Option_f has_lazy_storage, Option_t (ty, _, _), Some x ->
      fold_lazy_storage ~f ~init ctxt ty x ~has_lazy_storage
  | List_f has_lazy_storage, List_t (ty, _), l ->
      List.fold_left_e
        (fun ((init, ctxt) : ('acc, error) Fold_lazy_storage.result * context) x ->
          match init with
          | Fold_lazy_storage.Ok init ->
              fold_lazy_storage ~f ~init ctxt ty x ~has_lazy_storage
          | Fold_lazy_storage.Error -> ok (init, ctxt))
        (Fold_lazy_storage.Ok init, ctxt)
        l.elements
  | Map_f has_lazy_storage, Map_t (_, ty, _), m ->
      Script_map.fold
        (fun _
             v
             (acc : (('acc, error) Fold_lazy_storage.result * context) tzresult) ->
          acc >>? fun (init, ctxt) ->
          match init with
          | Fold_lazy_storage.Ok init ->
              fold_lazy_storage ~f ~init ctxt ty v ~has_lazy_storage
          | Fold_lazy_storage.Error -> ok (init, ctxt))
        m
        (ok (Fold_lazy_storage.Ok init, ctxt))

let collect_lazy_storage ctxt ty x =
  let has_lazy_storage = has_lazy_storage ty in
  let f kind id (acc : (_, never) Fold_lazy_storage.result) =
    let acc = match acc with Fold_lazy_storage.Ok acc -> acc in
    Fold_lazy_storage.Ok (Lazy_storage.IdSet.add kind id acc)
  in
  fold_lazy_storage ~f:{f} ~init:no_lazy_storage_id ctxt ty x ~has_lazy_storage
  >>? fun (ids, ctxt) ->
  match ids with Fold_lazy_storage.Ok ids -> ok (ids, ctxt)

let extract_lazy_storage_diff ctxt mode ~temporary ~to_duplicate ~to_update ty v
    =
  (*
    Basically [to_duplicate] are ids from the argument and [to_update] are ids
    from the storage before execution (i.e. it is safe to reuse them since they
    will be owned by the same contract).
  *)
  let to_duplicate = Lazy_storage.IdSet.diff to_duplicate to_update in
  extract_lazy_storage_updates ctxt mode ~temporary to_duplicate [] ty v
  >|=? fun (ctxt, v, alive, diffs) ->
  let diffs =
    if temporary then diffs
    else
      let dead = Lazy_storage.IdSet.diff to_update alive in
      Lazy_storage.IdSet.fold_all
        {f = (fun kind id acc -> Lazy_storage.make kind id Remove :: acc)}
        dead
        diffs
  in
  match diffs with
  | [] -> (v, None, ctxt)
  | diffs -> (v, Some diffs (* do not reverse *), ctxt)

let list_of_big_map_ids ids =
  Lazy_storage.IdSet.fold Big_map (fun id acc -> id :: acc) ids []

let parse_data ~elab_conf ctxt ~allow_forged ty t =
  parse_data ~unparse_code_rec ~elab_conf ~allow_forged ~stack_depth:0 ctxt ty t

let parse_view ~elab_conf ctxt ty view =
  parse_view ~unparse_code_rec ~elab_conf ctxt ty view

let parse_views ~elab_conf ctxt ty views =
  parse_views ~unparse_code_rec ~elab_conf ctxt ty views

let parse_code ~elab_conf ctxt ~code =
  parse_code ~unparse_code_rec ~elab_conf ctxt ~code

let parse_storage ~elab_conf ctxt ~allow_forged ty ~storage =
  parse_storage ~unparse_code_rec ~elab_conf ctxt ~allow_forged ty ~storage

let parse_script ~elab_conf ctxt ~allow_forged_in_storage script =
  parse_script ~unparse_code_rec ~elab_conf ctxt ~allow_forged_in_storage script

let parse_comparable_data ?type_logger ctxt ty t =
  parse_data
    ~elab_conf:Script_ir_translator_config.(make ~legacy:false ?type_logger ())
    ~allow_forged:false
    ctxt
    ty
    t

let parse_instr :
    type a s.
    elab_conf:elab_conf ->
    tc_context ->
    context ->
    Script.node ->
    (a, s) stack_ty ->
    ((a, s) judgement * context) tzresult Lwt.t =
 fun ~elab_conf tc_context ctxt script_instr stack_ty ->
  parse_instr
    ~unparse_code_rec
    ~elab_conf
    ~stack_depth:0
    tc_context
    ctxt
    script_instr
    stack_ty

let unparse_data = unparse_data ~stack_depth:0

let unparse_code ctxt mode code =
  (* Constants need to be expanded or [unparse_code] may fail. *)
  Global_constants_storage.expand ctxt (strip_locations code)
  >>=? fun (ctxt, code) -> unparse_code ~stack_depth:0 ctxt mode (root code)

let parse_contract_data context loc arg_ty contract ~entrypoint =
  parse_contract_data ~stack_depth:0 context loc arg_ty contract ~entrypoint

let parse_toplevel ctxt toplevel =
  Global_constants_storage.expand ctxt toplevel >>=? fun (ctxt, toplevel) ->
  Lwt.return @@ parse_toplevel ctxt toplevel

let parse_comparable_ty = parse_comparable_ty ~stack_depth:0

let parse_big_map_value_ty = parse_big_map_value_ty ~stack_depth:0

let parse_packable_ty = parse_packable_ty ~stack_depth:0

let parse_passable_ty = parse_passable_ty ~stack_depth:0

let parse_any_ty = parse_any_ty ~stack_depth:0

let parse_ty = parse_ty ~stack_depth:0 ~ret:Don't_parse_entrypoints

let parse_parameter_ty_and_entrypoints =
  parse_parameter_ty_and_entrypoints ~stack_depth:0

let get_single_sapling_state ctxt ty x =
  let has_lazy_storage = has_lazy_storage ty in
  let f (type i a u) (kind : (i, a, u) Lazy_storage.Kind.t) (id : i)
      single_id_opt : (Sapling.Id.t option, unit) Fold_lazy_storage.result =
    match kind with
    | Lazy_storage.Kind.Sapling_state -> (
        match single_id_opt with
        | Fold_lazy_storage.Ok None -> Fold_lazy_storage.Ok (Some id)
        | Fold_lazy_storage.Ok (Some _) ->
            Fold_lazy_storage.Error (* more than one *)
        | Fold_lazy_storage.Error -> single_id_opt)
    | _ -> single_id_opt
  in
  fold_lazy_storage ~f:{f} ~init:None ctxt ty x ~has_lazy_storage
  >>? fun (id, ctxt) ->
  match id with
  | Fold_lazy_storage.Ok (Some id) -> ok (Some id, ctxt)
  | Fold_lazy_storage.Ok None | Fold_lazy_storage.Error -> ok (None, ctxt)

(*

   {!Script_cache} needs a measure of the script size in memory.
   Determining this size is not easy in OCaml because of sharing.

   Indeed, many values present in the script share the same memory
   area. This is especially true for types and stack types: they are
   heavily shared in every typed IR internal representation. As a
   consequence, computing the size of the typed IR without taking
   sharing into account leads to a size which is sometimes two order
   of magnitude bigger than the actual size.

   We could track down this sharing. Unfortunately, sharing is not
   part of OCaml semantics: for this reason, a compiler can optimize
   memory representation by adding more sharing.  If two nodes use
   different optimization flags or compilers, such a precise
   computation of the memory footprint of scripts would lead to two
   distinct sizes. As these sizes occur in the blockchain context,
   this situation would lead to a fork.

   For this reason, we introduce a *size model* for the script size.
   This model provides an overapproximation of the actual size in
   memory. The risk is to be too far from the actual size: the cache
   would then be wrongly marked as full. This situation would make the
   cache less useful but should present no security risk .

*)
let script_size
    (Ex_script
      (Script
        {
          code_size;
          code = _;
          arg_type = _;
          storage;
          storage_type;
          entrypoints = _;
          views = _;
        })) =
  let nodes, storage_size =
    Script_typed_ir_size.value_size storage_type storage
  in
  let cost = Script_typed_ir_size_costs.nodes_cost ~nodes in
  (Saturation_repr.(add code_size storage_size |> to_int), cost)

let typecheck_code ~legacy ~show_types ctxt code =
  typecheck_code ~unparse_code_rec ~legacy ~show_types ctxt code
  >|=? fun (Typechecked_code_internal {type_map; _}, ctxt) -> (type_map, ctxt)
