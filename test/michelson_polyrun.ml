let string_contains s sub =
  try
    ignore Str.(search_forward (regexp_string sub) s 0) ;
    true
  with Not_found -> false

let string_contains = Alcotest.testable Format.pp_print_string string_contains

open Michelson_polyrun

let rust_output_as_string
    (tys : (string, Michelson_data_model.type_def) Hashtbl.t)
    (instrs : (string, Michelson_data_model.instruction_def) Hashtbl.t) =
  assert (Format.flush_str_formatter () = "") ;
  Types_rs_template.print tys instrs Format.str_formatter ;
  Format.flush_str_formatter ()

let hashtbl_of_list lst =
  let tys = Hashtbl.create (List.length lst) in
  List.iter (fun (key, value) -> Hashtbl.add tys key value) lst ;
  tys

let test_ord_type () =
  let tys =
    hashtbl_of_list
      [
        ( "unit",
          Michelson_data_model.
            {
              ty_name = "int";
              ty_arity = [];
              ty_ocaml_repr = "z num";
              ty_rust_repr = "IBig";
              ty_comparable = Yes;
              ty_atomic = true;
            } );
      ]
  in
  let instrs = hashtbl_of_list [] in
  let result = rust_output_as_string tys instrs in
  Alcotest.(check string_contains)
    "Contains unit type as an ord type"
    result
    "define_item_ord!(IntItem, IBig);"

let test_regular_type () =
  let tys =
    hashtbl_of_list
      [
        ( "key_hash",
          Michelson_data_model.
            {
              ty_name = "key_hash";
              ty_arity = [];
              ty_ocaml_repr = "key_hash";
              ty_rust_repr = "ImplicitAddress";
              ty_comparable = Yes;
              ty_atomic = true;
            } );
      ]
  in
  let instrs = hashtbl_of_list [] in
  let result = rust_output_as_string tys instrs in
  Alcotest.(check string_contains)
    "Contains key_hash type as a regular type"
    result
    "define_item!(KeyHashItem, ImplicitAddress);"

let test_parametric_type () =
  let tys =
    hashtbl_of_list
      [
        ( "list",
          Michelson_data_model.
            {
              ty_name = "list";
              ty_arity = [Type ("v", Any)];
              ty_ocaml_repr = "Script_list.t";
              ty_rust_repr = "Vec<StackItem>";
              ty_comparable = No;
              ty_atomic = false;
            } );
      ]
  in
  let instrs = hashtbl_of_list [] in
  let result = rust_output_as_string tys instrs in
  Alcotest.(check string_contains)
    "Contains list type as a parametric type"
    result
    "define_item_rec!(ListItem, Vec<StackItem>, Type);"

let test_predefined_type () =
  let tys =
    hashtbl_of_list
      [
        ( "operation",
          Michelson_data_model.
            {
              ty_name = "operation";
              ty_arity = [];
              ty_ocaml_repr = "operation";
              ty_rust_repr = "OperationItem";
              ty_comparable = No;
              ty_atomic = true;
            } );
      ]
  in
  let instrs = hashtbl_of_list [] in
  let result = rust_output_as_string tys instrs in
  Alcotest.(check (neg string_contains))
    "Doesn't contain operation defined from define_item macro"
    result
    "define_item_rec!(ListItem, Vec<StackItem>, Type);"

let () =
  Alcotest.(
    run
      "Polyrun"
      [
        ( "Rust backend",
          [
            test_case "Should emit ord types" `Quick test_ord_type;
            test_case "Should emit regular types" `Quick test_regular_type;
            test_case "Should emit parametric types" `Quick test_parametric_type;
            test_case
              "Shouldn't emit predefined type"
              `Quick
              test_predefined_type;
          ] );
      ])
