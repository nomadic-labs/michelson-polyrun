This repository is a proof of concept on using code generation to
derive parts of the Michelson type checker, interpreter,
documentation, unit tests, and Coq formalizations from a single
specification of the language. For this is a proof of concept, we
don't aim at supporting the full Michelson language but only a small
subset.

This is a prototype meant to drive design decisions. The code of this
repository is meant to be thrown away once the design is
settled. Don't build anything over this code and don't integrate it in
another project.

For more information, especially about the motivations behind this
project and a tentative roadmap, see
https://codimd.nomadic-labs.com/s/Ls8FjX_9o# .

# The fragment

The fragment of the Michelson language supported by this tool can be
found in file generated/text/output.txt. This file is generated by the
tool.

## How to use this repository

In the repo root directory run

```sh
dune exec ./bin/main.exe
```

## How to contribute

The source code is mainly in the `lib/` directory.
`bin/` contains the main entrypoint and the json description of michelson.
An overview of the organization of the modules is given in `doc/architecture.md`.

To rebuild the project after some changes do:

```sh
dune build
```

To reformat all files run

```sh
make format
```

Continuous integration is used on the git repository
(https://gitlab.com/nomadic-labs/michelson-polyrun) to check that:
- source files compile,
- they generate exactly the commited files,
- all OCaml files are well formatted.

## How to debug

Most types `ty` are annotated with `[@@deriving show]` which automatically
produce `show_ty: ty -> string` and `pp_ty : Format.formatter -> ty -> unit`.

If there is no pretty-printer defined for the type of value that you want to
print, adding the `[@@deriving show]` annotation to the type definition should
do it. not that type named `t` will have `show` and `pp` functions, not `show_t`
and `pp_t`.



As of today, we didn't felt the need for an actual logging mechanism, there is
just a Log.log function, which doesn't output anything. To make it verbose,
just change `ifprintf` to `fprintf` in `log.ml`
